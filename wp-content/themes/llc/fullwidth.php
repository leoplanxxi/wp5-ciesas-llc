<?php
/**
* Template Name: Full Width
 */

get_header(); ?>
<?php // Como son internas, leemos slug para aplicar efecto de color a menu seleccionado ?>
	<script>
	searchpath_URL = window.location.pathname;
	slug_path = searchpath_URL.split("/")[2];
		
	if (slug_path == "investigacion-publicaciones" || slug_path == "investigacion-proyectos") {
		var element = document.getElementById("menu-item-213");
 		element.classList.add("current_page_item");
	}
	else if (slug_path == "materiales-para-ninos" || slug_path == "materiales-para-maestros") {
		var element = document.getElementById("menu-item-210");
 		element.classList.add("current_page_item");
	}

	</script>
	<section id="primary" class="content-area col-sm-12">
		<div id="main" class="site-main" role="main">

			<?php
			while ( have_posts() ) : the_post();

				get_template_part( 'template-parts/content', 'page' );

				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;

			endwhile; // End of the loop.
			?>

		</div><!-- #main -->
	</section><!-- #primary -->

<?php
get_footer();
