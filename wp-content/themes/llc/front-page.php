<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WP_Bootstrap_Starter
 */

get_header(); ?>
<script>
var audio_playing = false;
var audio = new Audio('/wp-content/uploads/2021/01/audio_intro_lab.m4a');
jQuery(function($){
	$("#audioToggle").click(function(){
		if (audio_playing == false) {
			audio_playing = true;
			audio.play();
		}
		else {
			audio_playing = false;
			audio.pause();
		}
	});
})
</script>

	<section id="primary" class="content-area col-sm-12 col-lg-12">
		<?php echo llc_video_principal(); ?>
		<div id="main" class="site-main" role="main">
			<?php
			while ( have_posts() ) : the_post();

				get_template_part( 'template-parts/content', 'page' );

                // If comments are open or we have at least one comment, load up the comment template.
                if ( comments_open() || get_comments_number() ) :
                    comments_template();
                endif;

			endwhile; // End of the loop.
            ?>
            
        <div class="objetivos">
            <h1>Objetivos</h1>

            <div class="container">
                <div class="row row-objetivos">
                    <div class="col-lg-6 col-objetivo-a">
                        Construir un espacio en el que se realiza investigación básica y aplicada sobre temas de lingüística y semiótica desde marcos teóricos y metodológicos multidisciplinarios.
                    </div>
                    <div class="col-lg-6 col-objetivo-b">
                        Reconocer la relación lengua – cultura como acción humana realizada por actores específicos en contextos específicos.
                    </div>
                    <div class="col-lg-6 col-objetivo-c">
                        Trabajar por el conocimiento, rescate y difusión del patrimonio lingüístico y cultural del país, hasta ahora poco valorados y atendidos.
                    </div>
                    <div class="col-lg-6 col-objetivo-d">
                        Impactar en la comunidad científica, en instituciones del sector público y en organizaciones civiles; y trabajar con los actores sociales de distintas regiones del
país.

                    </div>
                </div>
            </div>
        </div>
		<p>
			&nbsp;
			</p>
		<div class="btns">
			<div class="row">
				<button type="button" class="toggle-quienes col-6" onclick="location.href='/index.php/quienes-somos/'">¿Quiénes somos?</button>
				<button type="button" class="toggle-tiempo col-6" onclick="location.href='/index.php/el-llc-en-el-tiempo/'">El LLC en el tiempo</button>

			</div>	
		</div>
        <div style="width: 90%; display: block; margin: 0 auto; border:1px solid #ddd; margin-top: 50px;"></div>
        <div class="laboratorios">
            <h1>Programas Especiales CIESAS</h1>
			<div class="row">
				<div class="col-lg-3 col-12">
                    <a href="https://lab.ciesas.edu.mx/prosig-csh/"><img  src="<?php echo get_template_directory_uri(); ?>/img/lab-prosig-ciesas.png"></a>
				</div>
				<div class="col-lg-3 col-12">
					<a href="https://tlacuatzin.com/"><img  src="<?php echo get_template_directory_uri(); ?>/img/lab-acervo-ciesas.png"></a>
				</div>
				<div class="col-lg-3 col-12">
                    <a href="https://lab.ciesas.edu.mx/audiovisual"><img  src="<?php echo get_template_directory_uri(); ?>/img/lab-audiovisual-ciesas.jpeg" style="width: 140px"></a>
				</div>
			</div>
        <div style="width: 90%; display: block; margin: 0 auto; border:1px solid #ddd; margin-top: 50px;"></div>
        <?php /* Fuente: https://azmind.com/bootstrap-carousel-multiple-items/ */ ?>
        
		<div class="top-content">
		    <div class="container-fluid">
		        <div id="carousel-example" class="carousel slide" data-ride="carousel">
		            <div class="carousel-inner row w-100 mx-auto" role="listbox" style="font-size: 30px; text-align: center; color: gray;">
		                <div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3 active">
							<a href="https://www.gob.mx/inpi">INPI</a>
		                </div>
		                <div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3">
							<a href="https://www.inali.gob.mx/">INALI</a>
		                </div>
		                <div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3">
							<a href="https://www.inaoep.mx/">INAOE</a>
		                </div>
		                <div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3">
							<a href="https://www.unicef.org/mexico/">UNICEF MEXICO</a>
		                </div>
		                <div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3">
							KW'ANIS
		                </div>
		                <div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3">
							<a href="https://dgei.basica.sep.gob.mx/">DGEI</a>
		                </div>
						<div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3">
							<a href="https://oralidadmodernidad.wixsite.com/oralidad">ORALIDAD MODERNIDAD</a>
						</div>

		            </div>
		            <a class="carousel-control-prev" href="#carousel-example" role="button" data-slide="prev">
						<img src="<?php echo get_template_directory_uri(); ?>/img/arr_izq.png" width="30" height="30">
		                <span class="sr-only">Previous</span>
		            </a>
		            <a class="carousel-control-next" href="#carousel-example" role="button" data-slide="next">
						<img src="<?php echo get_template_directory_uri(); ?>/img/arr_der.png" width="30" height="30">
		                <span class="sr-only">Next</span>
		            </a>
		        </div>
		    </div>
			<div style="width: 90%; display: block; margin: 0 auto; border:1px solid #ddd; margin-top: 50px;"></div>
			<h1>Contacto</h1>
			<style>
				a[class^=contacto-] {
					font-size: 25px;
					font-weight: bolder;
					padding: 20px 0 20px 30px;
					width: 100%;
					border: 0;
					display: block;
					text-align: left;
					text-decoration: none;
				}
				.contacto-email {background: white; color: black !important;}
				.contacto-yt { background: white; color: #c4302b !important;}
				.contacto-fb { background: white; color: #3b5998 !important; }
				.contacto-ig { background: white; color: #E1306C !important; }

			</style>
			<div class="row">
				<a href="https://www.youtube.com/channel/UCKSPQCK3THxRq6SIwOxR2QA" class="contacto-yt col-sm-6 col-12"><img src="/wp-content/themes/llc/img/iconos/youtube.png" width=64> YouTube</a>
				<a href="https://www.facebook.com/LaboratorioLenguayCultura " class="contacto-fb col-sm-6 col-12"><img src="/wp-content/themes/llc/img/iconos/facebook.png" width=64> Facebook</a>
				<a href="https://www.instagram.com/laboratorio_lengua_cultura/" class="contacto-ig col-sm-6 col-12"><img src="/wp-content/themes/llc/img/iconos/instagram.png" width=64> Instagram</a>
				<a href="mailto:lab.lenguaycultura@gmail.com" class="contacto-email col-sm-6 col-12">lab.lenguaycultura@gmail.com</a>

			</div>

		    <?php include "slider-front.php"; ?>
     </div>

		</div><!-- #main -->
	</section><!-- #primary -->

<?php
get_footer();
