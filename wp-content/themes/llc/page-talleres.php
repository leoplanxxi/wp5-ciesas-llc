<?php
/* Template Name: Pagina Taller */ ?>

<?php get_header(); ?>
<style>
.nav-taller { width: 100%; border-bottom: 1px solid #ddd; padding-top: 50px; padding-bottom: 10px; overflow: hidden;}
.nav-taller .talleres { color: #FF00FF; font-weight: bolder; font-size: 25px; float: left; width: 150px;}
.nav-taller .regresar { float: left; margin-top: 10px; }
.nav-taller .regresar a { font-weight: bolder; color: #979797;}
h1.entry-title { border: none !important; font-weight: bolder; font-size: 40px; color: #979797;} 
.container-taller-info .col-taller-info-left { border-right: 1px solid gray;}
	.boton-inscribirme { color: #EB1E79;}

</style>

	<section id="primary" class="content-area col-sm-12 col-lg-12">
		<div id="main" class="site-main" role="main">
			<div class='nav-taller'>
				<div class='talleres'>Talleres</div>
				<div class='regresar'><a href='/index.php/diplomados-talleres-y-webinart/'><< regresar</a></div>
			</div>

			<?php
			while ( have_posts() ) : the_post();

				get_template_part( 'template-parts/content', 'page' );

                // If comments are open or we have at least one comment, load up the comment template.
                if ( comments_open() || get_comments_number() ) :
                    comments_template();
                endif;

			endwhile; // End of the loop.
			?>

		</div><!-- #main -->
	</section><!-- #primary -->

<?php
#get_sidebar();
get_footer();
