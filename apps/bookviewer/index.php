<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Visor PDF</title>
  <meta name="viewport" content="width=device-width, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">


  <!-- Flipbook StyleSheet -->
  <link href="dflip/css/dflip.min.css" rel="stylesheet" type="text/css">

  <!-- Icons Stylesheet -->
  <link href="dflip/css/themify-icons.min.css" rel="stylesheet" type="text/css">

</head>
<body style="background-color: teal">
<?php
$id = $_GET["id"];
switch($id) {
	case 1:
    	$file = "/wp-content/uploads/2020/JE_RI_KANRAJI_GA_KJANU_YO_JNATJO.pdf";
    	break;
    case 2:
    	$file = "/wp-content/uploads/2020/INTERACTIVO_CUADERNO_PUREPECHA.pdf";
    	break;
    case "cuaderno-trabajo-otomi":
    	$file = "/wp-content/uploads/materiales/Cuaderno%20de%20trabajo%20de%20la%20lengua%20H%C3%91AH%C3%91U%20(OTOMI).pdf";
    	break;
    case "cuaderno-trabajo-purepecha":
    	$file = "/wp-content/uploads/materiales/Cuaderno%20de%20trabajo%20de%20la%20lengua%20Purepecha.pdf";
    	break;
    case "cuaderno-trabajo-espanol":
    	$file = "/wp-content/uploads/materiales/Cuaderno%20de%20trabajo%20Español.pdf";
    	break;
    case 3:
    	$file = "/wp-content/uploads/materiales/MANUAL_MORRALITO.pdf";
        break;
    default:
    	break;
}
?>
<div class="container">

  <div class="row">

    <div class="col-xs-12" style="padding-bottom:30px">
      <!--Normal FLipbook-->
      <div class="_df_book" style="height: 100%;" webgl="true" backgroundcolor="teal"
              source="<?php echo $file; ?>"
              id="df_manual_book">
      </div>

    </div>
  </div>
</div>

<!-- jQuery  -->
<script src="dflip/js/libs/jquery.min.js" type="text/javascript"></script>
<!-- Flipbook main Js file -->
<script src="dflip/js/dflip.min.js" type="text/javascript"></script>

</body>
</html>