var cont_p1 = 6; 
var cont_p2 = 6;
var cont_p3 = 6;

var anterior_p1 = 0;
var anterior_p2 = 0;
var anterior_p3 = 0;


var Anterior_p1 = 6;
var Anterior_p2 = 6;
var Anterior_p3 = 6;

var x = 0;
var y = 0;
var z = 0;
var y_Aves = 0;

var b_anuncio = 0;

var b_inicio = 0;


//	ESCENA ACERCA DE 4:

var HelloWorldScene4 = cc.Layer.extend({
	sprint:null,
	ctor:function() {
		this._super();
		this.init();
	},
	init:function() {
		this._super();
		var size = cc.director.getWinSize(); // Obtener tamaño de pantalla
		
		var ABOUT = cc.Sprite.create(res.BG_ABOUT);
		ABOUT.setPosition(size.width/2, size.height / 2);
		this.addChild(ABOUT, kZindexBG);
	
		//	Button siguiente:
		var Button_before = new ccui.Button();
		Button_before.loadTextures(res.BUTTON_BEFORE);
		Button_before.setScale(0.8, 0.8);
		Button_before.setPosition(cc.p(100, 100));
		Button_before.addTouchEventListener(this.touchEvent, this);
		this.addChild(Button_before, 0, "Button_before");
	},
	onEnter:function() {
		this._super();
	},
	touchEvent: function(sender, type)
    {	
		switch (type)
        {
            case ccui.Widget.TOUCH_BEGAN:
            var object_name = sender.getName(); //	Obtiene el nombre del evento

            	if(object_name == "Button_before"){
            		cc.director.runScene(HelloWorldScene3.scene());
            	}
    			break;               
        }
    }
});

HelloWorldScene4.scene = function(){
        var scene =  new cc.Scene();
        var layer = new HelloWorldScene4();
		scene.addChild(layer);
		return scene;
}



//	ESCENA INTRODUCCION 3:

var HelloWorldScene3 = cc.Layer.extend({
	sprint:null,
	ctor:function() {
		this._super();
		this.init();
	},
	init:function() {
		this._super();
		var size = cc.director.getWinSize(); // Obtener tamaño de pantalla
		
		var Introduccion = cc.Sprite.create(res.INTRODUCCION);
		Introduccion.setPosition(size.width/2, size.height / 2);
		this.addChild(Introduccion, kZindexBG);
	
	//	Button siguiente:
		var Button_play = new ccui.Button();
		Button_play.loadTextures(res.BUTTON_PLAY);
		Button_play.setPosition(cc.p(1800, 100));
		Button_play.addTouchEventListener(this.touchEvent, this);
		this.addChild(Button_play, 0, "Button_play");
	
		//	Button siguiente:
		var Button_about = new ccui.Button();
		Button_about.loadTextures(res.BUTTON_ABOUT);
		Button_about.setPosition(cc.p(200, 100));
		Button_about.addTouchEventListener(this.touchEvent, this);
		this.addChild(Button_about, 0, "Button_about");

		cc.audioEngine.playMusic(res.INTRO_MUSIC, true);
		cc.audioEngine.setMusicVolume(0.2);
	},
	onEnter:function() {
		this._super();
	},
	touchEvent: function(sender, type)
    {	
switch (type)
        {
            case ccui.Widget.TOUCH_BEGAN:
            	var object_name = sender.getName(); //	Obtiene el nombre del evento

            	if(object_name == "Button_play"){
            		cont_p1 = 6;
					cont_p2 = 6;
					cont_p3 = 6;

					b_anuncio = 0;

					Anterior_p1 = 6;
					Anterior_p2 = 6;
					Anterior_p3 = 6;

					anterior_p1 = 0;
					anterior_p2 = 0;
					anterior_p3 = 0;

            		cc.director.runScene(HelloWorldScene2.scene());
            	}
            	if(object_name == "Button_about"){
            		cc.director.runScene(HelloWorldScene4.scene());
            	}
    			break;               
        }
    }
});

HelloWorldScene3.scene = function(){
        var scene =  new cc.Scene();
        var layer = new HelloWorldScene3();
		scene.addChild(layer);
		return scene;
}


//	SCENA 2 JUEGO GUSANITO:

var HelloWorldScene2 = cc.Layer.extend({
	sprint:null,
	ctor:function() {
		this._super();
		this.init();
	},
	init:function() {
		this._super();

		var size = cc.director.getWinSize(); // Obtener tamaño de pantalla
		
		var bg_play = cc.Sprite.create(res.BG_START);
		bg_play.setPosition(size.width/2, size.height / 2);
		this.addChild(bg_play, kZindexBG);

	// Animación del Gusanito
		
		// Cuello del gusanito
		var cuello_gusanito = new cc.Sprite.create(res.NECK_GUSANITO);
		cuello_gusanito.setScale(0.4);
		cuello_gusanito.setAnchorPoint(cc.p(0.5,0.5));
		cuello_gusanito.setPosition(cc.p(190,250));
		this.addChild(cuello_gusanito, 0);

		var cuello_action1 = cc.MoveBy.create(1, -20, 0);
		var cuello_action2 = cc.MoveBy.create(1, 20, 0);
		var cuello_sequence = cc.Sequence.create(cuello_action1,cuello_action2);
		var cuello_repeat = cc.RepeatForever.create(cuello_sequence);
		cuello_gusanito.runAction(cuello_repeat);

		// Cabeza del gusanito
		var cabeza_gusanito = new cc.Sprite.create(res.GUSANITO);
		cabeza_gusanito.setScale(0.6);
		cabeza_gusanito.setAnchorPoint(cc.p(0.5,0.5));
		cabeza_gusanito.setPosition(cc.p(170, 420));
		this.addChild(cabeza_gusanito, 0);

		var cabeza_action = cc.RotateTo.create(1,-15);
		var cabeza_action2 = cc.RotateTo.create(1,15);
		var cabeza_sequence_action = cc.Sequence.create( cabeza_action, cabeza_action2);
		var cabeza_repeat_action = cc.RepeatForever.create(cabeza_sequence_action);
		cabeza_gusanito.runAction(cabeza_repeat_action);

		// Cuerpo del gusanito parte 1
		var cuerpo_gusanito_part1 = new cc.Sprite.create(res.BODY_GUSANITO);
		cuerpo_gusanito_part1.setScale(0.55,0.4);
		cuerpo_gusanito_part1.setAnchorPoint(cc.p(0.5,0.5));
		cuerpo_gusanito_part1.setPosition(cc.p(720, 170));
		this.addChild(cuerpo_gusanito_part1, 0);

		var cuerpo_action1_p1 = cc.MoveBy.create(1, 0, 20);
		var cuerpo_action2_p1 = cc.MoveBy.create(1, 0, -20);
		var cuerpo_sequence_p1 = cc.Sequence.create( cuerpo_action1_p1, cuerpo_action2_p1);
		var cuerpo_repeat_p1 = cc.RepeatForever.create(cuerpo_sequence_p1);
		cuerpo_gusanito_part1.runAction(cuerpo_repeat_p1);
		
		// Cuerpo del gusanito parte 2
		var cuerpo_gusanito_part2 = new cc.Sprite.create(res.BODY_GUSANITO);
		cuerpo_gusanito_part2.setScale(0.55, 0.4);
		cuerpo_gusanito_part2.setAnchorPoint(cc.p(0.5,0.5));
		cuerpo_gusanito_part2.setPosition(cc.p(490, 170));
		this.addChild(cuerpo_gusanito_part2, 0);

		var cuerpo_action1_p2 = cc.MoveBy.create(1, 0, -20);
		var cuerpo_action2_p2 = cc.MoveBy.create(1, 0, 20);
		var cuerpo_sequence_p2 = cc.Sequence.create( cuerpo_action1_p2, cuerpo_action2_p2);
		var cuerpo_repeat_p2 = cc.RepeatForever.create(cuerpo_sequence_p2);
		cuerpo_gusanito_part2.runAction(cuerpo_repeat_p2);

		// Cuerpo del gusanito parte 3
		var cuerpo_gusanito_part3 = new cc.Sprite.create(res.BODY_GUSANITO);
		cuerpo_gusanito_part3.setScale(0.55, 0.4);
		cuerpo_gusanito_part3.setAnchorPoint(cc.p(0.5,0.5));
		cuerpo_gusanito_part3.setPosition(cc.p(260, 170));
		this.addChild(cuerpo_gusanito_part3, 0);
		
		var cuerpo_action1_p3 = cc.MoveBy.create(1, 0, 20);
		var cuerpo_action2_p3 = cc.MoveBy.create(1, 0, -20);
		var cuerpo_sequence_p3 = cc.Sequence.create( cuerpo_action1_p3, cuerpo_action2_p3);
		var cuerpo_repeat_p3 = cc.RepeatForever.create(cuerpo_sequence_p3);
		cuerpo_gusanito_part3.runAction(cuerpo_repeat_p3);

//	Menu:
		
	//	Parte up1:
		//	Buton_up_p1:
		var Button_up_p1 = new ccui.Button();
		Button_up_p1.loadTextures(res.BUTTON_ARRIBA);
		Button_up_p1.setScale(0.5, 0.5);
		Button_up_p1.setPosition(cc.p(350, 195));
		Button_up_p1.addTouchEventListener(this.touchEvent, this);
        this.addChild(Button_up_p1, 0, "btn_up_p1");

		//	Animacion Button_up_p1:
		var Button_action1_up_p1 = cc.MoveBy.create(1, 0, 20);
		var Button_action2_up_p1 = cc.MoveBy.create(1, 0, -20);
		var Button_sequence_up_p1 = cc.Sequence.create(Button_action1_up_p1, Button_action2_up_p1);
		var Button_repeat_up_p1 = cc.RepeatForever.create(Button_sequence_up_p1);
		Button_up_p1.runAction(Button_repeat_up_p1);

		//	Buton_down_p1:
		var Button_down_p1 = new ccui.Button();
		Button_down_p1.loadTextures(res.BUTTON_ABAJO);
		Button_down_p1.setScale(0.5, 0.5);
		Button_down_p1.setPosition(cc.p(350, 145));
		Button_down_p1.addTouchEventListener(this.touchEvent, this);
        this.addChild(Button_down_p1, 0, "btn_down_p1");
		
		//	Animacion Button_down_p1:
		var Button_action1_down_p1 = cc.MoveBy.create(1, 0, 20);
		var Button_action2_down_p1 = cc.MoveBy.create(1, 0, -20);
		var Button_sequence_down_p1 = cc.Sequence.create(Button_action1_down_p1, Button_action2_down_p1);
		var Button_repeat_down_p1 = cc.RepeatForever.create(Button_sequence_down_p1);
		Button_down_p1.runAction(Button_repeat_down_p1);

		// PARTE 2:

		//	Buton_up_p2:

		var Button_up_p2 = new ccui.Button();
		Button_up_p2.loadTextures(res.BUTTON_ARRIBA);
		Button_up_p2.setScale(0.5, 0.5);
		Button_up_p2.setPosition(cc.p(585, 195));
		Button_up_p2.addTouchEventListener(this.touchEvent, this);
        this.addChild(Button_up_p2, 0, "btn_up_p2");

		//	ANIMACION Button_up_p2:
		var Button_action1_up_p2 = cc.MoveBy.create(1, 0, -20);
		var Button_action2_up_p2 = cc.MoveBy.create(1, 0, 20);
		var Button_sequence_up_p2 = cc.Sequence.create(Button_action1_up_p2, Button_action2_up_p2);
		var Button_repeat_up_p2 = cc.RepeatForever.create(Button_sequence_up_p2);
		Button_up_p2.runAction(Button_repeat_up_p2);
		
		//	Buton_down_p2:
		var Button_down_p2 = new ccui.Button();
		Button_down_p2.loadTextures(res.BUTTON_ABAJO);
		Button_down_p2.setScale(0.5, 0.5);
		Button_down_p2.setPosition(cc.p(585, 145));
		Button_down_p2.addTouchEventListener(this.touchEvent, this);
		this.addChild(Button_down_p2, 0, "btn_down_p2");
		
		//	ANIMACION Button_down_p2:
		var Button_action1_down_p2 = cc.MoveBy.create(1, 0, -20);
		var Button_action2_down_p2 = cc.MoveBy.create(1, 0, 20);
		var Button_sequence_down_p2 = cc.Sequence.create(Button_action1_down_p2, Button_action2_down_p2);
		var Button_repeat_down_p2 = cc.RepeatForever.create(Button_sequence_down_p2);
		Button_down_p2.runAction(Button_repeat_down_p2);

		//	PARTE 3:

		//	Buton_up_p3:
		var Button_up_p3 = new ccui.Button();
		Button_up_p3.loadTextures(res.BUTTON_ARRIBA);
		Button_up_p3.setScale(0.5, 0.5);
		Button_up_p3.setPosition(cc.p(820, 195));
		Button_up_p3.addTouchEventListener(this.touchEvent, this);
        this.addChild(Button_up_p3, 0, "btn_up_p3");

		//	ANIMACION Button_up_p3:
		var Button_action1_up_p3 = cc.MoveBy.create(1, 0, 20);
		var Button_action2_up_p3 = cc.MoveBy.create(1, 0, -20);
		var Button_sequence_up_p3 = cc.Sequence.create(Button_action1_up_p3, Button_action2_up_p3);
		var Button_repeat_up_p3 = cc.RepeatForever.create(Button_sequence_up_p3);
		Button_up_p3.runAction(Button_repeat_up_p3);
		
		//	Buton_down_p3:
		var Button_down_p3 = new ccui.Button();
		Button_down_p3.loadTextures(res.BUTTON_ABAJO);
		Button_down_p3.setScale(0.5, 0.5);
		Button_down_p3.setPosition(cc.p(820, 145));
		Button_down_p3.addTouchEventListener(this.touchEvent, this);
        this.addChild(Button_down_p3, 0, "btn_down_p3");
		
		//	ANIMACION Button_down_p3:
		var Button_action1_down_p3 = cc.MoveBy.create(1, 0, 20);
		var Button_action2_down_p3 = cc.MoveBy.create(1, 0, -20);
		var Button_sequence_down_p3 = cc.Sequence.create(Button_action1_down_p3, Button_action2_down_p3);
		var Button_repeat_down_p3 = cc.RepeatForever.create(Button_sequence_down_p3);
		Button_down_p3.runAction(Button_repeat_down_p3);

		//	Button_virgula:
		var Button_virgula = new ccui.Button();
		Button_virgula.loadTextures(res.BUTTON_VIRGULA);
		Button_virgula.setScale(0.6, .6);
		Button_virgula.setPosition(cc.p(450, 380));
		Button_virgula.addTouchEventListener(this.touchEvent, this);
        this.addChild(Button_virgula, 0, "btn_virgula");

        //	Button acerca de:
		var Button_before = new ccui.Button();
		Button_before.loadTextures(res.BUTTON_BEFORE);
		Button_before.setScale(0.8, 0.8);
		Button_before.setPosition(cc.p(80, 920));
		Button_before.addTouchEventListener(this.touchEvent, this);
		this.addChild(Button_before, 0, "Button_before");
        
// SPRITES:		

		//	Sprite Escenarios:
			var IL6 = new cc.Sprite.create(res.IL6);
			IL6.setScale(1.2);
			IL6.setPosition(cc.p(1370,480));
			IL6.setTag("IL6");
			this.addChild(IL6, 0);

			//	Sprite Frases_Lugar:
			var FL6 = new cc.Sprite.create(res.FL6);
			FL6.setScale(0.5);
			FL6.setPosition(cc.p(240, 175));
			FL6.setTag("FL6");
			this.addChild(FL6, 0);

			// Sprite Frases_Accion:
			var FA6 = new cc.Sprite.create(res.FA6);
			FA6.setScale(0.5);
			FA6.setPosition(cc.p(485, 175));
			FA6.setTag("FA6");
			this.addChild(FA6, 0);

			//	Sprite Frases_Personajes:
			var FP6 = new cc.Sprite.create(res.FP6);
			FP6.setScale(0.5);
			FP6.setPosition(cc.p(710, 175));
			FP6.setTag("FP6");
			this.addChild(FP6, 0);

		//	Anuncio:
			var Anuncio = new cc.Sprite.create(res.ANUNCIO);
			//Anuncio.setScale(0.8);
			Anuncio.setPosition(cc.p(550,650));
			Anuncio.setTag("Anuncio");
			this.addChild(Anuncio, 0);

//	Audio de fondo:
		cc.audioEngine.playMusic(res.MAIN_MUSIC, true);
		cc.audioEngine.setMusicVolume(0.1);	

	},
	onEnter:function() {
		this._super();
	},
	touchEvent: function(sender, type)
    {	

    	var object_name = sender.getName(); //	Obtiene el nombre del evento

//	Sprite Escenarios:
    	var IL1 = new cc.Sprite.create(res.IL1);
		IL1.setScale(1.2);
		IL1.setPosition(cc.p(1370,480));
		IL1.setTag("IL1");
		
		var IL2 = new cc.Sprite.create(res.IL2);
		IL2.setScale(1.2);
		IL2.setPosition(cc.p(1370,480));
		IL2.setTag("IL2");

		var IL3 = new cc.Sprite.create(res.IL3);
		IL3.setScale(1.2);
		IL3.setPosition(cc.p(1370,480));
		IL3.setTag("IL3");

		var IL4 = new cc.Sprite.create(res.IL4);
		IL4.setScale(1.2);
		IL4.setPosition(cc.p(1370,480));
		IL4.setTag("IL4");

		var IL5 = new cc.Sprite.create(res.IL5);
		IL5.setScale(1.2);
		IL5.setPosition(cc.p(1370,480));
		IL5.setTag("IL5");

		var IL6 = new cc.Sprite.create(res.IL6);
		IL6.setScale(1.2);
		IL6.setPosition(cc.p(1370,480));
		IL6.setTag("IL6");

		var IL7 = new cc.Sprite.create(res.IL7);
		IL7.setScale(1.2);
		IL7.setPosition(cc.p(1370,480));
		IL7.setTag("IL7");

		var IL8 = new cc.Sprite.create(res.IL8);
		IL8.setScale(1.2);
		IL8.setPosition(cc.p(1370,480));
		IL8.setTag("IL8");

		var IL9 = new cc.Sprite.create(res.IL9);
		IL9.setScale(1.2);
		IL9.setPosition(cc.p(1370,480));
		IL9.setTag("IL9");

		var IL10 = new cc.Sprite.create(res.IL10);
		IL10.setScale(1.2);
		IL10.setPosition(cc.p(1370,480));
		IL10.setTag("IL10");

		var IL11 = new cc.Sprite.create(res.IL11);
		IL11.setScale(1.2);
		IL11.setPosition(cc.p(1370,480));
		IL11.setTag("IL11");
//	Sprite Frases_Lugar:

		var FL1 = new cc.Sprite.create(res.FL1);
		FL1.setScale(0.5);
		FL1.setPosition(cc.p(240, 175));
		FL1.setTag("FL1");

		var FL2 = new cc.Sprite.create(res.FL2);
		FL2.setScale(0.5);
		FL2.setPosition(cc.p(240, 175));
		FL2.setTag("FL2");

		var FL3 = new cc.Sprite.create(res.FL3);
		FL3.setScale(0.5);
		FL3.setPosition(cc.p(240, 175));
		FL3.setTag("FL3");

		var FL4 = new cc.Sprite.create(res.FL4);
		FL4.setScale(0.5);
		FL4.setPosition(cc.p(240, 175));
		FL4.setTag("FL4");

		var FL5 = new cc.Sprite.create(res.FL5);
		FL5.setScale(0.5);
		FL5.setPosition(cc.p(240, 175));
		FL5.setTag("FL5");

		var FL6 = new cc.Sprite.create(res.FL6);
		FL6.setScale(0.5);
		FL6.setPosition(cc.p(240, 175));
		FL6.setTag("FL6");

		var FL7 = new cc.Sprite.create(res.FL7);
		FL7.setScale(0.5);
		FL7.setPosition(cc.p(240, 175));
		FL7.setTag("FL7");

		var FL8 = new cc.Sprite.create(res.FL8);
		FL8.setScale(0.5);
		FL8.setPosition(cc.p(240, 175));
		FL8.setTag("FL8");

		var FL9 = new cc.Sprite.create(res.FL9);
		FL9.setScale(0.5);
		FL9.setPosition(cc.p(240, 175));
		FL9.setTag("FL9");

		var FL10 = new cc.Sprite.create(res.FL10);
		FL10.setScale(0.5);
		FL10.setPosition(cc.p(240, 175));
		FL10.setTag("FL10");

		var FL11 = new cc.Sprite.create(res.FL11);
		FL11.setScale(0.5);
		FL11.setPosition(cc.p(240, 175));
		FL11.setTag("FL11");
//	Sprite Frases_Accion:

		var FA1 = new cc.Sprite.create(res.FA1);
		FA1.setScale(0.5);
		FA1.setPosition(cc.p(485, 175));
		FA1.setTag("FA1");

		var FA2 = new cc.Sprite.create(res.FA2);
		FA2.setScale(0.5);
		FA2.setPosition(cc.p(485, 175));
		FA2.setTag("FA2");

		var FA3 = new cc.Sprite.create(res.FA3);
		FA3.setScale(0.5);
		FA3.setPosition(cc.p(485, 175));
		FA3.setTag("FA3");

		var FA4 = new cc.Sprite.create(res.FA4);
		FA4.setScale(0.5);
		FA4.setPosition(cc.p(485, 175));
		FA4.setTag("FA4");

		var FA5 = new cc.Sprite.create(res.FA5);
		FA5.setScale(0.5);
		FA5.setPosition(cc.p(485, 175));
		FA5.setTag("FA5");

		var FA6 = new cc.Sprite.create(res.FA6);
		FA6.setScale(0.5);
		FA6.setPosition(cc.p(485, 175));
		FA6.setTag("FA6");

		var FA7 = new cc.Sprite.create(res.FA7);
		FA7.setScale(0.5);
		FA7.setPosition(cc.p(485, 175));
		FA7.setTag("FA7");

		var FA8 = new cc.Sprite.create(res.FA8);
		FA8.setScale(0.5);
		FA8.setPosition(cc.p(485, 175));
		FA8.setTag("FA8");

		var FA9 = new cc.Sprite.create(res.FA9);
		FA9.setScale(0.5);
		FA9.setPosition(cc.p(485, 175));
		FA9.setTag("FA9");

		var FA10 = new cc.Sprite.create(res.FA10);
		FA10.setScale(0.5);
		FA10.setPosition(cc.p(485, 175));
		FA10.setTag("FA10");

		var FA11 = new cc.Sprite.create(res.FA11);
		FA11.setScale(0.5);
		FA11.setPosition(cc.p(485, 175));
		FA11.setTag("FA11");		
//	Sprite Frases_Personajes:		

		var FP1 = new cc.Sprite.create(res.FP1);
		FP1.setScale(0.5);
		FP1.setPosition(cc.p(710, 175));
		FP1.setTag("FP1");

		var FP2 = new cc.Sprite.create(res.FP2);
		FP2.setScale(0.5);
		FP2.setPosition(cc.p(710, 175));
		FP2.setTag("FP2");

		var FP3 = new cc.Sprite.create(res.FP3);
		FP3.setScale(0.5);
		FP3.setPosition(cc.p(710, 175));
		FP3.setTag("FP3");

		var FP4 = new cc.Sprite.create(res.FP4);
		FP4.setScale(0.5);
		FP4.setPosition(cc.p(710, 175));
		FP4.setTag("FP4");

		var FP5 = new cc.Sprite.create(res.FP5);
		FP5.setScale(0.5);
		FP5.setPosition(cc.p(710, 175));
		FP5.setTag("FP5");

		var FP6 = new cc.Sprite.create(res.FP6);
		FP6.setScale(0.5);
		FP6.setPosition(cc.p(710, 175));
		FP6.setTag("FP6");

		var FP7 = new cc.Sprite.create(res.FP7);
		FP7.setScale(0.5);
		FP7.setPosition(cc.p(710, 175));
		FP7.setTag("FP7");

		var FP8 = new cc.Sprite.create(res.FP8);
		FP8.setScale(0.5);
		FP8.setPosition(cc.p(710, 175));
		FP8.setTag("FP8");

		var FP9 = new cc.Sprite.create(res.FP9);
		FP9.setScale(0.5);
		FP9.setPosition(cc.p(710, 175));
		FP9.setTag("FP9");

		var FP10 = new cc.Sprite.create(res.FP10);
		FP10.setScale(0.5);
		FP10.setPosition(cc.p(710, 175));
		FP10.setTag("FP10");

		var FP11 = new cc.Sprite.create(res.FP11);
		FP11.setScale(0.5);
		FP11.setPosition(cc.p(710, 175));
		FP11.setTag("FP11");

	// TRADUCCION DE FRASES:

//	Sprite Traduccion_Frases_Lugar:

		var TL1 = new cc.Sprite.create(res.TL1);
		TL1.setScale(1.2);
		TL1.setPosition(cc.p(1120, 700));
		TL1.setTag("TL1");

		var TL2 = new cc.Sprite.create(res.TL2);
		TL2.setScale(1.2);
		TL2.setPosition(cc.p(1120, 710));
		TL2.setTag("TL2");

		var TL3 = new cc.Sprite.create(res.TL3);
		TL3.setScale(1.2);
		TL3.setPosition(cc.p(1120, 710));
		TL3.setTag("TL3");

		var TL4 = new cc.Sprite.create(res.TL4);
		TL4.setScale(1.2);
		TL4.setPosition(cc.p(1120, 710));
		TL4.setTag("TL4");

		var TL5 = new cc.Sprite.create(res.TL5);
		TL5.setScale(1.2);
		TL5.setPosition(cc.p(1120, 710));
		TL5.setTag("TL5");

		var TL6 = new cc.Sprite.create(res.TL6);
		TL6.setScale(1.2);
		TL6.setPosition(cc.p(1120, 710));
		TL6.setTag("TL6");

		var TL7 = new cc.Sprite.create(res.TL7);
		TL7.setScale(1.2);
		TL7.setPosition(cc.p(1120, 710));
		TL7.setTag("TL7");

		var TL8 = new cc.Sprite.create(res.TL8);
		TL8.setScale(1.2);
		TL8.setPosition(cc.p(1120, 710));
		TL8.setTag("TL8");

		var TL9 = new cc.Sprite.create(res.TL9);
		TL9.setScale(1.2);
		TL9.setPosition(cc.p(1120, 710));
		TL9.setTag("TL9");

		var TL10 = new cc.Sprite.create(res.TL10);
		TL10.setScale(1.2);
		TL10.setPosition(cc.p(1120, 710));
		TL10.setTag("TL10");

		var TL11 = new cc.Sprite.create(res.TL11);
		TL11.setScale(1.2);
		TL11.setPosition(cc.p(1120, 710));
		TL11.setTag("TL11");
//	Sprite Traduccion_Frases_Accion:

		var TA1 = new cc.Sprite.create(res.TA1);
		TA1.setScale(1.2);
		TA1.setPosition(cc.p(1410, 710));
		TA1.setTag("TA1");

		var TA2 = new cc.Sprite.create(res.TA2);
		TA2.setScale(1.2);
		TA2.setPosition(cc.p(1410, 710));
		TA2.setTag("TA2");

		var TA3 = new cc.Sprite.create(res.TA3);
		TA3.setScale(1.2);
		TA3.setPosition(cc.p(1410, 710));
		TA3.setTag("TA3");

		var TA4 = new cc.Sprite.create(res.TA4);
		TA4.setScale(1.2);
		TA4.setPosition(cc.p(1410, 710));
		TA4.setTag("TA4");

		var TA5 = new cc.Sprite.create(res.TA5);
		TA5.setScale(1.2);
		TA5.setPosition(cc.p(1410, 710));
		TA5.setTag("TA5");

		var TA6 = new cc.Sprite.create(res.TA6);
		TA6.setScale(1.2);
		TA6.setPosition(cc.p(1410, 710));
		TA6.setTag("TA6");

		var TA7 = new cc.Sprite.create(res.TA7);
		TA7.setScale(1.2);
		TA7.setPosition(cc.p(1410, 710));
		TA7.setTag("TA7");

		var TA8 = new cc.Sprite.create(res.TA8);
		TA8.setScale(1.2);
		TA8.setPosition(cc.p(1410, 710));
		TA8.setTag("TA8");

		var TA9 = new cc.Sprite.create(res.TA9);
		TA9.setScale(1.2);
		TA9.setPosition(cc.p(1410, 710));
		TA9.setTag("TA9");

		var TA10 = new cc.Sprite.create(res.TA10);
		TA10.setScale(1.2);
		TA10.setPosition(cc.p(1410, 710));
		TA10.setTag("TA10");

		var TA11 = new cc.Sprite.create(res.TA11);
		TA11.setScale(1.2);
		TA11.setPosition(cc.p(1410, 710));
		TA11.setTag("TA11");
//	Sprite Traduccion_Frases_Personajes:

		var TP1 = new cc.Sprite.create(res.TP1);
		TP1.setScale(1.2);
		TP1.setPosition(cc.p(1700, 710));
		TP1.setTag("TP1");

		var TP2 = new cc.Sprite.create(res.TP2);
		TP2.setScale(1.2);
		TP2.setPosition(cc.p(1700, 710));
		TP2.setTag("TP2");

		var TP3 = new cc.Sprite.create(res.TP3);
		TP3.setScale(1.2);
		TP3.setPosition(cc.p(1700, 710));
		TP3.setTag("TP3");

		var TP4 = new cc.Sprite.create(res.TP4);
		TP4.setScale(1.2);
		TP4.setPosition(cc.p(1700, 710));
		TP4.setTag("TP4");

		var TP5 = new cc.Sprite.create(res.TP5);
		TP5.setScale(1.2);
		TP5.setPosition(cc.p(1700, 710));
		TP5.setTag("TP5");

		var TP6 = new cc.Sprite.create(res.TP6);
		TP6.setScale(1.2);
		TP6.setPosition(cc.p(1700, 710));
		TP6.setTag("TP6");

		var TP7 = new cc.Sprite.create(res.TP7);
		TP7.setScale(1.2);
		TP7.setPosition(cc.p(1700, 710));
		TP7.setTag("TP7");

		var TP8 = new cc.Sprite.create(res.TP8);
		TP8.setScale(1.2);
		TP8.setPosition(cc.p(1700, 710));
		TP8.setTag("TP8");

		var TP9 = new cc.Sprite.create(res.TP9);
		TP9.setScale(1.2);
		TP9.setPosition(cc.p(1700, 710));
		TP9.setTag("TP9");

		var TP10 = new cc.Sprite.create(res.TP10);
		TP10.setScale(1.2);
		TP10.setPosition(cc.p(1700, 710));
		TP10.setTag("TP10");

		var TP11 = new cc.Sprite.create(res.TP11);
		TP11.setScale(1.2);
		TP11.setPosition(cc.p(1700, 710));
		TP11.setTag("TP11");

//	Posición de Sprite:
		if(cont_p1 == 10){
			x = 1530;
			y = 480;
			z = 420;
			y_Aves = 525;
		}
		else if(cont_p1 == 1){
			x = 1500;
			y = 430;
			z = 330;
			y_Aves = 350;
		}
		else if(cont_p1 == 2){
			x = 1390;
			y = 400;
			z = 330;
			y_Aves = z+20;
		}
		else if(cont_p1 == 3 || cont_p1 == 4 || cont_p1 == 6){
			x = 1500;
			y = 400;
			z = 330;
			y_Aves = z;
		}
		else if (cont_p1 == 5 || cont_p1 == 7 || cont_p1 == 8 || cont_p1 == 9){
			x = 1420;
			y = 400;
			z = 330;
			y_Aves = 350;
		}
		else if(cont_p1 == 11){
			x = 1200;
			y = 400;
			z = 330;
			y_Aves = z;
		}

//	Sprite de A01 DUERME:
		// Sprite de A01P01:
    	this.A01P01 = cc.Sprite.create(res.A01P01_P1);        
    	this.A01P01.attr({
        	x: x,
			y: z,
        	tag: "A01P01"
    	});

    	// Sprite de A01P02:
    	this.A01P02 = cc.Sprite.create(res.A01P02_P1);        
    	this.A01P02.attr({
        	x: x,
			y: z,
        	tag: "A01P02"
    	});

    	// Sprite de A01P03:
    	this.A01P03 = cc.Sprite.create(res.A01P03_P1);        
    	this.A01P03.attr({
        	x: x,
			y: y_Aves,
        	tag: "A01P03"
    	});

    	// Sprite de A01P04:
    	this.A01P04 = cc.Sprite.create(res.A01P04_P1);        
    	this.A01P04.attr({
        	x: x,
			y: z,
        	tag: "A01P04"
    	});

    	// Sprite de A01P05:
    	this.A01P05 = cc.Sprite.create(res.A01P05_P1);        
    	this.A01P05.attr({
        	x: x,
			y: z,
        	tag: "A01P05"
    	});

    	// Sprite de A01P06:
    	this.A01P06 = cc.Sprite.create(res.A01P06_P1);        
    	this.A01P06.attr({
        	x: x,
			y: z,
        	tag: "A01P06"
    	});

    	// Sprite de A01P07:
    	this.A01P07 = cc.Sprite.create(res.A01P07_P1);        
    	this.A01P07.attr({
        	x: x,
			y: y_Aves,
			scale: 1.1,
        	tag: "A01P07"
    	});

    	// Sprite de A01P08:
    	this.A01P08 = cc.Sprite.create(res.A01P08_P1);        
    	this.A01P08.attr({
        	x: x,
			y: z,
        	tag: "A01P08"
    	});

		// Sprite de A01P09:
    	this.A01P09 = cc.Sprite.create(res.A01P09_P1);        
    	this.A01P09.attr({
        	x: x,
			y: y_Aves,
			scale: 1.1,
        	tag: "A01P09"
    	});

    	// Sprite de A01P10:
    	this.A01P10 = cc.Sprite.create(res.A01P10_P1);        
    	this.A01P10.attr({
        	x: x+20,
			y: y_Aves,
			scale: 1.1,
        	tag: "A01P10"
    	});

    	// Sprite de A01P11:
    	this.A01P11 = cc.Sprite.create(res.A01P11_P1);        
    	this.A01P11.attr({
        	x: x,
			y: y_Aves,
			scale: 1.1,
        	tag: "A01P11"
    	});
//	Sprite de A02 CORRE:
		// Sprite de A02P01:
    	this.A02P01 = cc.Sprite.create(res.A02P01_P1);        
    	this.A02P01.attr({
        	x: 900,
        	y: 380,
        	scale: 1.2,
        	tag: "A02P01"
    	});

    	// Sprite de A02P02:
    	this.A02P02 = cc.Sprite.create(res.A02P02_P1);        
    	this.A02P02.attr({
        	x: 900,
        	y: 380,
        	scale: 1.2,
        	tag: "A02P02"
    	});

    	// Sprite de A02P03:
    	this.A02P03 = cc.Sprite.create(res.A05P03_P1);        
    	this.A02P03.attr({
        	x: 900,
        	y: 330,
        	tag: "A02P03"
    	});

    	// Sprite de A02P04:
    	this.A02P04 = cc.Sprite.create(res.A02P04_P1);        
    	this.A02P04.attr({
        	x: 900,
        	y: 380,
        	scale: 1.2,
        	tag: "A02P04"
    	});
    	
    	// Sprite de A02P05:
    	this.A02P05 = cc.Sprite.create(res.A02P05_P1);        
    	this.A02P05.attr({
        	x: 900,
        	y: 380,
        	scale: 1.2,
        	tag: "A02P05"
    	});

    	// Sprite de A02P06:
    	this.A02P06 = cc.Sprite.create(res.A02P06_P1);        
    	this.A02P06.attr({
        	x: 900,
        	y: 380,
        	scale: 1.2,
        	tag: "A02P06"
    	});

    	// Sprite de A02P07:
    	this.A02P07 = cc.Sprite.create(res.A02P07_P1);        
    	this.A02P07.attr({
        	x: 900,
        	y: 320,
        	tag: "A02P07"
    	});

    	// Sprite de A02P08:
    	this.A02P08 = cc.Sprite.create(res.A02P08_P1);        
    	this.A02P08.attr({
        	x: 900,
        	y: 380,
        	scale: 1.2,
        	tag: "A02P08"
    	});

		// Sprite de A02P09:
    	this.A02P09 = cc.Sprite.create(res.A02P09_P1);        
    	this.A02P09.attr({
        	x: 900,
        	y: 320,
        	tag: "A02P09"
    	});

    	// Sprite de A02P10:
    	this.A02P10 = cc.Sprite.create(res.A02P10_P1);        
    	this.A02P10.attr({
        	x: 900,
        	y: 260,
        	tag: "A02P10"
    	});

    	// Sprite de A02P11:
    	this.A02P11 = cc.Sprite.create(res.A02P11_P1);        
    	this.A02P11.attr({
        	x: 900,
        	y: 320,
        	scale: 0.8,
        	tag: "A02P11"
    	});
//	Sprite de A03 MUERDE:
		// Sprite de A03P01:
    	this.A03P01 = cc.Sprite.create(res.A03P01_P1);        
    	this.A03P01.attr({
        	x: x,
			y: y,
        	scale: 1.2,
        	tag: "A03P01"
    	});

    	// Sprite de A03P02:
    	this.A03P02 = cc.Sprite.create(res.A03P02_P1);        
    	this.A03P02.attr({
        	x: x,
			y: y-10,
        	scale: 1.2,
        	tag: "A03P02"
    	});

    	// Sprite de A03P03:
    	this.A09P03 = cc.Sprite.create(res.A09P03_P1);        
    	this.A09P03.attr({
        	x: x,
			y: y_Aves,
        	tag: "A09P03"
    	});

    	// Sprite de A03P04:
    	this.A03P04 = cc.Sprite.create(res.A03P04_P1);        
    	this.A03P04.attr({
        	x: x,
			y: y,
        	scale: 1.2,
        	tag: "A03P04"
    	});
    	
    	// Sprite de A03P05:
    	this.A03P05 = cc.Sprite.create(res.A03P05_P1);        
    	this.A03P05.attr({
        	x: x,
			y: y,
        	scale: 1.2,
        	tag: "A03P05"
    	});

    	// Sprite de A03P06:
    	this.A03P06 = cc.Sprite.create(res.A03P06_P1);        
    	this.A03P06.attr({
        	x: x,
			y: y,
        	scale: 1.2,
        	tag: "A03P06"
    	});

    	// Sprite de A03P07:
    	this.A09P07 = cc.Sprite.create(res.A09P07_P1);        
    	this.A09P07.attr({
        	x: x,
			y: y_Aves,
        	tag: "A09P07"
    	});

    	// Sprite de A03P08:
    	this.A03P08 = cc.Sprite.create(res.A03P08_P1);        
    	this.A03P08.attr({
        	x: x,
			y: y,
        	scale: 1.2,
        	tag: "A03P08"
    	});

		// Sprite de A03P09:
    	this.A09P09 = cc.Sprite.create(res.A09P09_P1);        
    	this.A09P09.attr({
        	x: x,
			y: y_Aves,
        	tag: "A09P09"
    	});

    	// Sprite de A03P10:
    	this.A09P10 = cc.Sprite.create(res.A09P10_P1);        
    	this.A09P10.attr({
        	x: x+30,
			y: z,
			scale: 1.1,
        	tag: "A09P10"
    	});

    	// Sprite de A03P11:
    	this.A09P11 = cc.Sprite.create(res.A09P11_P1);        
    	this.A09P11.attr({
        	x: x,
			y: y_Aves,
        	tag: "A09P11"
    	});
//	Sprite de A04 BRINCA:
		// Sprite de A04P01:
    	this.A04P01 = cc.Sprite.create(res.A04P01_P1);        
    	this.A04P01.attr({
        	x: x,
        	y: y,
        	tag: "A04P01"
    	});

    	// Sprite de A04P02:
    	this.A04P02 = cc.Sprite.create(res.A04P02_P1);        
    	this.A04P02.attr({
        	x: x,
        	y: y,
        	tag: "A04P02"
    	});

    	// Sprite de A04P03:
    	this.A04P03 = cc.Sprite.create(res.A08P03_P1);        
    	this.A04P03.attr({
        	x: 1200,
        	y: 320,
        	scale: 0.9,
        	tag: "A04P03"
    	});

    	// Sprite de A04P04:
    	this.A04P04 = cc.Sprite.create(res.A04P04_P1);        
    	this.A04P04.attr({
        	x: x,
        	y: y,
        	tag: "A04P04"
    	});
    	
    	// Sprite de A04P05:
    	this.A04P05 = cc.Sprite.create(res.A04P05_P1);        
    	this.A04P05.attr({
        	x: x,
        	y: y,
        	tag: "A04P05"
    	});

    	// Sprite de A04P06:
    	this.A04P06 = cc.Sprite.create(res.A04P06_P1);        
    	this.A04P06.attr({
        	x: x,
        	y: y,
        	tag: "A04P06"
    	});

    	// Sprite de A04P07:
    	this.A04P07 = cc.Sprite.create(res.A04P07_P1);        
    	this.A04P07.attr({
        	x: x,
        	y: y_Aves-5,
        	scale: 1.1,
        	tag: "A04P07"
    	});

    	// Sprite de A04P08:
    	this.A04P08 = cc.Sprite.create(res.A04P08_P1);        
    	this.A04P08.attr({
        	x: x,
        	y: y,
        	tag: "A04P08"
    	});

		// Sprite de A04P09:
    	this.A04P09 = cc.Sprite.create(res.A04P09_P1);        
    	this.A04P09.attr({
        	x: x,
        	y: y_Aves-5,
        	scale: 1.1,
        	tag: "A04P09"
    	});

    	// Sprite de A04P10:
    	this.A04P10 = cc.Sprite.create(res.A04P10_P1);        
    	this.A04P10.attr({
        	x: x+50,
			y: z-50,
        	tag: "A04P10"
    	});

    	// Sprite de A04P11:
    	this.A04P11 = cc.Sprite.create(res.A04P11_P1);        
    	this.A04P11.attr({
        	x: x,
        	y: y_Aves-5,
        	scale: 1.1,
        	tag: "A04P11"
    	});
//	Sprite de A05 VUELA:
		// Sprite de A05P01:
    	this.A05P01 = cc.Sprite.create(res.A05P01_P1);        
    	this.A05P01.attr({
        	x: x,
        	y: y,
        	scale: 1.1,
        	tag: "A05P01"
    	});

    	// Sprite de A05P02:
    	this.A05P02 = cc.Sprite.create(res.A05P02_P1);        
    	this.A05P02.attr({
        	x: x,
        	y: y,
        	scale: 1.1,
        	tag: "A05P02"
    	});

    	// Sprite de A05P03:
    	this.A05P03 = cc.Sprite.create(res.A05P03_P1);        
    	this.A05P03.attr({
        	x: x,
        	y: y_Aves,
        	scale: 0.9,
        	tag: "A05P03"
    	});

    	// Sprite de A05P04:
    	this.A05P04 = cc.Sprite.create(res.A05P04_P1);        
    	this.A05P04.attr({
        	x: x,
        	y: y,
        	scale: 1.2,
        	tag: "A05P04"
    	});
    	
    	// Sprite de A05P05:
    	this.A05P05 = cc.Sprite.create(res.A05P05_P1);        
    	this.A05P05.attr({
        	x: x,
        	y: y,
        	scale: 1.2,
        	tag: "A05P05"
    	});

    	// Sprite de A05P06:
    	this.A05P06 = cc.Sprite.create(res.A05P06_P1);        
    	this.A05P06.attr({
        	x: x,
        	y: y,
        	scale: 1.2,
        	tag: "A05P06"
    	});

    	// Sprite de A05P07:
    	this.A05P07 = cc.Sprite.create(res.A05P07_P1);        
    	this.A05P07.attr({
        	x: 900,
        	y: 600,
        	scale: 0.8,
        	tag: "A05P07"
    	});

    	// Sprite de A01P08:
    	this.A05P08 = cc.Sprite.create(res.A05P08_P1);        
    	this.A05P08.attr({
        	x: x,
        	y: y,
        	scale: 1.2,
        	tag: "A05P08"
    	});

		// Sprite de A05P09:
    	this.A05P09 = cc.Sprite.create(res.A05P09_P1);        
    	this.A05P09.attr({
        	x: 900,
        	y: 600,
        	scale: 0.8,
        	tag: "A05P09"
    	});

    	// Sprite de A05P10:
    	this.A05P10 = cc.Sprite.create(res.A05P10_P1);        
    	this.A05P10.attr({
        	x: 900,
        	y: 600,
        	scale: 1.2,
        	tag: "A05P10"
    	});

    	// Sprite de A05P11:
    	this.A05P11 = cc.Sprite.create(res.A05P11_P1);        
    	this.A05P11.attr({
        	x: 900,
        	y: 600,
        	scale: 0.8,
        	tag: "A05P11"
    	});
//	Sprite de A06 HABLA:
		// Sprite de A06P01:
    	this.A06P01 = cc.Sprite.create(res.A06P01_P1);        
    	this.A06P01.attr({
        	x: x,
			y: y+30,
        	tag: "A06P01"
    	});

    	// Sprite de A06P02:
    	this.A06P02 = cc.Sprite.create(res.A06P02_P1);        
    	this.A06P02.attr({
        	x: x,
			y: y+30,
        	tag: "A06P02"
    	});

    	// Sprite de A06P03:
    	this.A06P03 = cc.Sprite.create(res.A06P03_P1);        
    	this.A06P03.attr({
        	x: x,
        	y: y_Aves+50,
        	scale: 0.9,
        	tag: "A06P03"
    	});

    	// Sprite de A06P04:
    	this.A06P04 = cc.Sprite.create(res.A06P04_P1);        
    	this.A06P04.attr({
        	x: x,
			y: y+30,
        	tag: "A06P04"
    	});
    	
    	// Sprite de A06P05:
    	this.A06P05 = cc.Sprite.create(res.A06P05_P1);        
    	this.A06P05.attr({
        	x: x,
			y: y+30,
        	tag: "A06P05"
    	});

    	// Sprite de A06P06:
    	this.A06P06 = cc.Sprite.create(res.A06P06_P1);        
    	this.A06P06.attr({
        	x: x,
			y: y+30,
        	tag: "A06P06"
    	});

    	// Sprite de A06P07:
    	this.A06P07 = cc.Sprite.create(res.A06P07_P1);        
    	this.A06P07.attr({
        	x: x,
        	y: y_Aves+30,
        	scale: 1.2,
        	tag: "A06P07"
    	});

    	// Sprite de A06P08:
    	this.A06P08 = cc.Sprite.create(res.A06P08_P1);        
    	this.A06P08.attr({
        	x: x,
			y: y+30,
        	tag: "A06P08"
    	});

		// Sprite de A06P09:
    	this.A06P09 = cc.Sprite.create(res.A06P09_P1);        
    	this.A06P09.attr({
        	x: x,
        	y: y_Aves+30,
        	scale: 1.2,
        	tag: "A06P09"
    	});

    	// Sprite de A06P10:
    	this.A06P10 = cc.Sprite.create(res.A06P10_P1);        
    	this.A06P10.attr({
        	x: x+50,
        	y: y_Aves-20,
        	tag: "A06P10"
    	});

    	// Sprite de A06P11:
    	this.A06P11 = cc.Sprite.create(res.A06P11_P1);        
    	this.A06P11.attr({
        	x: x,
        	y: y_Aves+30,
        	scale: 1.2,
        	tag: "A06P11"
    	});
//	Sprite de A07 CAMINA:
    	this.A07P01 = cc.Sprite.create(res.A07P01_P1);        
    	this.A07P01.attr({
        	x: x,
        	y: y-20,
        	tag: "A07P01"
    	});

    	// Sprite de A07P02:
    	this.A07P02 = cc.Sprite.create(res.A07P02_P1);        
    	this.A07P02.attr({
        	x: x,
        	y: y-30,
        	tag: "A07P02"
    	});

    	// Sprite de A07P03:
    	this.A07P03 = cc.Sprite.create(res.A07P03_P1);        
    	this.A07P03.attr({
        	x: x,
        	y: y_Aves+20,
        	scale: 0.9,
        	tag: "A07P03"
    	});

    	// Sprite de A07P04:
    	this.A07P04 = cc.Sprite.create(res.A07P04_P1);        
    	this.A07P04.attr({
        	x: x,
        	y: y-20,
        	tag: "A07P04"
    	});
    	
    	// Sprite de A07P05:
    	this.A07P05 = cc.Sprite.create(res.A07P05_P1);        
    	this.A07P05.attr({
        	x: x,
        	y: y-20,
        	tag: "A07P05"
    	});

    	// Sprite de A07P06:
    	this.A07P06 = cc.Sprite.create(res.A07P06_P1);        
    	this.A07P06.attr({
        	x: x,
        	y: y-10,
        	tag: "A07P06"
    	});

    	// Sprite de A07P07 == A04P07:
    	
    	// Sprite de A07P08:
    	this.A07P08 = cc.Sprite.create(res.A07P08_P1);        
    	this.A07P08.attr({
        	x: x,
        	y: y-20,
        	tag: "A07P08"
    	});

		// Sprite de A07P09 == A04P09:

    	// Sprite de A07P10:
    	this.A07P10 = cc.Sprite.create(res.A07P10_P1);        
    	this.A07P10.attr({
        	x: x,
        	y: y_Aves-20,
        	tag: "A07P10"
    	});

    	// Sprite de A07P11 == A04P11:
//	Sprite de A08 CRUZA:
		// Sprite de A08P01:
    	this.A08P01 = cc.Sprite.create(res.A08P01_P1);        
    	this.A08P01.attr({
        	x: 1200,
        	y: 380,
        	scale: 1.1,
        	tag: "A08P01"
    	});

    	// Sprite de A08P02:
    	this.A08P02 = cc.Sprite.create(res.A08P02_P1);        
    	this.A08P02.attr({
        	x: 1200,
        	y: 380,
        	scale: 1.1,
        	tag: "A08P02"
    	});

    	// Sprite de A04P03:
    	this.A08P03 = cc.Sprite.create(res.A08P03_P1);        
    	this.A08P03.attr({
        	x: 1250,
        	y: 300,
        	scale: 0.9,
        	tag: "A08P03"
    	});

    	// Sprite de A08P04:
    	this.A08P04 = cc.Sprite.create(res.A08P04_P1);        
    	this.A08P04.attr({
        	x: 1200,
        	y: 380,
        	scale: 1.1,
        	tag: "A08P04"
    	});
    	
    	// Sprite de A08P05:
    	this.A08P05 = cc.Sprite.create(res.A08P05_P1);        
    	this.A08P05.attr({
        	x: 1200,
        	y: 380,
        	scale: 1.1,
        	tag: "A08P05"
    	});

    	// Sprite de A08P06:
    	this.A08P06 = cc.Sprite.create(res.A08P06_P1);        
    	this.A08P06.attr({
        	x: 1200,
        	y: 380,
        	scale: 1.1,
        	tag: "A08P06"
    	});

    	// Sprite de A08P07:
    	this.A08P07 = cc.Sprite.create(res.A08P07_P1);        
    	this.A08P07.attr({
        	x: 1200,
        	y: 320,
        	scale: 1.1,
        	tag: "A08P07"
    	});

    	// Sprite de A08P08:
    	this.A08P08 = cc.Sprite.create(res.A08P08_P1);        
    	this.A08P08.attr({
        	x: 1200,
        	y: 380,
        	scale: 1.1,
        	tag: "A08P08"
    	});

		// Sprite de A08P09:
    	this.A08P09 = cc.Sprite.create(res.A08P09_P1);        
    	this.A08P09.attr({
        	x: 1200,
        	y: 320,
        	scale: 1.1,
        	tag: "A08P09"
    	});

    	// Sprite de A08P10:
    	this.A08P10 = cc.Sprite.create(res.A08P10_P1);        
    	this.A08P10.attr({
        	x: 1250,
        	y: 280,
        	scale: 0.9,
        	tag: "A08P10"
    	});

    	// Sprite de A08P11:
    	this.A08P11 = cc.Sprite.create(res.A08P11_P1);        
    	this.A08P11.attr({
        	x: 1220,
        	y: 320,
        	scale: 1.1,
        	tag: "A08P11"
    	});
//	Sprite de A09 COME:
		// Sprite de A09P01 estan en A03P01:

    	// Sprite de A09P02 estan en A03P02:

    	// Sprite de A09P03:
    	this.A03P03 = cc.Sprite.create(res.A03P03_P1);        
    	this.A03P03.attr({
        	x: x,
        	y: y_Aves,
        	tag: "A03P03"
    	});

    	// Sprite de A09P04 estan en A03P04:
    	
    	
    	// Sprite de A02P05 estan en A03P05:
    	
    	// Sprite de A09P06 estan en A03P06:

    	// Sprite de A09P07:
    	this.A03P07 = cc.Sprite.create(res.A03P07_P1);        
    	this.A03P07.attr({
        	x: x,
        	y: y_Aves-20,
        	scale: 1.1,
        	tag: "A03P07"
    	});

    	// Sprite de A09P08 estan en A03P08:

		// Sprite de A09P09:
    	this.A03P09 = cc.Sprite.create(res.A03P09_P1);        
    	this.A03P09.attr({
        	x: x,
        	y: y_Aves-20,
        	scale: 1.1,
        	tag: "A03P09"
    	});

    	// Sprite de A09P10:
    	this.A03P10 = cc.Sprite.create(res.A03P10_P1);        
    	this.A03P10.attr({
        	x: x+60,
        	y: z-50,
        	scale: 1.1,
        	tag: "A03P10"
    	});

    	// Sprite de A09P11:
    	this.A03P11 = cc.Sprite.create(res.A03P11_P1);        
    	this.A03P11.attr({
        	x: x,
        	y: y_Aves-20,
        	scale: 1.1,
        	tag: "A03P11"
    	});
//	Sprite de A10 BEBE:
		// Sprite de A10P01:
    	this.A10P01 = cc.Sprite.create(res.A10P01_P1);        
    	this.A10P01.attr({
        	x: x,
        	y: y-10,
        	tag: "A10P01"
    	});

    	// Sprite de A10P02:
    	this.A10P02 = cc.Sprite.create(res.A10P02_P1);        
    	this.A10P02.attr({
        	x: x,
        	y: y-10,
        	tag: "A10P02"
    	});

    	// Sprite de A10P03:
    	this.A10P03 = cc.Sprite.create(res.A10P03_P1);        
    	this.A10P03.attr({
        	x: x,
        	y: y_Aves,
        	scale: 0.9,
        	tag: "A10P03"
    	});

    	// Sprite de A10P04:
    	this.A10P04 = cc.Sprite.create(res.A10P04_P1);        
    	this.A10P04.attr({
        	x: x,
        	y: y-10,
        	tag: "A10P04"
    	});
    	
    	// Sprite de A10P05:
    	this.A10P05 = cc.Sprite.create(res.A10P05_P1);        
    	this.A10P05.attr({
        	x: x,
        	y: y-10,
        	tag: "A10P05"
    	});

    	// Sprite de A10P06:
    	this.A10P06 = cc.Sprite.create(res.A10P06_P1);        
    	this.A10P06.attr({
        	x: x,
        	y: y-10,
        	tag: "A10P06"
    	});

    	// Sprite de A10P07:
    	this.A10P07 = cc.Sprite.create(res.A10P07_P1);        
    	this.A10P07.attr({
        	x: x,
        	y: y_Aves-20,
        	scale: 1.1,
        	tag: "A10P07"
    	});

    	// Sprite de A10P08:
    	this.A10P08 = cc.Sprite.create(res.A10P08_P1);        
    	this.A10P08.attr({
        	x: x,
        	y: y-10,
        	tag: "A10P08"
    	});

		// Sprite de A10P09:
    	this.A10P09 = cc.Sprite.create(res.A10P09_P1);        
    	this.A10P09.attr({
        	x: x,
        	y: y_Aves-20,
        	scale: 1.1,
        	tag: "A10P09"
    	});

    	// Sprite de A10P10:
    	this.A10P10 = cc.Sprite.create(res.A10P10_P1);        
    	this.A10P10.attr({
        	x: x+50,
        	y: z-50,
        	scale: 1.1,
        	tag: "A10P10"
    	});

    	// Sprite de A10P11:
    	this.A10P11 = cc.Sprite.create(res.A10P11_P1);        
    	this.A10P11.attr({
        	x: x+10,
        	y: y_Aves-20,
        	scale: 1.1,
        	tag: "A10P11"
    	});
//	Sprite de A11 CANTA:
		// Sprite de A11P01:
    	this.A11P01 = cc.Sprite.create(res.A11P01_P1);        
    	this.A11P01.attr({
        	x: x,
			y: y+20,
        	tag: "A11P01"
    	});

    	// Sprite de A11P02:
    	this.A11P02 = cc.Sprite.create(res.A11P02_P1);        
    	this.A11P02.attr({
        	x: x,
			y: y+20,
        	tag: "A11P02"
    	});

    	// Sprite de A11P03:
    	this.A11P03 = cc.Sprite.create(res.A11P03_P1);        
    	this.A11P03.attr({
        	x: x+20,
        	y: y_Aves+50,
        	scale: 0.9,
        	tag: "A11P03"
    	});

    	// Sprite de A11P04:
    	this.A11P04 = cc.Sprite.create(res.A11P04_P1);        
    	this.A11P04.attr({
        	x: x,
			y: y+20,
        	tag: "A11P04"
    	});
    	
    	// Sprite de A11P05:
    	this.A11P05 = cc.Sprite.create(res.A11P05_P1);        
    	this.A11P05.attr({
        	x: x,
			y: y+20,
        	tag: "A11P05"
    	});

    	// Sprite de A11P06:
    	this.A11P06 = cc.Sprite.create(res.A11P06_P1);        
    	this.A11P06.attr({
        	x: x,
			y: y+20,
        	tag: "A11P06"
    	});

    	// Sprite de A11P07:
    	this.A11P07 = cc.Sprite.create(res.A11P07_P1);        
    	this.A11P07.attr({
        	x: x,
        	y: y_Aves+40,
        	tag: "A11P07"
    	});

    	// Sprite de A11P08:
    	this.A11P08 = cc.Sprite.create(res.A11P08_P1);        
    	this.A11P08.attr({
        	x: x,
			y: y+20,
        	tag: "A11P08"
    	});

		// Sprite de A11P09:
    	this.A11P09 = cc.Sprite.create(res.A11P09_P1);        
    	this.A11P09.attr({
        	x: x,
        	y: y_Aves+40,
        	tag: "A11P09"
    	});

    	this.A11P10 = cc.Sprite.create(res.A11P10_P1);        
    	this.A11P10.attr({
        	x: x+10,
        	y: z,
        	tag: "A11P10"
    	});

    	this.A11P11 = cc.Sprite.create(res.A11P11_P1);        
    	this.A11P11.attr({
        	x: x,
        	y: y_Aves+40,
        	tag: "A11P11"
    	});

//	Movimiento A02 CORRER:
		var move_A02_1 = cc.MoveBy.create(1.99, 850, 0);
		var move_A02_2 = cc.MoveBy.create(0.01, -850, 0);
		var move_A02_sequence = cc.Sequence.create(move_A02_1, move_A02_2);
		var move_A02_repeat = cc.RepeatForever.create(move_A02_sequence);
//	Movimiento A04_P1,2,4,5,6,8:
		var move_A04_1 = cc.MoveBy.create(0.5, 0, 60);
		var move_A04_2 = cc.MoveBy.create(0.5, 0, -60);
		var move_A04_sequence = cc.Sequence.create(move_A04_1, move_A04_2);
		var move_A04_repeat = cc.RepeatForever.create(move_A04_sequence);
//	Movimiento A04P03 BRINCA:
		var move_A04P03_1 = cc.JumpTo.create(0.9, cc.p(1600, 320), 50, 1);
		var move_A04P03_2 = cc.MoveBy.create(0.29, 0, 0);
		var move_A04P03_3 = cc.MoveBy.create(0.01, -400, 0);
		var move_A04P03_sequence = cc.Sequence.create(move_A04P03_1, move_A04P03_2, move_A04P03_3);
		var move_A04P03_repeat = cc.RepeatForever.create(move_A04P03_sequence);	
//	Movimiento A04P7,9,11:
		var move_A04P7_1 = cc.JumpTo.create(5, cc.p(x+80, y_Aves), 150, 10);
		var move_A04P7_2 = cc.JumpTo.create(5, cc.p(x-80, y_Aves), 150, 10);
		var move_A04P7_sequence = cc.Sequence.create(move_A04P7_1, move_A04P7_2);
		var move_A04P7_repeat = cc.RepeatForever.create(move_A04P7_sequence);
//	Movimiento A04P10:
		var move_A04P10_1 = cc.MoveBy.create(0.3, 0, 0);
		var move_A04P10_2 = cc.MoveBy.create(0.3, 0, 15);
		var move_A04P10_3 = cc.MoveBy.create(0.29, 0, 15);
		var move_A04P10_4 = cc.MoveBy.create(0.01, 0, -30);
		var move_A04P10_sequence = cc.Sequence.create(move_A04P10_1, move_A04P10_2, move_A04P10_3, move_A04P10_4);
		var move_A04P10_repeat = cc.RepeatForever.create(move_A04P10_sequence);
//	Movimiento A05_P1,2,3,4,5,6,8:
		var move_A05P1_1 = cc.MoveBy.create(2, 0, 60);
		var move_A05P1_2 = cc.MoveBy.create(1, 0, -60);
		var move_A05P1_sequence = cc.Sequence.create(move_A05P1_1, move_A05P1_2);
		var move_A05P1_repeat = cc.RepeatForever.create(move_A05P1_sequence);
//	Movimiento A05_P07,09,10,11:
	var move_A05_1 = cc.MoveBy.create(3.99, 800, 0);
	var move_A05_2 = cc.MoveBy.create(0.01, -800, 0);
	var move_A05_sequence = cc.Sequence.create(move_A05_1, move_A05_2);
	var move_A05_repeat = cc.RepeatForever.create(move_A05_sequence);
//	Movimiento A08P01,02,04,05,06,08:
		var move_A08P01_1 = cc.MoveBy.create(0.29, 0, 0);
		var move_A08P01_2 = cc.MoveBy.create(0.01, 110, 25);
		var move_A08P01_3 = cc.MoveBy.create(0.29, 0, 0);
		var move_A08P01_4 = cc.MoveBy.create(0.01, 110, 25);
		var move_A08P01_5 = cc.MoveBy.create(0.29, 0, 0);
		var move_A08P01_6 = cc.MoveBy.create(0.01, 110, 25);
		var move_A08P01_7 = cc.MoveBy.create(0.29, 0, 0);
		var move_A08P01_8 = cc.MoveBy.create(1.2, 0, -75);
		var move_A08P01_9 = cc.MoveBy.create(0.3, 0, 0);
		var move_A08P01_10 = cc.MoveBy.create(0.01, -110, 25);
		var move_A08P01_11 = cc.MoveBy.create(0.29, 0, 0);
		var move_A08P01_12 = cc.MoveBy.create(0.01, -110, 25);
		var move_A08P01_13 = cc.MoveBy.create(0.29, 0, 0);
		var move_A08P01_14 = cc.MoveBy.create(0.01, -110, 25);
		var move_A08P01_15 = cc.MoveBy.create(0.28, 0, 0);
		var move_A08P01_16 = cc.MoveBy.create(0.01, 0, -75);
		var move_A08P01_sequence = cc.Sequence.create(move_A08P01_1, move_A08P01_2,  
			move_A08P01_3, move_A08P01_4, move_A08P01_5, move_A08P01_6, move_A08P01_7, 
			move_A08P01_8, move_A08P01_9, move_A08P01_10, move_A08P01_11, move_A08P01_12, 
			move_A08P01_13, move_A08P01_14, move_A08P01_15, move_A08P01_16);
		var move_A08P01_repeat = cc.RepeatForever.create(move_A08P01_sequence);
//	Movimiento A08P03:	
		var move_A08P03_1 = cc.MoveBy.create(0.91, 80, 20);
		var move_A08P03_2 = cc.MoveBy.create(0.29, 0, 0);
		var move_A08P03_3 = cc.MoveBy.create(0.01, 70, 20);
		var move_A08P03_4 = cc.MoveBy.create(0.29, 0, 0);
		var move_A08P03_5 = cc.MoveBy.create(0.01, 70, 20);
		var move_A08P03_6 = cc.MoveBy.create(0.29, 0, 0);
		var move_A08P03_7 = cc.MoveBy.create(0.01, 70, 20);
		var move_A08P03_8 = cc.MoveBy.create(0.29, 0, 0);
		var move_A08P03_9 = cc.MoveBy.create(0.01, 70, 20);
		var move_A08P03_10 = cc.MoveBy.create(0.29, 0, 0);

		var move_A08P03_11 = cc.MoveBy.create(1.2, 0, -100);
		var move_A08P03_12 = cc.MoveBy.create(0.91, -80, 20);
		var move_A08P03_13 = cc.MoveBy.create(0.29, 0, 0);
		var move_A08P03_14 = cc.MoveBy.create(0.01, -70, 20);
		var move_A08P03_15 = cc.MoveBy.create(0.29, 0, 0);
		var move_A08P03_16 = cc.MoveBy.create(0.01, -70, 20);
		var move_A08P03_17 = cc.MoveBy.create(0.29, 0, 0);
		var move_A08P03_18 = cc.MoveBy.create(0.01, -70, 20);
		var move_A08P03_19 = cc.MoveBy.create(0.29, 0, 0);
		var move_A08P03_20 = cc.MoveBy.create(0.01, -70, 20);
		var move_A08P03_21 = cc.MoveBy.create(0.28, 0, 0);
		var move_A08P03_22 = cc.MoveBy.create(0.01, 0, -100);

		var move_A08P03_sequence = cc.Sequence.create(move_A08P03_1, move_A08P03_2,  move_A08P03_3, move_A08P03_4,
			move_A08P03_5, move_A08P03_6,  move_A08P03_7, move_A08P03_8,
			move_A08P03_9, move_A08P03_10,  move_A08P03_11, move_A08P03_12,
			move_A08P03_13, move_A08P03_14,  move_A08P03_15, move_A08P03_16,
			move_A08P03_17, move_A08P03_18,  move_A08P03_19, move_A08P03_20,move_A08P03_21, move_A08P03_22);
		var move_A08P03_repeat = cc.RepeatForever.create(move_A08P03_sequence);				
// Botones
        switch (type)
        {
            case ccui.Widget.TOUCH_BEGAN:

            	if(object_name == "Button_before"){
            		cc.audioEngine.stopAllEffects(); // Stop Efectos de sonido
            		//	ESCENA DE INTRODUCCION:
            		cc.director.runScene(HelloWorldScene3.scene());
            	}

            //	BOTONES PARTE 1:
            	if(object_name == "btn_up_p1"){
            		if(Anterior_p1 == 1){
            			this.removeChildByTag("FL1");
            		}
            		else if (Anterior_p1 == 2){
            			this.removeChildByTag("FL2");
            		}
            		else if (Anterior_p1 == 3){
            			this.removeChildByTag("FL3");
            		}
            		else if (Anterior_p1 == 4){
            			this.removeChildByTag("FL4");
            		}
            		else if (Anterior_p1 == 5){
            			this.removeChildByTag("FL5");
            		}
            		else if (Anterior_p1 == 6){
            			this.removeChildByTag("FL6");
            		}
            		else if (Anterior_p1 == 7){
            			this.removeChildByTag("FL7");
            		}
            		else if (Anterior_p1 == 8){
            			this.removeChildByTag("FL8");
            		}
            		else if (Anterior_p1 == 9){
            			this.removeChildByTag("FL9");
            		}
            		else if (Anterior_p1 == 10){
            			this.removeChildByTag("FL10");
            		}
            		else if (Anterior_p1 == 11){
            			this.removeChildByTag("FL11");
            		}
            		if(cont_p1 == 1){
            			cont_p1 = 12;
            		}

            		cont_p1 = cont_p1 - 1;

            		if(cont_p1 == 1){
            			this.addChild(FL1, 0);
            		}
            		else if (cont_p1 == 2){
            			this.addChild(FL2, 0);
            		}
            		else if (cont_p1 == 3){
            			this.addChild(FL3, 0);
            		}
            		else if (cont_p1 == 4){
            			this.addChild(FL4, 0);
            		}
            		else if (cont_p1 == 5){
            			this.addChild(FL5, 0);
            		}
            		else if (cont_p1 == 6){
            			this.addChild(FL6, 0);
            		}
            		else if (cont_p1 == 7){
            			this.addChild(FL7, 0);
            		}
            		else if (cont_p1 == 8){
            			this.addChild(FL8, 0);
            		}
            		else if (cont_p1 == 9){
            			this.addChild(FL9, 0);
            		}
            		else if (cont_p1 == 10){
            			this.addChild(FL10, 0);
            		}
            		else if (cont_p1 == 11){
            			this.addChild(FL11, 0);
            		}
            		Anterior_p1 = cont_p1;          		
            	}
            	if (object_name == "btn_down_p1"){
            		
            		if(Anterior_p1 == 1){
            			this.removeChildByTag("FL1");
            		}
            		else if (Anterior_p1 == 2){
            			this.removeChildByTag("FL2");
            		}
            		else if (Anterior_p1 == 3){
            			this.removeChildByTag("FL3");
            		}
            		else if (Anterior_p1 == 4){
            			this.removeChildByTag("FL4");
            		}
            		else if (Anterior_p1 == 5){
            			this.removeChildByTag("FL5");
            		}
            		else if (Anterior_p1 == 6){
            			this.removeChildByTag("FL6");
            		}
            		else if (Anterior_p1 == 7){
            			this.removeChildByTag("FL7");
            		}
            		else if (Anterior_p1 == 8){
            			this.removeChildByTag("FL8");
            		}
            		else if (Anterior_p1 == 9){
            			this.removeChildByTag("FL9");
            		}
            		else if (Anterior_p1 == 10){
            			this.removeChildByTag("FL10");
            		}
            		else if (Anterior_p1 == 11){
            			this.removeChildByTag("FL11");
            		}
            		if(cont_p1 == 11){
            			cont_p1 = 0;
            		}

            		cont_p1 = cont_p1 + 1;

            		if(cont_p1 == 1){
            			this.addChild(FL1, 0);
            		}
            		else if (cont_p1 == 2){
            			this.addChild(FL2, 0);
            		}
            		else if (cont_p1 == 3){
            			this.addChild(FL3, 0);
            		}
            		else if (cont_p1 == 4){
            			this.addChild(FL4, 0);
            		}
            		else if (cont_p1 == 5){
            			this.addChild(FL5, 0);
            		}
            		else if (cont_p1 == 6){
            			this.addChild(FL6, 0);
            		}
            		else if (cont_p1 == 7){
            			this.addChild(FL7, 0);
            		}
            		else if (cont_p1 == 8){
            			this.addChild(FL8, 0);
            		}
            		else if (cont_p1 == 9){
            			this.addChild(FL9, 0);
            		}
            		else if (cont_p1 == 10){
            			this.addChild(FL10, 0);
            		}
            		else if (cont_p1 == 11){
            			this.addChild(FL11, 0);
            		}
            		Anterior_p1 = cont_p1;  
            	}

            //	BOTONES PARTE 2:

            	if(object_name == "btn_up_p2"){

            		if(Anterior_p2 == 1){
            			this.removeChildByTag("FA1");
            		}
            		else if (Anterior_p2 == 2){
            			this.removeChildByTag("FA2");
            		}
            		else if (Anterior_p2 == 3){
            			this.removeChildByTag("FA3");
            		}
            		else if (Anterior_p2 == 4){
            			this.removeChildByTag("FA4");
            		}
            		else if (Anterior_p2 == 5){
            			this.removeChildByTag("FA5");
            		}
            		else if (Anterior_p2 == 6){
            			this.removeChildByTag("FA6");
            		}
            		else if (Anterior_p2 == 7){
            			this.removeChildByTag("FA7");
            		}
            		else if (Anterior_p2 == 8){
            			this.removeChildByTag("FA8");
            		}
            		else if (Anterior_p2 == 9){
            			this.removeChildByTag("FA9");
            		}
            		else if (Anterior_p2 == 10){
            			this.removeChildByTag("FA10");
            		}
            		else if (Anterior_p2 == 11){
            			this.removeChildByTag("FA11");
            		}
            		if(cont_p2 == 1){
            			cont_p2 = 12;
            		}

            		cont_p2 = cont_p2 - 1;

            		if(cont_p2 == 1){
            			this.addChild(FA1, 0);
            		}
            		else if (cont_p2 == 2){
            			this.addChild(FA2, 0);
            		}
            		else if (cont_p2 == 3){
            			this.addChild(FA3, 0);
            		}
            		else if (cont_p2 == 4){
            			this.addChild(FA4, 0);
            		}
            		else if (cont_p2 == 5){
            			this.addChild(FA5, 0);
            		}
            		else if (cont_p2 == 6){
            			this.addChild(FA6, 0);
            		}
            		else if (cont_p2 == 7){
            			this.addChild(FA7, 0);
            		}
            		else if (cont_p2 == 8){
            			this.addChild(FA8, 0);
            		}
            		else if (cont_p2 == 9){
            			this.addChild(FA9, 0);
            		}
            		else if (cont_p2 == 10){
            			this.addChild(FA10, 0);
            		}
            		else if (cont_p2 == 11){
            			this.addChild(FA11, 0);
            		}
            		Anterior_p2 = cont_p2; 
        		}

            	if(object_name == "btn_down_p2"){
            		if(Anterior_p2 == 1){
            			this.removeChildByTag("FA1");
            		}
            		else if (Anterior_p2 == 2){
            			this.removeChildByTag("FA2");
            		}
            		else if (Anterior_p2 == 3){
            			this.removeChildByTag("FA3");
            		}
            		else if (Anterior_p2 == 4){
            			this.removeChildByTag("FA4");
            		}
            		else if (Anterior_p2 == 5){
            			this.removeChildByTag("FA5");
            		}
            		else if (Anterior_p2 == 6){
            			this.removeChildByTag("FA6");
            		}
            		else if (Anterior_p2 == 7){
            			this.removeChildByTag("FA7");
            		}
            		else if (Anterior_p2 == 8){
            			this.removeChildByTag("FA8");
            		}
            		else if (Anterior_p2 == 9){
            			this.removeChildByTag("FA9");
            		}
            		else if (Anterior_p2 == 10){
            			this.removeChildByTag("FA10");
            		}
            		else if (Anterior_p2 == 11){
            			this.removeChildByTag("FA11");
            		}

            		if(cont_p2 == 11){
            			cont_p2 = 0;
            		}

            		cont_p2 = cont_p2 + 1;

            		if(cont_p2 == 1){
            			this.addChild(FA1, 0);
            		}
            		else if (cont_p2 == 2){
            			this.addChild(FA2, 0);
            		}
            		else if (cont_p2 == 3){
            			this.addChild(FA3, 0);
            		}
            		else if (cont_p2 == 4){
            			this.addChild(FA4, 0);
            		}
            		else if (cont_p2 == 5){
            			this.addChild(FA5, 0);
            		}
            		else if (cont_p2 == 6){
            			this.addChild(FA6, 0);
            		}
            		else if (cont_p2 == 7){
            			this.addChild(FA7, 0);
            		}
            		else if (cont_p2 == 8){
            			this.addChild(FA8, 0);
            		}
            		else if (cont_p2 == 9){
            			this.addChild(FA9, 0);
            		}
            		else if (cont_p2 == 10){
            			this.addChild(FA10, 0);
            		}
            		else if (cont_p2 == 11){
            			this.addChild(FA11, 0);
            		}
            		Anterior_p2 = cont_p2; 
        		}

        	//	BOTONES PARTE 3:
            	if(object_name == "btn_up_p3"){
            		if(Anterior_p3 == 1){
            			this.removeChildByTag("FP1");
            		}
            		else if (Anterior_p3 == 2){
            			this.removeChildByTag("FP2");
            		}
            		else if (Anterior_p3 == 3){
            			this.removeChildByTag("FP3");
            		}
            		else if (Anterior_p3 == 4){
            			this.removeChildByTag("FP4");
            		}
            		else if (Anterior_p3 == 5){
            			this.removeChildByTag("FP5");
            		}
            		else if (Anterior_p3 == 6){
            			this.removeChildByTag("FP6");
            		}
            		else if (Anterior_p3 == 7){
            			this.removeChildByTag("FP7");
            		}
            		else if (Anterior_p3 == 8){
            			this.removeChildByTag("FP8");
            		}
            		else if (Anterior_p3 == 9){
            			this.removeChildByTag("FP9");
            		}
            		else if (Anterior_p3 == 10){
            			this.removeChildByTag("FP10");
            		}
            		else if (Anterior_p3 == 11){
            			this.removeChildByTag("FP11");
            		}
            		if(cont_p3 == 1){
            			cont_p3 = 12;
            		}

            		cont_p3 = cont_p3 - 1;

            		if(cont_p3 == 1){
            			this.addChild(FP1, 0);
            		}
            		else if (cont_p3 == 2){
            			this.addChild(FP2, 0);
            		}
            		else if (cont_p3 == 3){
            			this.addChild(FP3, 0);
            		}
            		else if (cont_p3 == 4){
            			this.addChild(FP4, 0);
            		}
            		else if (cont_p3 == 5){
            			this.addChild(FP5, 0);
            		}
            		else if (cont_p3 == 6){
            			this.addChild(FP6, 0);
            		}
            		else if (cont_p3 == 7){
            			this.addChild(FP7, 0);
            		}
            		else if (cont_p3 == 8){
            			this.addChild(FP8, 0);
            		}
            		else if (cont_p3 == 9){
            			this.addChild(FP9, 0);
            		}
            		else if (cont_p3 == 10){
            			this.addChild(FP10, 0);
            		}
            		else if (cont_p3 == 11){
            			this.addChild(FP11, 0);
            		}
            		Anterior_p3 = cont_p3; 
        		}

            	if(object_name == "btn_down_p3"){
            		if(Anterior_p3 == 1){
            			this.removeChildByTag("FP1");
            		}
            		else if (Anterior_p3 == 2){
            			this.removeChildByTag("FP2");
            		}
            		else if (Anterior_p3 == 3){
            			this.removeChildByTag("FP3");
            		}
            		else if (Anterior_p3 == 4){
            			this.removeChildByTag("FP4");
            		}
            		else if (Anterior_p3 == 5){
            			this.removeChildByTag("FP5");
            		}
            		else if (Anterior_p3 == 6){
            			this.removeChildByTag("FP6");
            		}
            		else if (Anterior_p3 == 7){
            			this.removeChildByTag("FP7");
            		}
            		else if (Anterior_p3 == 8){
            			this.removeChildByTag("FP8");
            		}
            		else if (Anterior_p3 == 9){
            			this.removeChildByTag("FP9");
            		}
            		else if (Anterior_p3 == 10){
            			this.removeChildByTag("FP10");
            		}
            		else if (Anterior_p3 == 11){
            			this.removeChildByTag("FP11");
            		}
            		if(cont_p3 == 11){
            			cont_p3 = 0;
            		}

            		cont_p3 = cont_p3 + 1;

            		if(cont_p3 == 1){
            			this.addChild(FP1, 0);
            		}
            		else if (cont_p3 == 2){
            			this.addChild(FP2, 0);
            		}
            		else if (cont_p3 == 3){
            			this.addChild(FP3, 0);
            		}
            		else if (cont_p3 == 4){
            			this.addChild(FP4, 0);
            		}
            		else if (cont_p3 == 5){
            			this.addChild(FP5, 0);
            		}
            		else if (cont_p3 == 6){
            			this.addChild(FP6, 0);
            		}
            		else if (cont_p3 == 7){
            			this.addChild(FP7, 0);
            		}
            		else if (cont_p3 == 8){
            			this.addChild(FP8, 0);
            		}
            		else if (cont_p3 == 9){
            			this.addChild(FP9, 0);
            		}
            		else if (cont_p3 == 10){
            			this.addChild(FP10, 0);
            		}
            		else if (cont_p3 == 11){
            			this.addChild(FP11, 0);
            		}
            		Anterior_p3 = cont_p3;   	
        		}
        		if(object_name == "btn_virgula"){
        			cc.audioEngine.stopAllEffects(); // Stop Efectos de sonido

        			if(b_anuncio == 0){
        				this.removeChildByTag("Anuncio");
        				b_anuncio = 1;
        			}
//	Remover:
    				
    				if(anterior_p2 == 1){
            			this.removeChildByTag("TA1");
        			}
        			else if(anterior_p2 == 2){
            			this.removeChildByTag("TA2");
        			}
        			else if(anterior_p2 == 3){
            			this.removeChildByTag("TA3");      				
        			}
        			else if(anterior_p2 == 4){
            			this.removeChildByTag("TA4");        				
        			}
        			else if(anterior_p2 == 5){
            			this.removeChildByTag("TA5");        				
        			}
        			else if(anterior_p2 == 6){
            			this.removeChildByTag("TA6");        				
        			}
        			else if(anterior_p2 == 7){
            			this.removeChildByTag("TA7");       				
        			}
        			else if(anterior_p2 == 8){
            			this.removeChildByTag("TA8");        				
        			}
        			else if(anterior_p2 == 9){
            			this.removeChildByTag("TA9");        				
        			}
        			else if(anterior_p2 == 10){
            			this.removeChildByTag("TA10");        				
        			}
        			else if(anterior_p2 == 11){
            			this.removeChildByTag("TA11");        				
        			}
        			
        			if(anterior_p1 == 1){
						this.removeChildByTag("IL1");
            			this.removeChildByTag("TL1");
        			}
        			else if(anterior_p1 == 2){
        				this.removeChildByTag("IL2");
            			this.removeChildByTag("TL2");
        			}
        			else if(anterior_p1 == 3){
        				this.removeChildByTag("IL3");
            			this.removeChildByTag("TL3");      				
        			}
        			else if(anterior_p1 == 4){
        				this.removeChildByTag("IL4");
            			this.removeChildByTag("TL4");        				
        			}
        			else if(anterior_p1 == 5){
        				this.removeChildByTag("IL5");
            			this.removeChildByTag("TL5");        				
        			}
        			else if(anterior_p1 == 6){
        				this.removeChildByTag("IL6");
            			this.removeChildByTag("TL6");        				
        			}
        			else if(anterior_p1 == 7){
        				this.removeChildByTag("IL7");
            			this.removeChildByTag("TL7");       				
        			}
        			else if(anterior_p1 == 8){
        				this.removeChildByTag("IL8");
            			this.removeChildByTag("TL8");        				
        			}
        			else if(anterior_p1 == 9){
        				this.removeChildByTag("IL9");
            			this.removeChildByTag("TL9");        				
        			}
        			else if(anterior_p1 == 10){
        				this.removeChildByTag("IL10");
            			this.removeChildByTag("TL10");        				
        			}
        			else if(anterior_p1 == 11){
        				this.removeChildByTag("IL11");
            			this.removeChildByTag("TL11");        				
        			}

        			
        			if(anterior_p3 == 1){
            			this.removeChildByTag("TP1");
        			}
        			else if(anterior_p3 == 2){
            			this.removeChildByTag("TP2");
        			}
        			else if(anterior_p3 == 3){
            			this.removeChildByTag("TP3");      				
        			}
        			else if(anterior_p3 == 4){
            			this.removeChildByTag("TP4");        				
        			}
        			else if(anterior_p3 == 5){
            			this.removeChildByTag("TP5");        				
        			}
        			else if(anterior_p3 == 6){
            			this.removeChildByTag("TP6");        				
        			}
        			else if(anterior_p3 == 7){
            			this.removeChildByTag("TP7");       				
        			}
        			else if(anterior_p3 == 8){
            			this.removeChildByTag("TP8");        				
        			}
        			else if(anterior_p3 == 9){
            			this.removeChildByTag("TP9");        				
        			}
        			else if(anterior_p3 == 10){
            			this.removeChildByTag("TP10");        				
        			}
        			else if(anterior_p3 == 11){
            			this.removeChildByTag("TP11");        				
        			}
//	Sonidos Lugar:
        			if(cont_p1 == 1){
						this.addChild(IL1, 0);
            			this.addChild(TL1, 0);
            			cc.audioEngine.playEffect(res.SL1, false);
        			}
        			else if(cont_p1 == 2){
        				this.addChild(IL2, 0);
            			this.addChild(TL2, 0);
            			cc.audioEngine.playEffect(res.SL2, false);
        			}
        			else if(cont_p1 == 3){
        				this.addChild(IL3, 0);
            			this.addChild(TL3, 0);
            			cc.audioEngine.playEffect(res.SL3, false);      				
        			}
        			else if(cont_p1 == 4){
        				this.addChild(IL4, 0);
            			this.addChild(TL4, 0); 
            			cc.audioEngine.playEffect(res.SL4, false);       				
        			}
        			else if(cont_p1 == 5){
        				this.addChild(IL5, 0);
            			this.addChild(TL5, 0);
            			cc.audioEngine.playEffect(res.SL5, false);        				
        			}
        			else if(cont_p1 == 6){
        				this.addChild(IL6, 0);
            			this.addChild(TL6, 0);
            			cc.audioEngine.playEffect(res.SL6, false);        				
        			}
        			else if(cont_p1 == 7){
        				this.addChild(IL7, 0);
            			this.addChild(TL7, 0);
            			cc.audioEngine.playEffect(res.SL7, false);       				
        			}
        			else if(cont_p1 == 8){
        				this.addChild(IL8, 0);
            			this.addChild(TL8, 0);
            			cc.audioEngine.playEffect(res.SL8, false);        				
        			}
        			else if(cont_p1 == 9){
        				this.addChild(IL9, 0);
            			this.addChild(TL9, 0); 
            			cc.audioEngine.playEffect(res.SL9, false);       				
        			}
        			else if(cont_p1 == 10){
        				this.addChild(IL10, 0);
            			this.addChild(TL10, 0);
            			cc.audioEngine.playEffect(res.SL10, false);        				
        			}
        			else if(cont_p1 == 11){
        				this.addChild(IL11, 0);
            			this.addChild(TL11, 0);
            			cc.audioEngine.playEffect(res.SL11, false);        				
        			}
//	Sonido Accion:
        			if(cont_p2 == 1){
            			this.addChild(TA1, 0);
            			cc.audioEngine.playEffect(res.SA1, false);
        			}
        			else if(cont_p2 == 2){
            			this.addChild(TA2, 0);
            			cc.audioEngine.playEffect(res.SA2, false);
        			}
        			else if(cont_p2 == 3){
            			this.addChild(TA3, 0); 
            			cc.audioEngine.playEffect(res.SA3, false);     				
        			}
        			else if(cont_p2 == 4){
            			this.addChild(TA4, 0);
            			cc.audioEngine.playEffect(res.SA4, false);        				
        			}
        			else if(cont_p2 == 5){
            			this.addChild(TA5, 0);  
            			cc.audioEngine.playEffect(res.SA5, false);      				
        			}
        			else if(cont_p2 == 6){
            			this.addChild(TA6, 0);  
            			cc.audioEngine.playEffect(res.SA6, false);      				
        			}
        			else if(cont_p2 == 7){
            			this.addChild(TA7, 0); 
            			cc.audioEngine.playEffect(res.SA7, false);      				
        			}
        			else if(cont_p2 == 8){
            			this.addChild(TA8, 0);
            			cc.audioEngine.playEffect(res.SA8, false);        				
        			}
        			else if(cont_p2 == 9){
            			this.addChild(TA9, 0);    
            			cc.audioEngine.playEffect(res.SA9, false);    				
        			}
        			else if(cont_p2 == 10){
            			this.addChild(TA10, 0); 
            			cc.audioEngine.playEffect(res.SA10, false);       				
        			}
        			else if(cont_p2 == 11){
            			this.addChild(TA11, 0); 
            			cc.audioEngine.playEffect(res.SA11, false);       				
        			}
//	Sonido Personajes
        			if(cont_p3 == 1){
            			this.addChild(TP1, 0);
            			cc.audioEngine.playEffect(res.SP1, false);
        			}
        			else if(cont_p3 == 2){
            			this.addChild(TP2, 0);
            			cc.audioEngine.playEffect(res.SP2, false);
        			}
        			else if(cont_p3 == 3){
            			this.addChild(TP3, 0);   
            			cc.audioEngine.playEffect(res.SP3, false);   				
        			}
        			else if(cont_p3 == 4){
            			this.addChild(TP4, 0); 
            			cc.audioEngine.playEffect(res.SP4, false);       				
        			}
        			else if(cont_p3 == 5){
            			this.addChild(TP5, 0); 
            			cc.audioEngine.playEffect(res.SP5, false);       				
        			}
        			else if(cont_p3 == 6){
            			this.addChild(TP6, 0); 
            			cc.audioEngine.playEffect(res.SP6, false);       				
        			}
        			else if(cont_p3 == 7){
            			this.addChild(TP7, 0); 
            			cc.audioEngine.playEffect(res.SP7, false);      				
        			}
        			else if(cont_p3 == 8){
            			this.addChild(TP8, 0);  
            			cc.audioEngine.playEffect(res.SP8, false);      				
        			}
        			else if(cont_p3 == 9){
            			this.addChild(TP9, 0); 
            			cc.audioEngine.playEffect(res.SP9, false);       				
        			}
        			else if(cont_p3 == 10){
            			this.addChild(TP10, 0);  
            			cc.audioEngine.playEffect(res.SP10, false);      				
        			}
        			else if(cont_p3 == 11){
            			this.addChild(TP11, 0); 
            			cc.audioEngine.playEffect(res.SP11, false);       				
        			}

//	Efectos
					//	Duerme:
					if(cont_p2 == 1 && (cont_p3 == 1 || cont_p3 == 2 || cont_p3 == 4 || cont_p3 == 5 || cont_p3 == 6 || cont_p3 == 8)){
            			cc.audioEngine.playEffect(res.E_A01P01, false);
					}
					else if(cont_p2 == 1 && (cont_p3 == 3 || cont_p3 == 7 || cont_p3 == 9 || cont_p3 == 10 || cont_p3 == 11)){
            			cc.audioEngine.playEffect(res.E_A01P07, false);
					}


					//	Muerde:
					if(cont_p2 == 2){
						cc.audioEngine.playEffect(res.E_A02, false);
					}
					//	Muerde:
					if(cont_p2 == 3 && (cont_p3 == 1 || cont_p3 == 2 || cont_p3 == 3 || cont_p3 == 4 || cont_p3 == 5 || cont_p3 == 6 || cont_p3 == 8 || cont_p3 == 10)){
						cc.audioEngine.playEffect(res.E_A03P01, false);
					}
					else if (cont_p2 == 3 && (cont_p3 == 7 || cont_p3 == 9 || cont_p3 == 11)){
						cc.audioEngine.playEffect(res.E_A03P07, false);
					}
					//	Brinca:
					if(cont_p2 == 4){
						cc.audioEngine.playEffect(res.E_A04, false);
					}
					//	Vuela:
					if(cont_p2 == 5){
						cc.audioEngine.playEffect(res.E_A05, false);
					}
					//	Habla:
					if(cont_p2 == 6 && (cont_p3 == 1 || cont_p3 == 2 || cont_p3 == 4 || cont_p3 == 5 || cont_p3 == 6 || cont_p3 == 8)){
						cc.audioEngine.playEffect(res.E_A06P01, false);
					}
					else if (cont_p2 == 6 && (cont_p3 == 3 || cont_p3 == 7 || cont_p3 == 9 || cont_p3 == 10 || cont_p3 == 11)){
						cc.audioEngine.playEffect(res.E_A06P03_10, false);
					}
					//	Camina
					if (cont_p2 == 7){
						if (cont_p1 == 1 || cont_p1 == 3){
							cc.audioEngine.playEffect(res.E_A07P01_L01, false);
						}
						else if(cont_p1 !== 1 || cont_p1 !== 3){
							cc.audioEngine.playEffect(res.E_A07P01, false);
						}
					}
					//	Cruza:
					if (cont_p2 == 8){
							cc.audioEngine.playEffect(res.E_A08, false);
						}
					//	Come:
					if(cont_p2 == 9 && (cont_p3 == 1 || cont_p3 == 2 || cont_p3 == 3 || cont_p3 == 4 || cont_p3 == 5 || cont_p3 == 6 || cont_p3 == 8)){
						cc.audioEngine.playEffect(res.E_A09P01, false);
					}
					else if (cont_p2 == 9 && (cont_p3 == 7 || cont_p3 == 9 || cont_p3 == 11)){
						cc.audioEngine.playEffect(res.E_A09P07, false);
					}
					else if (cont_p2 == 9 && (cont_p3 == 10)){
						cc.audioEngine.playEffect(res.E_A09P10, false);
					}
					//	Bebe:
					if(cont_p2 == 10 && (cont_p3 == 1 || cont_p3 == 2 || cont_p3 == 4 || cont_p3 == 5 || cont_p3 == 6 || cont_p3 == 8)){
						cc.audioEngine.playEffect(res.E_A10P01, false);
					}
					else if (cont_p2 == 10 && (cont_p3 == 3 || cont_p3 == 7 || cont_p3 == 9 || cont_p3 == 10 || cont_p3 == 11)){
						cc.audioEngine.playEffect(res.E_A10P03, false);
					}
					//	Canta:
					if(cont_p2 == 11 && (cont_p3 == 1 || cont_p3 == 2 || cont_p3 == 6 || cont_p3 == 8)){
						cc.audioEngine.playEffect(res.E_A11P01, false);
					}
					else if (cont_p2 == 11 && (cont_p3 == 4 || cont_p3 == 5)){
						cc.audioEngine.playEffect(res.E_A11P04, false);
					}
					else if (cont_p2 == 11 && cont_p3 == 7){
						cc.audioEngine.playEffect(res.E_A11P07, false);
					}
					else if (cont_p2 == 11 && cont_p3 == 9){
						cc.audioEngine.playEffect(res.E_A11P09, false);
					}
					else if (cont_p2 == 11 && (cont_p3 == 3 || cont_p3 == 10)){
						cc.audioEngine.playEffect(res.E_A11P03_10, false);
					}
					else if (cont_p2 == 11 && cont_p3 == 11){
						cc.audioEngine.playEffect(res.E_A11P11, false);
					}

//	Remover connt_p2 == 1 DUERME:
        			if(anterior_p2 == 1 && anterior_p3 == 1){
        				this.removeChildByTag("A01P01");
        			}
        			else if(anterior_p2 == 1 && anterior_p3 == 2){
        				this.removeChildByTag("A01P02");
        			}
        			else if(anterior_p2 == 1 && anterior_p3 == 3){
        				this.removeChildByTag("A01P03");
        			}
        			else if(anterior_p2 == 1 && anterior_p3 == 4){
        				this.removeChildByTag("A01P04");
        			}
        			else if(anterior_p2 == 1 && anterior_p3 == 5){
        				this.removeChildByTag("A01P05");
        			}
        			else if(anterior_p2 == 1 && anterior_p3 == 6){
        				this.removeChildByTag("A01P06");
        			}
        			else if(anterior_p2 == 1 && anterior_p3 == 7){
        				this.removeChildByTag("A01P07");
        			}
        			else if(anterior_p2 == 1 && anterior_p3 == 8){
        				this.removeChildByTag("A01P08");
        			}
        			else if(anterior_p2 == 1 && anterior_p3 == 9){
        				this.removeChildByTag("A01P09");
        			}
        			else if(anterior_p2 == 1 && anterior_p3 == 10){
        				this.removeChildByTag("A01P10");
        			}
        			else if(anterior_p2 == 1 && anterior_p3 == 11){
        				this.removeChildByTag("A01P11");
        			}
//	Remover connt_p2 == 2 CORRE:
        			
        			if(anterior_p2 == 2 && anterior_p3 == 1){
        				this.removeChildByTag("A02P01");
        			}
        			else if(anterior_p2 == 2 && anterior_p3 == 2){
        				this.removeChildByTag("A02P02");
        			}
        			else if(anterior_p2 == 2 && anterior_p3 == 3){
        				this.removeChildByTag("A02P03");
        			}
        			else if(anterior_p2 == 2 && anterior_p3 == 4){
        				this.removeChildByTag("A02P04");
        			}
        			else if(anterior_p2 == 2 && anterior_p3 == 5){
        				this.removeChildByTag("A02P05");
        			}
        			else if(anterior_p2 == 2 && anterior_p3 == 6){
        				this.removeChildByTag("A02P06");
        			}
        			else if(anterior_p2 == 2 && anterior_p3 == 7){
        				this.removeChildByTag("A02P07");
        			}
        			else if(anterior_p2 == 2 && anterior_p3 == 8){
        				this.removeChildByTag("A02P08");
        			}
        			else if(anterior_p2 == 2 && anterior_p3 == 9){
        				this.removeChildByTag("A02P09");
        			}
        			else if(anterior_p2 == 2 && anterior_p3 == 10){
        				this.removeChildByTag("A02P10");
        			}
        			else if(anterior_p2 == 2 && anterior_p3 == 11){
        				this.removeChildByTag("A02P11");
        			}
//	Remover connt_p2 == 3 MUERDE:
        			
        			if(anterior_p2 == 3 && anterior_p3 == 1){
        				this.removeChildByTag("A03P01");
        			}
        			else if(anterior_p2 == 3 && anterior_p3 == 2){
        				this.removeChildByTag("A03P02");
        			}
        			else if(anterior_p2 == 3 && anterior_p3 == 3){
        				this.removeChildByTag("A09P03");
        			}
        			else if(anterior_p2 == 3 && anterior_p3 == 4){
        				this.removeChildByTag("A03P04");
        			}
        			else if(anterior_p2 == 3 && anterior_p3 == 5){
        				this.removeChildByTag("A03P05");
        			}
        			else if(anterior_p2 == 3 && anterior_p3 == 6){
        				this.removeChildByTag("A03P06");
        			}
        			else if(anterior_p2 == 3 && anterior_p3 == 7){
        				this.removeChildByTag("A09P07");
        			}
        			else if(anterior_p2 == 3 && anterior_p3 == 8){
        				this.removeChildByTag("A03P08");
        			}
        			else if(anterior_p2 == 3 && anterior_p3 == 9){
        				this.removeChildByTag("A09P09");
        			}
        			else if(anterior_p2 == 3 && anterior_p3 == 10){
        				this.removeChildByTag("A09P10");
        			}
        			else if(anterior_p2 == 3 && anterior_p3 == 11){
        				this.removeChildByTag("A09P11");
        			}        			
//	Remover connt_p2 == 4 BRINCA:
        			
        			if(anterior_p2 == 4 && anterior_p3 == 1){
        				this.removeChildByTag("A04P01");
        			}
        			else if(anterior_p2 == 4 && anterior_p3 == 2){
        				this.removeChildByTag("A04P02");
        			}
        			else if(anterior_p2 == 4 && anterior_p3 == 3){
        				this.removeChildByTag("A04P03");
        			}
        			else if(anterior_p2 == 4 && anterior_p3 == 4){
        				this.removeChildByTag("A04P04");
        			}
        			else if(anterior_p2 == 4 && anterior_p3 == 5){
        				this.removeChildByTag("A04P05");
        			}
        			else if(anterior_p2 == 4 && anterior_p3 == 6){
        				this.removeChildByTag("A04P06");
        			}
        			else if(anterior_p2 == 4 && anterior_p3 == 7){
        				this.removeChildByTag("A04P07");
        			}
        			else if(anterior_p2 == 4 && anterior_p3 == 8){
        				this.removeChildByTag("A04P08");
        			}
        			else if(anterior_p2 == 4 && anterior_p3 == 9){
        				this.removeChildByTag("A04P09");
        			}
        			else if(anterior_p2 == 4 && anterior_p3 == 10){
        				this.removeChildByTag("A04P10");
        			}
        			else if(anterior_p2 == 4 && anterior_p3 == 11){
        				this.removeChildByTag("A04P11");
        			}
//	Remover connt_p2 == 5 VUELA:
        			
        			if(anterior_p2 == 5 && anterior_p3 == 1){
        				this.removeChildByTag("A05P01");
        			}
        			else if(anterior_p2 == 5 && anterior_p3 == 2){
        				this.removeChildByTag("A05P02");
        			}
        			else if(anterior_p2 == 5 && anterior_p3 == 3){
        				this.removeChildByTag("A05P03");
        			}
        			else if(anterior_p2 == 5 && anterior_p3 == 4){
        				this.removeChildByTag("A05P04");
        			}
        			else if(anterior_p2 == 5 && anterior_p3 == 5){
        				this.removeChildByTag("A05P05");
        			}
        			else if(anterior_p2 == 5 && anterior_p3 == 6){
        				this.removeChildByTag("A05P06");
        			}
        			else if(anterior_p2 == 5 && anterior_p3 == 7){
        				this.removeChildByTag("A05P07");
        			}
        			else if(anterior_p2 == 5 && anterior_p3 == 8){
        				this.removeChildByTag("A05P08");
        			}
        			else if(anterior_p2 == 5 && anterior_p3 == 9){
        				this.removeChildByTag("A05P09");
        			}
        			else if(anterior_p2 == 5 && anterior_p3 == 10){
        				this.removeChildByTag("A05P10");
        			}
        			else if(anterior_p2 == 5 && anterior_p3 == 11){
        				this.removeChildByTag("A05P11");
        			}
//	Remover connt_p2 == 6 HABLA:
        			
        			if(anterior_p2 == 6 && anterior_p3 == 1){
        				this.removeChildByTag("A06P01");
        			}
        			else if(anterior_p2 == 6 && anterior_p3 == 2){
        				this.removeChildByTag("A06P02");
        			}
        			else if(anterior_p2 == 6 && anterior_p3 == 3){
        				this.removeChildByTag("A06P03");
        			}
        			else if(anterior_p2 == 6 && anterior_p3 == 4){
        				this.removeChildByTag("A06P04");
        			}
        			else if(anterior_p2 == 6 && anterior_p3 == 5){
        				this.removeChildByTag("A06P05");
        			}
        			else if(anterior_p2 == 6 && anterior_p3 == 6){
        				this.removeChildByTag("A06P06");
        			}
        			else if(anterior_p2 == 6 && anterior_p3 == 7){
        				this.removeChildByTag("A06P07");
        			}
        			else if(anterior_p2 == 6 && anterior_p3 == 8){
        				this.removeChildByTag("A06P08");
        			}
        			else if(anterior_p2 == 6 && anterior_p3 == 9){
        				this.removeChildByTag("A06P09");
        			}
        			else if(anterior_p2 == 6 && anterior_p3 == 10){
        				this.removeChildByTag("A06P10");
        			}
        			else if(anterior_p2 == 6 && anterior_p3 == 11){
        				this.removeChildByTag("A06P11");
        			}
//	Remover connt_p2 == 7 CAMINA:
        			
        			if(anterior_p2 == 7 && anterior_p3 == 1){
        				this.removeChildByTag("A07P01");
        			}
        			else if(anterior_p2 == 7 && anterior_p3 == 2){
        				this.removeChildByTag("A07P02");
        			}
        			else if(anterior_p2 == 7 && anterior_p3 == 3){
        				this.removeChildByTag("A07P03");
        			}
        			else if(anterior_p2 == 7 && anterior_p3 == 4){
        				this.removeChildByTag("A07P04");
        			}
        			else if(anterior_p2 == 7 && anterior_p3 == 5){
        				this.removeChildByTag("A07P05");
        			}
        			else if(anterior_p2 == 7 && anterior_p3 == 6){
        				this.removeChildByTag("A07P06");
        			}
        			else if(anterior_p2 == 7 && anterior_p3 == 7){
        				this.removeChildByTag("A04P07");
        			}
        			else if(anterior_p2 == 7 && anterior_p3 == 8){
        				this.removeChildByTag("A07P08");
        			}
        			else if(anterior_p2 == 7 && anterior_p3 == 9){
        				this.removeChildByTag("A04P09");
        			}
        			else if(anterior_p2 == 7 && anterior_p3 == 10){
        				this.removeChildByTag("A07P10");
        			}
        			else if(anterior_p2 == 7 && anterior_p3 == 11){
        				this.removeChildByTag("A04P11");
        			}
//	Remover connt_p2 == 8 CRUZA:
        			
        			if(anterior_p2 == 8 && anterior_p3 == 1){
        				this.removeChildByTag("A08P01");
        			}
        			else if(anterior_p2 == 8 && anterior_p3 == 2){
        				this.removeChildByTag("A08P02");
        			}
        			else if(anterior_p2 == 8 && anterior_p3 == 3){
        				this.removeChildByTag("A08P03");
        			}
        			else if(anterior_p2 == 8 && anterior_p3 == 4){
        				this.removeChildByTag("A08P04");
        			}
        			else if(anterior_p2 == 8 && anterior_p3 == 5){
        				this.removeChildByTag("A08P05");
        			}
        			else if(anterior_p2 == 8 && anterior_p3 == 6){
        				this.removeChildByTag("A08P06");
        			}
        			else if(anterior_p2 == 8 && anterior_p3 == 7){
        				this.removeChildByTag("A08P07");
        			}
        			else if(anterior_p2 == 8 && anterior_p3 == 8){
        				this.removeChildByTag("A08P08");
        			}
        			else if(anterior_p2 == 8 && anterior_p3 == 9){
        				this.removeChildByTag("A08P09");
        			}
        			else if(anterior_p2 == 8 && anterior_p3 == 10){
        				this.removeChildByTag("A08P10");
        			}
        			else if(anterior_p2 == 8 && anterior_p3 == 11){
        				this.removeChildByTag("A08P11");
        			}
//	Remover connt_p2 == 9 COME:
        			
        			if(anterior_p2 == 9 && anterior_p3 == 1){
        				this.removeChildByTag("A03P01");
        			}
        			else if(anterior_p2 == 9 && anterior_p3 == 2){
        				this.removeChildByTag("A03P02");
        			}
        			else if(anterior_p2 == 9 && anterior_p3 == 3){
        				this.removeChildByTag("A03P03");
        			}
        			else if(anterior_p2 == 9 && anterior_p3 == 4){
        				this.removeChildByTag("A03P04");
        			}
        			else if(anterior_p2 == 9 && anterior_p3 == 5){
        				this.removeChildByTag("A03P05");
        			}
        			else if(anterior_p2 == 9 && anterior_p3 == 6){
        				this.removeChildByTag("A03P06");
        			}
        			else if(anterior_p2 == 9 && anterior_p3 == 7){
        				this.removeChildByTag("A03P07");
        			}
        			else if(anterior_p2 == 9 && anterior_p3 == 8){
        				this.removeChildByTag("A03P08");
        			}
        			else if(anterior_p2 == 9 && anterior_p3 == 9){
        				this.removeChildByTag("A03P09");
        			}
        			else if(anterior_p2 == 9 && anterior_p3 == 10){
        				this.removeChildByTag("A03P10");
        			}
        			else if(anterior_p2 == 9 && anterior_p3 == 11){
        				this.removeChildByTag("A03P11");
        			}
//	Remover connt_p2 == 10 BEBE:
        			
        			if(anterior_p2 == 10 && anterior_p3 == 1){
        				this.removeChildByTag("A10P01");
        			}
        			else if(anterior_p2 == 10 && anterior_p3 == 2){
        				this.removeChildByTag("A10P02");
        			}
        			else if(anterior_p2 == 10 && anterior_p3 == 3){
        				this.removeChildByTag("A10P03");
        			}
        			else if(anterior_p2 == 10 && anterior_p3 == 4){
        				this.removeChildByTag("A10P04");
        			}
        			else if(anterior_p2 == 10 && anterior_p3 == 5){
        				this.removeChildByTag("A10P05");
        			}
        			else if(anterior_p2 == 10 && anterior_p3 == 6){
        				this.removeChildByTag("A10P06");
        			}
        			else if(anterior_p2 == 10 && anterior_p3 == 7){
        				this.removeChildByTag("A10P07");
        			}
        			else if(anterior_p2 == 10 && anterior_p3 == 8){
        				this.removeChildByTag("A10P08");
        			}
        			else if(anterior_p2 == 10 && anterior_p3 == 9){
        				this.removeChildByTag("A10P09");
        			}
        			else if(anterior_p2 == 10 && anterior_p3 == 10){
        				this.removeChildByTag("A10P10");
        			}
        			else if(anterior_p2 == 10 && anterior_p3 == 11){
        				this.removeChildByTag("A10P11");
        			}
//	Remover connt_p2 == 11 CANTA:
        			
        			if(anterior_p2 == 11 && anterior_p3 == 1){
        				this.removeChildByTag("A11P01");
        			}
        			else if(anterior_p2 == 11 && anterior_p3 == 2){
        				this.removeChildByTag("A11P02");
        			}
        			else if(anterior_p2 == 11 && anterior_p3 == 3){
        				this.removeChildByTag("A11P03");
        			}
        			else if(anterior_p2 == 11 && anterior_p3 == 4){
        				this.removeChildByTag("A11P04");
        			}
        			else if(anterior_p2 == 11 && anterior_p3 == 5){
        				this.removeChildByTag("A11P05");
        			}
        			else if(anterior_p2 == 11 && anterior_p3 == 6){
        				this.removeChildByTag("A11P06");
        			}
        			else if(anterior_p2 == 11 && anterior_p3 == 7){
        				this.removeChildByTag("A11P07");
        			}
        			else if(anterior_p2 == 11 && anterior_p3 == 8){
        				this.removeChildByTag("A11P08");
        			}
        			else if(anterior_p2 == 11 && anterior_p3 == 9){
        				this.removeChildByTag("A11P09");
        			}
        			else if(anterior_p2 == 11 && anterior_p3 == 10){
        				this.removeChildByTag("A11P10");
        			}
        			else if(anterior_p2 == 11 && anterior_p3 == 11){
        				this.removeChildByTag("A11P11");
        			}

//	cont_p2 == 1 DUERME:
        			if(cont_p2 == 1 && cont_p3 == 1){
 						// Sprite de A01P01:
    					this.addChild(this.A01P01, 0);
    					cc.spriteFrameCache.addSpriteFrames(res.A01P01_A);
    					var animFrames = [];
						var str = "";
						for (var i = 1; i < 3; i++) {
    						str = "A01P01_P" + (i < 2 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.6, true);
						var animate   = cc.Animate.create(animation);
						var animate_repeat = cc.RepeatForever.create(animate); 
    					this.A01P01.runAction(animate_repeat);
        			}
        			else if(cont_p2 == 1 && cont_p3 == 2){
 						// Sprite de A01P02:
    					this.addChild(this.A01P02, 0);
    					cc.spriteFrameCache.addSpriteFrames(res.A01P02_A);
    					var animFrames = [];
						var str = "";
						for (var i = 1; i < 3; i++) {
    						str = "A01P02_P" + (i < 2 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.6, 100);
						var animate   = cc.Animate.create(animation); 
						var animate_repeat = cc.RepeatForever.create(animate);
    					this.A01P02.runAction(animate_repeat);
        			}
        			else if(cont_p2 == 1 && cont_p3 == 3){
 						// Sprite de A01P03:
    					this.addChild(this.A01P03, 0);
    					cc.spriteFrameCache.addSpriteFrames(res.A01P03_A);
    					var animFrames = [];
						var str = "";
						for (var i = 1; i < 3; i++) {
    						str = "A01P03_P" + (i < 2 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.6, 100);
						var animate   = cc.Animate.create(animation);
						var animate_repeat = cc.RepeatForever.create(animate); 
    					this.A01P03.runAction(animate_repeat);
        			}
        			else if(cont_p2 == 1 && cont_p3 == 4){
 						// Sprite de A01P04:
    					this.addChild(this.A01P04, 0);
    					cc.spriteFrameCache.addSpriteFrames(res.A01P04_A);
    					var animFrames = [];
						var str = "";
						for (var i = 1; i < 3; i++) {
    						str = "A01P04_P" + (i < 2 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.6, 100);
						var animate   = cc.Animate.create(animation); 
						var animate_repeat = cc.RepeatForever.create(animate);
    					this.A01P04.runAction(animate_repeat);
        			}
        			else if(cont_p2 == 1 && cont_p3 == 5){
    					this.addChild(this.A01P05, 0);
    					cc.spriteFrameCache.addSpriteFrames(res.A01P05_A);
    					var animFrames = [];
						var str = "";
						for (var i = 1; i < 3; i++) {
    						str = "A01P05_P" + (i < 2 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.6, 100);
						var animate   = cc.Animate.create(animation);  
						var animate_repeat = cc.RepeatForever.create(animate);   
    					this.A01P05.runAction(animate_repeat);
        			}
        			else if(cont_p2 == 1 && cont_p3 == 6){
 						// Sprite de A01P06:
    					this.addChild(this.A01P06, 0);
    					cc.spriteFrameCache.addSpriteFrames(res.A01P06_A);
    					var animFrames = [];
						var str = "";
						for (var i = 1; i < 3; i++) {
    						str = "A01P06_P" + (i < 2 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.6, 100);
						var animate   = cc.Animate.create(animation);
						var animate_repeat = cc.RepeatForever.create(animate);
    					this.A01P06.runAction(animate_repeat);
        			}
        			else if(cont_p2 == 1 && cont_p3 == 7){
 						// Sprite de A01P07:
    					this.addChild(this.A01P07, 0);
    					cc.spriteFrameCache.addSpriteFrames(res.A01P07_A);
    					var animFrames = [];
						var str = "";
						for (var i = 1; i < 3; i++) {
    						str = "A01P07_P" + (i < 2 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.6, 100);
						var animate   = cc.Animate.create(animation);
						var animate_repeat = cc.RepeatForever.create(animate);
    					this.A01P07.runAction(animate_repeat);
        			}
        			else if(cont_p2 == 1 && cont_p3 == 8){
 						// Sprite de A01P08:
    					this.addChild(this.A01P08, 0);
    					cc.spriteFrameCache.addSpriteFrames(res.A01P08_A);
    					var animFrames = [];
						var str = "";
						for (var i = 1; i < 3; i++) {
    						str = "A01P08_P" + (i < 2 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.6, 100);
						var animate   = cc.Animate.create(animation);
						var animate_repeat = cc.RepeatForever.create(animate); 
    					this.A01P08.runAction(animate_repeat);
        			}
        			else if(cont_p2 == 1 && cont_p3 == 9){
 						// Sprite de A01P09:
        				this.addChild(this.A01P09, 0);
        				cc.spriteFrameCache.addSpriteFrames(res.A01P09_A);
    					var animFrames = [];
						var str = "";
						for (var i = 1; i < 3; i++) {
    						str = "A01P09_P" + (i < 2 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.6, 100);
						var animate   = cc.Animate.create(animation);
						var animate_repeat = cc.RepeatForever.create(animate);
    					this.A01P09.runAction(animate_repeat);
        			}
        			else if(cont_p2 == 1 && cont_p3 ==10){
 						// Sprite de A01P10:
        				this.addChild(this.A01P10, 0);
        				cc.spriteFrameCache.addSpriteFrames(res.A01P10_A);
    					var animFrames = [];
						var str = "";
						for (var i = 1; i < 4; i++) {
    						str = "A01P10_P" + (i < 3 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.6, 100);
						var animate   = cc.Animate.create(animation); 
						var animate_repeat = cc.RepeatForever.create(animate);
    					this.A01P10.runAction(animate_repeat);
        			}
        			else if(cont_p2 == 1 && cont_p3 == 11){
        				this.addChild(this.A01P11, 0);
        				cc.spriteFrameCache.addSpriteFrames(res.A01P11_A);
    					var animFrames = [];
						var str = "";
						for (var i = 1; i < 3; i++) {
    						str = "A01P11_P" + (i < 2 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.6, 100);
						var animate   = cc.Animate.create(animation); 
						var animate_repeat = cc.RepeatForever.create(animate);
    					this.A01P11.runAction(animate_repeat);
        			}
//	cont_p2 == 2 CORRE:
        			if(cont_p2 == 2 && cont_p3 == 1){
        				this.addChild(this.A02P01, 0);
						cc.spriteFrameCache.addSpriteFrames(res.A02P01_A);
    					var animFrames = [];
						var str = "";
						for (var i = 1; i < 4; i++) {
    						str = "A02P01_P" + (i < 3 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.1, 500);
						var animate   = cc.Animate.create(animation); 
						var animate_repeat = cc.RepeatForever.create(animate);
    					this.A02P01.runAction(animate_repeat);
    					this.A02P01.runAction(move_A02_repeat);	
        			}
        			else if(cont_p2 == 2 && cont_p3 == 2){
						this.addChild(this.A02P02, 0);
						cc.spriteFrameCache.addSpriteFrames(res.A02P02_A);
    					var animFrames = [];
						var str = "";
						for (var i = 1; i < 4; i++) {
    						str = "A02P02_P" + (i < 3 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.1, 500);
						var animate   = cc.Animate.create(animation); 
						var animate_repeat = cc.RepeatForever.create(animate);
    					this.A02P02.runAction(animate_repeat);
    					this.A02P02.runAction(move_A02_repeat);
        			}
        			else if(cont_p2 == 2 && cont_p3 == 3){
						this.addChild(this.A02P03, 0);
						cc.spriteFrameCache.addSpriteFrames(res.A05P03_A);
    					var animFrames = [];
						var str = "";
						for (var i = 1; i < 4; i++) {
    						str = "A05P03_P" + (i < 3 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.1, 500);
						var animate   = cc.Animate.create(animation); 
						var animate_repeat = cc.RepeatForever.create(animate);
    					this.A02P03.runAction(animate_repeat);
    					this.A02P03.runAction(move_A02_repeat);
        			}
        			else if(cont_p2 == 2 && cont_p3 == 4){
						this.addChild(this.A02P04, 0);
    					cc.spriteFrameCache.addSpriteFrames(res.A02P04_A);

    					var animFrames = [];
						var str = "";
						for (var i = 1; i < 4; i++) {
    						str = "A02P04_P" + (i < 3 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.1, 500);
						var animate   = cc.Animate.create(animation); 
						var animate_repeat = cc.RepeatForever.create(animate);
    					this.A02P04.runAction(animate_repeat);
    					this.A02P04.runAction(move_A02_repeat);
        			}
        			else if(cont_p2 == 2 && cont_p3 == 5){
						this.addChild(this.A02P05, 0);
    					cc.spriteFrameCache.addSpriteFrames(res.A02P05_A);

    					var animFrames = [];
						var str = "";
						for (var i = 1; i < 4; i++) {
    						str = "A02P05_P" + (i < 3 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.1, 500);
						var animate   = cc.Animate.create(animation); 
						var animate_repeat = cc.RepeatForever.create(animate);
    					this.A02P05.runAction(animate_repeat);
    					this.A02P05.runAction(move_A02_repeat);
        			}
        			else if(cont_p2 == 2 && cont_p3 == 6){
						this.addChild(this.A02P06, 0);
    					cc.spriteFrameCache.addSpriteFrames(res.A02P06_A);

    					var animFrames = [];
						var str = "";
						for (var i = 1; i < 4; i++) {
    						str = "A02P06_P" + (i < 3 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.1, 500);
						var animate   = cc.Animate.create(animation); 
						var animate_repeat = cc.RepeatForever.create(animate);
    					this.A02P06.runAction(animate_repeat);
    					this.A02P06.runAction(move_A02_repeat);
        			}
        			else if(cont_p2 == 2 && cont_p3 == 7){
						this.addChild(this.A02P07, 0);
    					cc.spriteFrameCache.addSpriteFrames(res.A02P07_A);

    					var animFrames = [];
						var str = "";
						for (var i = 1; i < 5; i++) {
    						str = "A02P07_P" + (i < 4 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.1, 500);
						var animate   = cc.Animate.create(animation); 
						var animate_repeat = cc.RepeatForever.create(animate);
    					this.A02P07.runAction(animate_repeat);
    					this.A02P07.runAction(move_A02_repeat);
        			}
        			else if(cont_p2 == 2 && cont_p3 == 8){
						this.addChild(this.A02P08, 0);
    					cc.spriteFrameCache.addSpriteFrames(res.A02P08_A);

    					var animFrames = [];
						var str = "";
						for (var i = 1; i < 4; i++) {
    						str = "A02P08_P" + (i < 3 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.1, 500);
						var animate   = cc.Animate.create(animation); 
						var animate_repeat = cc.RepeatForever.create(animate);
    					this.A02P08.runAction(animate_repeat);
    					this.A02P08.runAction(move_A02_repeat);
        			}
        			else if(cont_p2 == 2 && cont_p3 == 9){
						this.addChild(this.A02P09, 0);
    					cc.spriteFrameCache.addSpriteFrames(res.A02P09_A);

    					var animFrames = [];
						var str = "";
						for (var i = 1; i < 5; i++) {
    						str = "A02P09_P" + (i < 4 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.1, 500);
						var animate   = cc.Animate.create(animation); 
						var animate_repeat = cc.RepeatForever.create(animate);
    					this.A02P09.runAction(animate_repeat);
    					this.A02P09.runAction(move_A02_repeat);
        			}
        			else if(cont_p2 == 2 && cont_p3 == 10){
						this.addChild(this.A02P10, 0);
    					cc.spriteFrameCache.addSpriteFrames(res.A02P10_A);

    					var animFrames = [];
						var str = "";
						for (var i = 1; i < 3; i++) {
    						str = "A02P10_P" + (i < 2 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.1, 500);
						var animate   = cc.Animate.create(animation); 
						var animate_repeat = cc.RepeatForever.create(animate);
    					this.A02P10.runAction(animate_repeat);
    					this.A02P10.runAction(move_A02_repeat);
        			}
        			else if(cont_p2 == 2 && cont_p3 == 11){
						this.addChild(this.A02P11, 0);
    					cc.spriteFrameCache.addSpriteFrames(res.A02P11_A);

    					var animFrames = [];
						var str = "";
						for (var i = 1; i < 5; i++) {
    						str = "A02P11_P" + (i < 4 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.1, 500);
						var animate   = cc.Animate.create(animation); 
						var animate_repeat = cc.RepeatForever.create(animate);
    					this.A02P11.runAction(animate_repeat);
    					this.A02P11.runAction(move_A02_repeat);
        			}
//	cont_p2 == 3 MUERDE:
        			if(cont_p2 == 3 && cont_p3 == 1){
 						// Sprite de A03P01:
    					this.addChild(this.A03P01, 0);
    					cc.spriteFrameCache.addSpriteFrames(res.A03P01_A);
    					var animFrames = [];
						var str = "";
						for (var i = 3; i < 6; i++) {
    						str = "A03P01_P" + (i < 5 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.5, 200);
						var animate   = cc.Animate.create(animation); 

						var animate_repeat = cc.RepeatForever.create(animate);
    					this.A03P01.runAction(animate_repeat);
        			}
        			else if(cont_p2 == 3 && cont_p3 == 2){
 						// Sprite de A03P02:
    					this.addChild(this.A03P02, 0);
    					cc.spriteFrameCache.addSpriteFrames(res.A03P02_A);
    					var animFrames = [];
						var str = "";
						for (var i = 3; i < 6; i++) {
    						str = "A03P02_P" + (i < 5 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.5, 200);
						var animate   = cc.Animate.create(animation); 
						var animate_repeat = cc.RepeatForever.create(animate);
    					this.A03P02.runAction(animate_repeat);
        			}
        			else if(cont_p2 == 3 && cont_p3 == 3){
    					this.addChild(this.A09P03, 0);
    					cc.spriteFrameCache.addSpriteFrames(res.A09P03_A);
    					var animFrames = [];
						var str = "";
						for (var i = 1; i < 4; i++) {
    						str = "A09P03_P" + (i < 3 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.5, 200);
						var animate   = cc.Animate.create(animation); 
						var animate_repeat = cc.RepeatForever.create(animate);
    					this.A09P03.runAction(animate_repeat);
        			}
        			else if(cont_p2 == 3 && cont_p3 == 4){
 						// Sprite de A03P04:
    					this.addChild(this.A03P04, 0);
    					cc.spriteFrameCache.addSpriteFrames(res.A03P04_A);
    					var animFrames = [];
						var str = "";
						for (var i = 3; i < 6; i++) {
    						str = "A03P04_P" + (i < 5 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.5, 200);
						var animate   = cc.Animate.create(animation); 
						var animate_repeat = cc.RepeatForever.create(animate);
    					this.A03P04.runAction(animate_repeat);
        			}
        			else if(cont_p2 == 3 && cont_p3 == 5){
 						// Sprite de A03P03:
    					this.addChild(this.A03P05, 0);
    					cc.spriteFrameCache.addSpriteFrames(res.A03P05_A);
    					var animFrames = [];
						var str = "";
						for (var i = 3; i < 6; i++) {
    						str = "A03P05_P" + (i < 5 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.5, 200);
						var animate   = cc.Animate.create(animation);  
						var animate_repeat = cc.RepeatForever.create(animate);   
    					this.A03P05.runAction(animate_repeat);
        			}
        			else if(cont_p2 == 3 && cont_p3 == 6){
 						// Sprite de A03P06:
    					this.addChild(this.A03P06, 0);
    					cc.spriteFrameCache.addSpriteFrames(res.A03P06_A);
    					var animFrames = [];
						var str = "";
						for (var i = 3; i < 6; i++) {
    						str = "A03P06_P" + (i < 5 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.5, 200);
						var animate   = cc.Animate.create(animation);
						var animate_repeat = cc.RepeatForever.create(animate);
    					this.A03P06.runAction(animate_repeat);
        			}
        			else if(cont_p2 == 3 && cont_p3 == 7){
    					this.addChild(this.A09P07, 0);
    					cc.spriteFrameCache.addSpriteFrames(res.A09P07_A);
    					var animFrames = [];
						var str = "";
						for (var i = 1; i < 3; i++) {
    						str = "A09P07_P" + (i < 2 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.5, 200);
						var animate   = cc.Animate.create(animation);
						var animate_repeat = cc.RepeatForever.create(animate);
    					this.A09P07.runAction(animate_repeat);
        			}
        			else if(cont_p2 == 3 && cont_p3 == 8){
 						// Sprite de A03P08:
    					this.addChild(this.A03P08, 0);
    					cc.spriteFrameCache.addSpriteFrames(res.A03P08_A);
    					var animFrames = [];
						var str = "";
						for (var i = 3; i < 6; i++) {
    						str = "A03P08_P" + (i < 5 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.5, 200);
						var animate   = cc.Animate.create(animation); 
						var animate_repeat = cc.RepeatForever.create(animate);
    					this.A03P08.runAction(animate_repeat);
        			}
        			else if(cont_p2 == 3 && cont_p3 == 9){
        				this.addChild(this.A09P09, 0);
        				cc.spriteFrameCache.addSpriteFrames(res.A09P09_A);
    					var animFrames = [];
						var str = "";
						for (var i = 1; i < 3; i++) {
    						str = "A09P09_P" + (i < 2 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.5, 200);
						var animate   = cc.Animate.create(animation);
						var animate_repeat = cc.RepeatForever.create(animate);
    					this.A09P09.runAction(animate_repeat);
        			}
        			else if(cont_p2 == 3 && cont_p3 ==10){
 						// Sprite de A03P10:
        				this.addChild(this.A09P10, 0);
        				cc.spriteFrameCache.addSpriteFrames(res.A09P10_A);
    					var animFrames = [];
						var str = "";
						for (var i = 1; i < 3; i++) {
    						str = "A09P10_P" + (i < 2 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.5, 200);
						var animate   = cc.Animate.create(animation); 
						var animate_repeat = cc.RepeatForever.create(animate);
    					this.A09P10.runAction(animate_repeat);
        			}
        			else if(cont_p2 == 3 && cont_p3 == 11){
        				this.addChild(this.A09P11, 0);
        				cc.spriteFrameCache.addSpriteFrames(res.A09P11_A);
    					var animFrames = [];
						var str = "";
						for (var i = 1; i < 3; i++) {
    						str = "A09P11_P" + (i < 2 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.5, 200);
						var animate   = cc.Animate.create(animation); 
						var animate_repeat = cc.RepeatForever.create(animate);
    					this.A09P11.runAction(animate_repeat);
        			}
//	cont_p2 == 4 BRINCA:
        			if(cont_p2 == 4 && cont_p3 == 1){
        				this.addChild(this.A04P01, 0);
				
						cc.spriteFrameCache.addSpriteFrames(res.A04P01_A);

    					var animFrames = [];
						var str = "";
						for (var i = 1; i < 3; i++) {
    						str = "A04P01_P" + (i < 2 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.5, 200);
						var animate   = cc.Animate.create(animation); 
						var animate_repeat = cc.RepeatForever.create(animate); 
    					this.A04P01.runAction(animate_repeat);
    					this.A04P01.runAction(move_A04_repeat);	
        			}
        			else if(cont_p2 == 4 && cont_p3 == 2){
						this.addChild(this.A04P02, 0);
				
						cc.spriteFrameCache.addSpriteFrames(res.A04P02_A);

    					var animFrames = [];
						var str = "";
						for (var i = 1; i < 3; i++) {
    						str = "A04P02_P" + (i < 2 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.5, 200);
						var animate   = cc.Animate.create(animation); 
						var animate_repeat = cc.RepeatForever.create(animate); 
    					this.A04P02.runAction(animate_repeat);
    					this.A04P02.runAction(move_A04_repeat);	
        			}
        			else if(cont_p2 == 4 && cont_p3 == 3){
						this.addChild(this.A04P03, 0);
						cc.spriteFrameCache.addSpriteFrames(res.A04P03_A);

    					var animFrames = [];
						var str = "";
						for (var i = 1; i < 5; i++) {
    						str = "A08P03_P" + (i < 4 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.3, 4);
						var animate   = cc.Animate.create(animation); 
						var animate_repeat = cc.RepeatForever.create(animate); 
    					this.A04P03.runAction(animate_repeat);
    					this.A04P03.runAction(move_A04P03_repeat);
        			}
        			else if(cont_p2 == 4 && cont_p3 == 4){
						this.addChild(this.A04P04, 0);
						cc.spriteFrameCache.addSpriteFrames(res.A04P04_A);

    					var animFrames = [];
						var str = "";
						for (var i = 1; i < 3; i++) {
    						str = "A04P04_P" + (i < 2 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.5, 200);
						var animate   = cc.Animate.create(animation); 
						var animate_repeat = cc.RepeatForever.create(animate); 
    					this.A04P04.runAction(animate_repeat);
    					this.A04P04.runAction(move_A04_repeat);
        			}
        			else if(cont_p2 == 4 && cont_p3 == 5){
						this.addChild(this.A04P05, 0);
    					cc.spriteFrameCache.addSpriteFrames(res.A04P05_A);

    					var animFrames = [];
						var str = "";
						for (var i = 1; i < 3; i++) {
    						str = "A04P05_P" + (i < 2 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.5, 200);
						var animate   = cc.Animate.create(animation); 
						var animate_repeat = cc.RepeatForever.create(animate); 
    					this.A04P05.runAction(animate_repeat);
    					this.A04P05.runAction(move_A04_repeat);
        			}
        			else if(cont_p2 == 4 && cont_p3 == 6){
						this.addChild(this.A04P06, 0);
    					cc.spriteFrameCache.addSpriteFrames(res.A04P06_A);

    					var animFrames = [];
						var str = "";
						for (var i = 1; i < 3; i++) {
    						str = "A04P06_P" + (i < 2 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.5, 200);
						var animate   = cc.Animate.create(animation); 
						var animate_repeat = cc.RepeatForever.create(animate); 
    					this.A04P06.runAction(animate_repeat);
    					this.A04P06.runAction(move_A04_repeat);
        			}
        			else if(cont_p2 == 4 && cont_p3 == 7){
						this.addChild(this.A04P07, 0);
    					cc.spriteFrameCache.addSpriteFrames(res.A04P07_A);

    					var animFrames = [];
						var str = "";
						for (var i = 1; i < 3; i++) {
    						str = "A04P07_P" + (i < 2 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.5, 200);
						var animate   = cc.Animate.create(animation); 
						var animate_repeat = cc.RepeatForever.create(animate); 
    					this.A04P07.runAction(animate_repeat);
    					this.A04P07.runAction(move_A04P7_repeat);
        			}
       				else if(cont_p2 == 4 && cont_p3 == 8){
						this.addChild(this.A04P08, 0);
    					cc.spriteFrameCache.addSpriteFrames(res.A04P08_A);

    					var animFrames = [];
						var str = "";
						for (var i = 1; i < 3; i++) {
    						str = "A04P08_P" + (i < 2 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.5, 200);
						var animate   = cc.Animate.create(animation); 
						var animate_repeat = cc.RepeatForever.create(animate); 
    					this.A04P08.runAction(animate_repeat);
    					this.A04P08.runAction(move_A04_repeat);
        			}
         			else if(cont_p2 == 4 && cont_p3 == 9){
						this.addChild(this.A04P09, 0);
    					cc.spriteFrameCache.addSpriteFrames(res.A04P09_A);

    					var animFrames = [];
						var str = "";
						for (var i = 1; i < 3; i++) {
    						str = "A04P09_P" + (i < 2 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.5, 200);
						var animate   = cc.Animate.create(animation); 
						var animate_repeat = cc.RepeatForever.create(animate); 
    					this.A04P09.runAction(animate_repeat);
    					this.A04P09.runAction(move_A04P7_repeat);
        			}
        			else if(cont_p2 == 4 && cont_p3 == 10){
						this.addChild(this.A04P10, 0);
    					cc.spriteFrameCache.addSpriteFrames(res.A04P10_A);

    					var animFrames = [];
						var str = "";
						for (var i = 1; i < 4; i++) {
    						str = "A04P10_P" + (i < 3 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.3, 200);
						var animate   = cc.Animate.create(animation); 
						var animate_repeat = cc.RepeatForever.create(animate); 
    					this.A04P10.runAction(animate_repeat);
    					this.A04P10.runAction(move_A04P10_repeat);	
    					
        			}
        			else if(cont_p2 == 4 && cont_p3 == 11){
						this.addChild(this.A04P11, 0);
    					cc.spriteFrameCache.addSpriteFrames(res.A04P11_A);

    					var animFrames = [];
						var str = "";
						for (var i = 1; i < 3; i++) {
    						str = "A04P11_P" + (i < 2 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.5, 200);
						var animate   = cc.Animate.create(animation); 
						var animate_repeat = cc.RepeatForever.create(animate); 
    					this.A04P11.runAction(animate_repeat);
    					this.A04P11.runAction(move_A04P7_repeat);
        			}
//	cont_p2 == 5 VUELA:
        			if(cont_p2 == 5 && cont_p3 == 1){
    					this.addChild(this.A05P01, 0);
    					cc.spriteFrameCache.addSpriteFrames(res.A05P01_A);
    					var animFrames = [];
						var str = "";
						for (var i = 1; i < 4; i++) {
    						str = "A05P01_P" + (i < 3 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.09, 200);
						var animate   = cc.Animate.create(animation); 
						var animate_repeat = cc.RepeatForever.create(animate); 
    					this.A05P01.runAction(animate_repeat);
    					this.A05P01.runAction(move_A05P1_repeat);
        			}
        			else if(cont_p2 == 5 && cont_p3 == 2){
    					this.addChild(this.A05P02, 0);
    					cc.spriteFrameCache.addSpriteFrames(res.A05P02_A);
    					var animFrames = [];
						var str = "";
						for (var i = 1; i < 4; i++) {
    						str = "A05P02_P" + (i < 3 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.09, 200);
						var animate   = cc.Animate.create(animation);
						var animate_repeat = cc.RepeatForever.create(animate);  
    					this.A05P02.runAction(animate_repeat);
    					this.A05P02.runAction(move_A05P1_repeat);
        			}
        			else if(cont_p2 == 5 && cont_p3 == 3){    					
        				this.addChild(this.A05P03, 0);
    					cc.spriteFrameCache.addSpriteFrames(res.A05P03_A);
    					var animFrames = [];
						var str = "";
						for (var i = 1; i < 4; i++) {
    						str = "A05P03_P" + (i < 3 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.09, 200);
						var animate   = cc.Animate.create(animation); 
						var animate_repeat = cc.RepeatForever.create(animate); 
    					this.A05P03.runAction(animate_repeat);
    					this.A05P03.runAction(move_A05P1_repeat);
        			}
        			else if(cont_p2 == 5 && cont_p3 == 4){
    					this.addChild(this.A05P04, 0);
    					cc.spriteFrameCache.addSpriteFrames(res.A05P04_A);
    					var animFrames = [];
						var str = "";
						for (var i = 1; i < 4; i++) {
    						str = "A05P04_P" + (i < 3 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.09, 200);
						var animate   = cc.Animate.create(animation); 
						var animate_repeat = cc.RepeatForever.create(animate); 
    					this.A05P04.runAction(animate_repeat);
    					this.A05P04.runAction(move_A05P1_repeat);
        			}
        			else if(cont_p2 == 5 && cont_p3 == 5){
    					this.addChild(this.A05P05, 0);
    					cc.spriteFrameCache.addSpriteFrames(res.A05P05_A);
    					var animFrames = [];
						var str = "";
						for (var i = 1; i < 4; i++) {
    						str = "A05P05_P" + (i < 3 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.09, 200);
						var animate   = cc.Animate.create(animation); 
						var animate_repeat = cc.RepeatForever.create(animate);     
    					this.A05P05.runAction(animate_repeat);
    					this.A05P05.runAction(move_A05P1_repeat);
        			}
        			else if(cont_p2 == 5 && cont_p3 == 6){
    					this.addChild(this.A05P06, 0);
    					cc.spriteFrameCache.addSpriteFrames(res.A05P06_A);
    					var animFrames = [];
						var str = "";
						for (var i = 1; i < 4; i++) {
    						str = "A05P06_P" + (i < 3 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.09, 200);
						var animate   = cc.Animate.create(animation);
						var animate_repeat = cc.RepeatForever.create(animate); 
    					this.A05P06.runAction(animate_repeat);
    					this.A05P06.runAction(move_A05P1_repeat);
        			}
        			else if(cont_p2 == 5 && cont_p3 == 7){
    					this.addChild(this.A05P07, 0);
    					cc.spriteFrameCache.addSpriteFrames(res.A05P07_A);
    					var animFrames = [];
						var str = "";
						for (var i = 1; i < 5; i++) {
    						str = "A05P07_P" + (i < 4 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.1, 200);
						var animate   = cc.Animate.create(animation);
						var animate_repeat = cc.RepeatForever.create(animate); 
    					this.A05P07.runAction(animate_repeat);
    					this.A05P07.runAction(move_A05_repeat);
        			}
        			else if(cont_p2 == 5 && cont_p3 == 8){
    					this.addChild(this.A05P08, 0);
    					cc.spriteFrameCache.addSpriteFrames(res.A05P08_A);
    					var animFrames = [];
						var str = "";
						for (var i = 1; i < 4; i++) {
    						str = "A05P08_P" + (i < 3 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.09, 200);
						var animate   = cc.Animate.create(animation); 
						var animate_repeat = cc.RepeatForever.create(animate); 
    					this.A05P08.runAction(animate_repeat);
    					this.A05P08.runAction(move_A05P1_repeat);
        			}
        			else if(cont_p2 == 5 && cont_p3 == 9){
        				this.addChild(this.A05P09, 0);
        				cc.spriteFrameCache.addSpriteFrames(res.A05P09_A);
    					var animFrames = [];
						var str = "";
						for (var i = 1; i < 5; i++) {
    						str = "A05P09_P" + (i < 4 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.1, 200);
						var animate   = cc.Animate.create(animation);
						var animate_repeat = cc.RepeatForever.create(animate); 
    					this.A05P09.runAction(animate_repeat);
    					this.A05P09.runAction(move_A05_repeat);
        			}
        			else if(cont_p2 == 5 && cont_p3 ==10){
        				this.addChild(this.A05P10, 0);
        				cc.spriteFrameCache.addSpriteFrames(res.A05P10_A);
    					var animFrames = [];
						var str = "";
						for (var i = 1; i < 7; i++) {
    						str = "A05P10_P" + (i < 6 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.2, 200);
						var animate   = cc.Animate.create(animation); 
						var animate_repeat = cc.RepeatForever.create(animate); 
    					this.A05P10.runAction(animate_repeat);
    					this.A05P10.runAction(move_A05_repeat);
        			}
        			else if(cont_p2 == 5 && cont_p3 == 11){
        				this.addChild(this.A05P11, 0);
        				cc.spriteFrameCache.addSpriteFrames(res.A05P11_A);
    					var animFrames = [];
						var str = "";
						for (var i = 1; i < 5; i++) {
    						str = "A05P11_P" + (i < 4 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.1, 200);
						var animate   = cc.Animate.create(animation); 
						var animate_repeat = cc.RepeatForever.create(animate); 
    					this.A05P11.runAction(animate_repeat);
    					this.A05P11.runAction(move_A05_repeat);	
        			}
//	cont_p2 == 6 HABLA:
        			if(cont_p2 == 6 && cont_p3 == 1){
    					this.addChild(this.A06P01, 0);
    					cc.spriteFrameCache.addSpriteFrames(res.A06P01_A);
    					var animFrames = [];
						var str = "";
						for (var i = 1; i < 3; i++) {
    						str = "A06P01_P" + (i < 2 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.5, 200);
						var animate   = cc.Animate.create(animation); 
						var animate_repeat = cc.RepeatForever.create(animate); 
    					this.A06P01.runAction(animate_repeat);
        			}
        			else if(cont_p2 == 6 && cont_p3 == 2){
 						// Sprite de A06P02:
    					this.addChild(this.A06P02, 0);
    					cc.spriteFrameCache.addSpriteFrames(res.A06P02_A);
    					var animFrames = [];
						var str = "";
						for (var i = 1; i < 3; i++) {
    						str = "A06P02_P" + (i < 2 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.5, 200);
						var animate   = cc.Animate.create(animation); 
						var animate_repeat = cc.RepeatForever.create(animate); 
    					this.A06P02.runAction(animate_repeat);
        			}
        			else if(cont_p2 == 6 && cont_p3 == 3){
    					this.addChild(this.A06P03, 0);
    					cc.spriteFrameCache.addSpriteFrames(res.A06P03_A);
    					var animFrames = [];
						var str = "";
						for (var i = 1; i < 3; i++) {
    						str = "A06P03_P" + (i < 2 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.5, 200);
						var animate   = cc.Animate.create(animation); 
						var animate_repeat = cc.RepeatForever.create(animate); 
    					this.A06P03.runAction(animate_repeat);
        			}
        			else if(cont_p2 == 6 && cont_p3 == 4){
    					this.addChild(this.A06P04, 0);
    					cc.spriteFrameCache.addSpriteFrames(res.A06P04_A);
    					var animFrames = [];
						var str = "";
						for (var i = 1; i < 3; i++) {
    						str = "A06P04_P" + (i < 2 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.5, 200);
						var animate   = cc.Animate.create(animation);
						var animate_repeat = cc.RepeatForever.create(animate); 
    					this.A06P04.runAction(animate_repeat);
        			}
        			else if(cont_p2 == 6 && cont_p3 == 5){
    					this.addChild(this.A06P05, 0);
    					cc.spriteFrameCache.addSpriteFrames(res.A06P05_A);
    					var animFrames = [];
						var str = "";
						for (var i = 1; i < 3; i++) {
    						str = "A06P05_P" + (i < 2 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.5, 200);
						var animate   = cc.Animate.create(animation);
						var animate_repeat = cc.RepeatForever.create(animate);     
    					this.A06P05.runAction(animate_repeat);
        			}
        			else if(cont_p2 == 6 && cont_p3 == 6){
 						// Sprite de A06P06:
    					this.addChild(this.A06P06, 0);
    					cc.spriteFrameCache.addSpriteFrames(res.A06P06_A);
    					var animFrames = [];
						var str = "";
						for (var i = 1; i < 3; i++) {
    						str = "A06P06_P" + (i < 2 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.5, 200);
						var animate   = cc.Animate.create(animation);
						var animate_repeat = cc.RepeatForever.create(animate); 
    					this.A06P06.runAction(animate_repeat);
        			}
        			else if(cont_p2 == 6 && cont_p3 == 7){
    					this.addChild(this.A06P07, 0);
    					cc.spriteFrameCache.addSpriteFrames(res.A06P07_A);
    					var animFrames = [];
						var str = "";
						for (var i = 1; i < 3; i++) {
    						str = "A06P07_P" + (i < 2 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.5, 200);
						var animate   = cc.Animate.create(animation);
						var animate_repeat = cc.RepeatForever.create(animate); 
    					this.A06P07.runAction(animate_repeat);
        			}
        			else if(cont_p2 == 6 && cont_p3 == 8){
    					this.addChild(this.A06P08, 0);
    					cc.spriteFrameCache.addSpriteFrames(res.A06P08_A);
    					var animFrames = [];
						var str = "";
						for (var i = 1; i < 3; i++) {
    						str = "A06P08_P" + (i < 2 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.5, 200);
						var animate   = cc.Animate.create(animation); 
						var animate_repeat = cc.RepeatForever.create(animate); 
    					this.A06P08.runAction(animate_repeat);
        			}
        			else if(cont_p2 == 6 && cont_p3 == 9){
        				this.addChild(this.A06P09, 0);
        				cc.spriteFrameCache.addSpriteFrames(res.A06P09_A);
    					var animFrames = [];
						var str = "";
						for (var i = 1; i < 3; i++) {
    						str = "A06P09_P" + (i < 2 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.5, 200);
						var animate   = cc.Animate.create(animation);
						var animate_repeat = cc.RepeatForever.create(animate); 
    					this.A06P09.runAction(animate_repeat);
        			}
        			else if(cont_p2 == 6 && cont_p3 ==10){
        				this.addChild(this.A06P10, 0);
        				cc.spriteFrameCache.addSpriteFrames(res.A06P10_A);
    					var animFrames = [];
						var str = "";
						for (var i = 1; i < 3; i++) {
    						str = "A06P10_P" + (i < 2 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.5, 200);
						var animate   = cc.Animate.create(animation); 
						var animate_repeat = cc.RepeatForever.create(animate); 
    					this.A06P10.runAction(animate_repeat);
        			}
        			else if(cont_p2 == 6 && cont_p3 == 11){
        				this.addChild(this.A06P11, 0);
        				cc.spriteFrameCache.addSpriteFrames(res.A06P11_A);
    					var animFrames = [];
						var str = "";
						for (var i = 1; i < 3; i++) {
    						str = "A06P11_P" + (i < 2 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.5, 200);
						var animate   = cc.Animate.create(animation); 
						var animate_repeat = cc.RepeatForever.create(animate); 
    					this.A06P11.runAction(animate_repeat);
        			}
//	cont_p2 == 7 CAMINA:
        			if(cont_p2 == 7 && cont_p3 == 1){
    					this.addChild(this.A07P01, 0);
    					cc.spriteFrameCache.addSpriteFrames(res.A07P01_A);
    					var animFrames = [];
						var str = "";
						for (var i = 1; i < 3; i++) {
    						str = "A07P01_P" + (i < 2 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.5, 200);
						var animate   = cc.Animate.create(animation);
						var animate_repeat = cc.RepeatForever.create(animate);  
    					this.A07P01.runAction(animate_repeat);
        			}
        			else if(cont_p2 == 7 && cont_p3 == 2){
    					this.addChild(this.A07P02, 0);
    					cc.spriteFrameCache.addSpriteFrames(res.A07P02_A);
    					var animFrames = [];
						var str = "";
						for (var i = 1; i < 3; i++) {
    						str = "A07P02_P" + (i < 2 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.5, 200);
						var animate   = cc.Animate.create(animation); 
						var animate_repeat = cc.RepeatForever.create(animate); 
    					this.A07P02.runAction(animate_repeat);
        			}
        			else if(cont_p2 == 7 && cont_p3 == 3){
    					this.addChild(this.A07P03, 0);
    					cc.spriteFrameCache.addSpriteFrames(res.A07P03_A);
    					var animFrames = [];
						var str = "";
						for (var i = 1; i < 3; i++) {
    						str = "A07P03_P" + (i < 2 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.5, 200);
						var animate   = cc.Animate.create(animation); 
						var animate_repeat = cc.RepeatForever.create(animate); 
    					this.A07P03.runAction(animate_repeat);
        			}
        			else if(cont_p2 == 7 && cont_p3 == 4){
    					this.addChild(this.A07P04, 0);
    					cc.spriteFrameCache.addSpriteFrames(res.A07P04_A);
    					var animFrames = [];
						var str = "";
						for (var i = 1; i < 3; i++) {
    						str = "A07P04_P" + (i < 2 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.5, 200);
						var animate   = cc.Animate.create(animation);
						var animate_repeat = cc.RepeatForever.create(animate);  
    					this.A07P04.runAction(animate_repeat);
        			}
        			else if(cont_p2 == 7 && cont_p3 == 5){
    					this.addChild(this.A07P05, 0);
    					cc.spriteFrameCache.addSpriteFrames(res.A07P05_A);
    					var animFrames = [];
						var str = "";
						for (var i = 1; i < 3; i++) {
    						str = "A07P05_P" + (i < 2 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.5, 200);
						var animate   = cc.Animate.create(animation);  
						var animate_repeat = cc.RepeatForever.create(animate);    
    					this.A07P05.runAction(animate_repeat);
        			}
        			else if(cont_p2 == 7 && cont_p3 == 6){
    					this.addChild(this.A07P06, 0);
    					cc.spriteFrameCache.addSpriteFrames(res.A07P06_A);
    					var animFrames = [];
						var str = "";
						for (var i = 1; i < 3; i++) {
    						str = "A07P06_P" + (i < 2 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.5, 200);
						var animate   = cc.Animate.create(animation);
						var animate_repeat = cc.RepeatForever.create(animate); 
    					this.A07P06.runAction(animate_repeat);
        			}
        			else if(cont_p2 == 7 && cont_p3 == 7){
    					this.addChild(this.A04P07, 0);
    					cc.spriteFrameCache.addSpriteFrames(res.A04P07_A);
    					var animFrames = [];
						var str = "";
						for (var i = 1; i < 3; i++) {
    						str = "A04P07_P" + (i < 2 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.5, 200);
						var animate   = cc.Animate.create(animation);
						var animate_repeat = cc.RepeatForever.create(animate); 
    					this.A04P07.runAction(animate_repeat);
        			}
        			else if(cont_p2 == 7 && cont_p3 == 8){
    					this.addChild(this.A07P08, 0);
    					cc.spriteFrameCache.addSpriteFrames(res.A07P08_A);
    					var animFrames = [];
						var str = "";
						for (var i = 1; i < 3; i++) {
    						str = "A07P08_P" + (i < 2 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.5, 200);
						var animate   = cc.Animate.create(animation); 
						var animate_repeat = cc.RepeatForever.create(animate); 
    					this.A07P08.runAction(animate_repeat);
        			}
        			else if(cont_p2 == 7 && cont_p3 == 9){
        				this.addChild(this.A04P09, 0);
        				cc.spriteFrameCache.addSpriteFrames(res.A04P09_A);
    					var animFrames = [];
						var str = "";
						for (var i = 1; i < 3; i++) {
    						str = "A04P09_P" + (i < 2 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.5, 200);
						var animate   = cc.Animate.create(animation);
						var animate_repeat = cc.RepeatForever.create(animate); 
    					this.A04P09.runAction(animate_repeat);
        			}
        			else if(cont_p2 == 7 && cont_p3 ==10){
        				this.addChild(this.A07P10, 0);
        				cc.spriteFrameCache.addSpriteFrames(res.A07P10_A);
    					var animFrames = [];
						var str = "";
						for (var i = 1; i < 4; i++) {
    						str = "A07P10_P" + (i < 3 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.5, 200);
						var animate   = cc.Animate.create(animation); 
						var animate_repeat = cc.RepeatForever.create(animate); 
    					this.A07P10.runAction(animate_repeat);
        			}
        			else if(cont_p2 == 7 && cont_p3 == 11){
        				this.addChild(this.A04P11, 0);
        				cc.spriteFrameCache.addSpriteFrames(res.A04P11_A);
    					var animFrames = [];
						var str = "";
						for (var i = 1; i < 3; i++) {
    						str = "A04P11_P" + (i < 2 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.5, 200);
						var animate   = cc.Animate.create(animation);
						var animate_repeat = cc.RepeatForever.create(animate);  
    					this.A04P11.runAction(animate_repeat);
        			}
//	cont_p2 == 8 CRUZA:
        			if(cont_p2 == 8 && cont_p3 == 1){
    					this.addChild(this.A08P01, 0);
    					cc.spriteFrameCache.addSpriteFrames(res.A08P01_A);
    					var animFrames = [];
						var str = "";
						for (var i = 1; i < 13; i++) {
    						str = "A08P01_P" + (i < 12 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.3, 200);
						var animate   = cc.Animate.create(animation); 
						var animate_repeat = cc.RepeatForever.create(animate);
    					this.A08P01.runAction(animate_repeat);
    					this.A08P01.runAction(move_A08P01_repeat);
    					
        			}
        			else if(cont_p2 == 8 && cont_p3 == 2){
    					this.addChild(this.A08P02, 0);
    					cc.spriteFrameCache.addSpriteFrames(res.A08P02_A);
    					var animFrames = [];
						var str = "";
						for (var i = 1; i < 13; i++) {
    						str = "A08P02_P" + (i < 12 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.3, 200);
						var animate   = cc.Animate.create(animation); 
						var animate_repeat = cc.RepeatForever.create(animate);
    					this.A08P02.runAction(animate_repeat);
    					this.A08P02.runAction(move_A08P01_repeat);
        			}
        			else if(cont_p2 == 8 && cont_p3 == 3){    					
        				this.addChild(this.A08P03, 0);
    					cc.spriteFrameCache.addSpriteFrames(res.A08P03_A);
    					var animFrames = [];
						var str = "";
						for (var i = 1; i < 21; i++) {
    						str = "A08P03_P" + (i < 20 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.3, 200);
						var animate   = cc.Animate.create(animation); 
						var animate_repeat = cc.RepeatForever.create(animate); 
    					this.A08P03.runAction(animate_repeat);
    					this.A08P03.runAction(move_A08P03_repeat);
        			}
        			else if(cont_p2 == 8 && cont_p3 == 4){
    					this.addChild(this.A08P04, 0);
    					cc.spriteFrameCache.addSpriteFrames(res.A08P04_A);
    					var animFrames = [];
						var str = "";
						for (var i = 1; i < 13; i++) {
    						str = "A08P04_P" + (i < 12 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.3, 200);
						var animate   = cc.Animate.create(animation); 
						var animate_repeat = cc.RepeatForever.create(animate); 
    					this.A08P04.runAction(animate_repeat);
    					this.A08P04.runAction(move_A08P01_repeat);
        			}
        			else if(cont_p2 == 8 && cont_p3 == 5){
    					this.addChild(this.A08P05, 0);
    					cc.spriteFrameCache.addSpriteFrames(res.A08P05_A);
    					var animFrames = [];
						var str = "";
						for (var i = 1; i < 13; i++) {
    						str = "A08P05_P" + (i < 12 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.3, 200);
						var animate   = cc.Animate.create(animation); 
						var animate_repeat = cc.RepeatForever.create(animate);     
    					this.A08P05.runAction(animate_repeat);
    					this.A08P05.runAction(move_A08P01_repeat);
        			}
        			else if(cont_p2 == 8 && cont_p3 == 6){
    					this.addChild(this.A08P06, 0);
    					cc.spriteFrameCache.addSpriteFrames(res.A08P06_A);
    					var animFrames = [];
						var str = "";
						for (var i = 1; i < 13; i++) {
    						str = "A08P06_P" + (i < 12 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.3, 200);
						var animate   = cc.Animate.create(animation);
						var animate_repeat = cc.RepeatForever.create(animate); 
    					this.A08P06.runAction(animate_repeat);
    					this.A08P06.runAction(move_A08P01_repeat);
        			}
        			else if(cont_p2 == 8 && cont_p3 == 7){
    					this.addChild(this.A08P07, 0);
    					cc.spriteFrameCache.addSpriteFrames(res.A08P07_A);
    					var animFrames = [];
						var str = "";
						for (var i = 1; i < 13; i++) {
    						str = "A08P07_P" + (i < 12 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.3, 200);
						var animate   = cc.Animate.create(animation);
						var animate_repeat = cc.RepeatForever.create(animate); 
    					this.A08P07.runAction(animate_repeat);
    					this.A08P07.runAction(move_A08P01_repeat);
        			}
        			else if(cont_p2 == 8 && cont_p3 == 8){
    					this.addChild(this.A08P08, 0);
    					cc.spriteFrameCache.addSpriteFrames(res.A08P08_A);
    					var animFrames = [];
						var str = "";
						for (var i = 1; i < 13; i++) {
    						str = "A08P08_P" + (i < 12 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.3, 200);
						var animate   = cc.Animate.create(animation);
						var animate_repeat = cc.RepeatForever.create(animate);  
    					this.A08P08.runAction(animate_repeat);
    					this.A08P08.runAction(move_A08P01_repeat);
        			}
        			else if(cont_p2 == 8 && cont_p3 == 9){
        				this.addChild(this.A08P09, 0);
        				cc.spriteFrameCache.addSpriteFrames(res.A08P09_A);
    					var animFrames = [];
						var str = "";
						for (var i = 1; i < 13; i++) {
    						str = "A08P09_P" + (i < 12 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.3, 200);
						var animate   = cc.Animate.create(animation);
						var animate_repeat = cc.RepeatForever.create(animate); 
    					this.A08P09.runAction(animate_repeat);
    					this.A08P09.runAction(move_A08P01_repeat);
        			}
        			else if(cont_p2 == 8 && cont_p3 ==10){
        				this.addChild(this.A08P10, 0);
        				cc.spriteFrameCache.addSpriteFrames(res.A08P10_A);
    					var animFrames = [];
						var str = "";
						for (var i = 1; i < 13; i++) {
    						str = "A08P10_P" + (i < 12 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.3, 200);
						var animate   = cc.Animate.create(animation); 
						var animate_repeat = cc.RepeatForever.create(animate); 
    					this.A08P10.runAction(animate_repeat);
    					this.A08P10.runAction(move_A08P01_repeat);
        			}
        			else if(cont_p2 == 8 && cont_p3 == 11){
        				this.addChild(this.A08P11, 0);
        				cc.spriteFrameCache.addSpriteFrames(res.A08P11_A);
    					var animFrames = [];
						var str = "";
						for (var i = 1; i < 13; i++) {
    						str = "A08P11_P" + (i < 12 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.3, 200);
						var animate   = cc.Animate.create(animation); 
						var animate_repeat = cc.RepeatForever.create(animate); 
    					this.A08P11.runAction(animate_repeat);
    					this.A08P11.runAction(move_A08P01_repeat);	
        			}
//	cont_p2 == 9 COME:
        			if(cont_p2 == 9 && cont_p3 == 1){
    					this.addChild(this.A03P01, 0);
    					cc.spriteFrameCache.addSpriteFrames(res.A03P01_A);
    					var animFrames = [];
						var str = "";
						for (var i = 1; i < 6; i++) {
    						str = "A03P01_P" + (i < 5 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.5, 200);
						var animate   = cc.Animate.create(animation); 
						var animate_repeat = cc.RepeatForever.create(animate); 
    					this.A03P01.runAction(animate_repeat);
        			}
        			else if(cont_p2 == 9 && cont_p3 == 2){
    					this.addChild(this.A03P02, 0);
    					cc.spriteFrameCache.addSpriteFrames(res.A03P02_A);
    					var animFrames = [];
						var str = "";
						for (var i = 1; i < 6; i++) {
    						str = "A03P02_P" + (i < 5 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.5, 200);
						var animate   = cc.Animate.create(animation); 
						var animate_repeat = cc.RepeatForever.create(animate); 
    					this.A03P02.runAction(animate_repeat);
        			}
        			else if(cont_p2 == 9 && cont_p3 == 3){
    					this.addChild(this.A03P03, 0);
    					cc.spriteFrameCache.addSpriteFrames(res.A03P03_A);
    					var animFrames = [];
						var str = "";
						for (var i = 1; i < 7; i++) {
    						str = "A03P03_P" + (i < 6 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.3, 200);
						var animate   = cc.Animate.create(animation);
						var animate_repeat = cc.RepeatForever.create(animate);  
    					this.A03P03.runAction(animate_repeat);
        			}
        			else if(cont_p2 == 9 && cont_p3 == 4){
    					this.addChild(this.A03P04, 0);
    					cc.spriteFrameCache.addSpriteFrames(res.A03P04_A);
    					var animFrames = [];
						var str = "";
						for (var i = 1; i < 6; i++) {
    						str = "A03P04_P" + (i < 5 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.5, 200);
						var animate   = cc.Animate.create(animation); 
						var animate_repeat = cc.RepeatForever.create(animate); 
    					this.A03P04.runAction(animate_repeat);
        			}
        			else if(cont_p2 == 9 && cont_p3 == 5){
    					this.addChild(this.A03P05, 0);
    					cc.spriteFrameCache.addSpriteFrames(res.A03P05_A);
    					var animFrames = [];
						var str = "";
						for (var i = 1; i < 6; i++) {
    						str = "A03P05_P" + (i < 5 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.5, 200);
						var animate   = cc.Animate.create(animation); 
						var animate_repeat = cc.RepeatForever.create(animate);     
    					this.A03P05.runAction(animate_repeat);
        			}
        			else if(cont_p2 == 9 && cont_p3 == 6){
    					this.addChild(this.A03P06, 0);
    					cc.spriteFrameCache.addSpriteFrames(res.A03P06_A);
    					var animFrames = [];
						var str = "";
						for (var i = 1; i < 6; i++) {
    						str = "A03P06_P" + (i < 5 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.5, 200);
						var animate   = cc.Animate.create(animation);
						var animate_repeat = cc.RepeatForever.create(animate); 
    					this.A03P06.runAction(animate_repeat);
        			}
        			else if(cont_p2 == 9 && cont_p3 == 7){
    					this.addChild(this.A03P07, 0);
    					cc.spriteFrameCache.addSpriteFrames(res.A03P07_A);
    					var animFrames = [];
						var str = "";
						for (var i = 1; i < 6; i++) {
    						str = "A03P07_P" + (i < 5 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.3, 200);
						var animate   = cc.Animate.create(animation);
						var animate_repeat = cc.RepeatForever.create(animate); 
    					this.A03P07.runAction(animate_repeat);
        			}
        			else if(cont_p2 == 9 && cont_p3 == 8){
    					this.addChild(this.A03P08, 0);
    					cc.spriteFrameCache.addSpriteFrames(res.A03P08_A);
    					var animFrames = [];
						var str = "";
						for (var i = 1; i < 6; i++) {
    						str = "A03P08_P" + (i < 5 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.5, 200);
						var animate   = cc.Animate.create(animation); 
						var animate_repeat = cc.RepeatForever.create(animate); 
    					this.A03P08.runAction(animate_repeat);
        			}
        			else if(cont_p2 == 9 && cont_p3 == 9){
        				this.addChild(this.A03P09, 0);
        				cc.spriteFrameCache.addSpriteFrames(res.A03P09_A);
    					var animFrames = [];
						var str = "";
						for (var i = 1; i < 6; i++) {
    						str = "A03P09_P" + (i < 5 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.3, 200);
						var animate   = cc.Animate.create(animation);
						var animate_repeat = cc.RepeatForever.create(animate); 
    					this.A03P09.runAction(animate_repeat);
        			}
        			else if(cont_p2 == 9 && cont_p3 ==10){
        				this.addChild(this.A03P10, 0);
        				cc.spriteFrameCache.addSpriteFrames(res.A03P10_A);
    					var animFrames = [];
						var str = "";
						for (var i = 1; i < 4; i++) {
    						str = "A03P10_P" + (i < 3 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.3, 200);
						var animate   = cc.Animate.create(animation); 
						var animate_repeat = cc.RepeatForever.create(animate); 
    					this.A03P10.runAction(animate_repeat);
        			}
        			else if(cont_p2 == 9 && cont_p3 == 11){
        				this.addChild(this.A03P11, 0);
        				cc.spriteFrameCache.addSpriteFrames(res.A03P11_A);
    					var animFrames = [];
						var str = "";
						for (var i = 1; i < 6; i++) {
    						str = "A03P11_P" + (i < 5 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.3, 100);
						var animate   = cc.Animate.create(animation);
						var animate_repeat = cc.RepeatForever.create(animate);  
    					this.A03P11.runAction(animate_repeat);
        			}
//	cont_p2 == 10 BEBE:
				//	1:
        			if(cont_p2 == 10 && cont_p3 == 1){
    					this.addChild(this.A10P01, 0);
    					cc.spriteFrameCache.addSpriteFrames(res.A10P01_A);
    					var animFrames = [];
						var str = "";
						for (var i = 3; i < 5; i++) {
    						str = "A10P01_P" + (i < 4 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.5, 200);
						var animate   = cc.Animate.create(animation); 
						var animate_repeat = cc.RepeatForever.create(animate); 
    					this.A10P01.runAction(animate_repeat);
        			}
        		//	2:
        			else if(cont_p2 ==10 && cont_p3 == 2){
    					this.addChild(this.A10P02, 0);
    					cc.spriteFrameCache.addSpriteFrames(res.A10P02_A);
    					var animFrames = [];
						var str = "";
						for (var i = 2; i < 5; i++) {
    						str = "A10P02_P" + (i < 4 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.5, 200);
						var animate   = cc.Animate.create(animation); 
						var animate_repeat = cc.RepeatForever.create(animate); 
    					this.A10P02.runAction(animate_repeat);
        			}
        		//	3:
        			else if(cont_p2 == 10 && cont_p3 == 3){
    					this.addChild(this.A10P03, 0);
    					cc.spriteFrameCache.addSpriteFrames(res.A10P03_A);
    					var animFrames = [];
						var str = "";
						for (var i = 2; i < 5; i++) {
    						str = "A10P03_P" + (i < 4 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.4, 200);
						var animate   = cc.Animate.create(animation); 
						var animate_repeat = cc.RepeatForever.create(animate); 
    					this.A10P03.runAction(animate_repeat);
        			}
        		//	4:
        			else if(cont_p2 == 10 && cont_p3 == 4){
    					this.addChild(this.A10P04, 0);
    					cc.spriteFrameCache.addSpriteFrames(res.A10P04_A);
    					var animFrames = [];
						var str = "";
						for (var i = 2; i < 5; i++) {
    						str = "A10P04_P" + (i < 4 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.5, 200);
						var animate   = cc.Animate.create(animation);
						var animate_repeat = cc.RepeatForever.create(animate);  
    					this.A10P04.runAction(animate_repeat);
        			}
        		//	5:
        			else if(cont_p2 == 10 && cont_p3 == 5){
    					this.addChild(this.A10P05, 0);
    					cc.spriteFrameCache.addSpriteFrames(res.A10P05_A);
    					var animFrames = [];
						var str = "";
						for (var i = 2; i < 5; i++) {
    						str = "A10P05_P" + (i < 4 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.5, 200);
						var animate   = cc.Animate.create(animation);
						var animate_repeat = cc.RepeatForever.create(animate);      
    					this.A10P05.runAction(animate_repeat);
        			}
        		//	6:
        			else if(cont_p2 == 10 && cont_p3 == 6){
    					this.addChild(this.A10P06, 0);
    					cc.spriteFrameCache.addSpriteFrames(res.A10P06_A);
    					var animFrames = [];
						var str = "";
						for (var i = 2; i < 4; i++) {
    						str = "A10P06_P" + (i < 3 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.5, 200);
						var animate   = cc.Animate.create(animation);
						var animate_repeat = cc.RepeatForever.create(animate); 
    					this.A10P06.runAction(animate_repeat);
        			}
        		//	7:
        			else if(cont_p2 == 10 && cont_p3 == 7){
    					this.addChild(this.A10P07, 0);
    					cc.spriteFrameCache.addSpriteFrames(res.A10P07_A);
    					var animFrames = [];
						var str = "";
						for (var i = 2; i < 5; i++) {
    						str = "A10P07_P" + (i < 4 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.4, 200);
						var animate   = cc.Animate.create(animation);
						var animate_repeat = cc.RepeatForever.create(animate); 
    					this.A10P07.runAction(animate_repeat);
        			}
        		//	8:
        			else if(cont_p2 == 10 && cont_p3 == 8){
    					this.addChild(this.A10P08, 0);
    					cc.spriteFrameCache.addSpriteFrames(res.A10P08_A);
    					var animFrames = [];
						var str = "";
						for (var i = 2; i < 4; i++) {
    						str = "A10P08_P" + (i < 3 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.5, 200);
						var animate   = cc.Animate.create(animation); 
						var animate_repeat = cc.RepeatForever.create(animate); 
    					this.A10P08.runAction(animate_repeat);
        			}
        		//	9:
        			else if(cont_p2 == 10 && cont_p3 == 9){
        				this.addChild(this.A10P09, 0);
        				cc.spriteFrameCache.addSpriteFrames(res.A10P09_A);
    					var animFrames = [];
						var str = "";
						for (var i = 2; i < 5; i++) {
    						str = "A10P09_P" + (i < 4 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.4, 200);
						var animate   = cc.Animate.create(animation);
						var animate_repeat = cc.RepeatForever.create(animate); 
    					this.A10P09.runAction(animate_repeat);
        			}
        		//	10:
        			else if(cont_p2 == 10 && cont_p3 ==10){
        				this.addChild(this.A10P10, 0);
        				cc.spriteFrameCache.addSpriteFrames(res.A10P10_A);
    					var animFrames = [];
						var str = "";
						for (var i = 2; i < 4; i++) {
    						str = "A10P10_P" + (i < 3 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.4, 200);
						var animate   = cc.Animate.create(animation); 
						var animate_repeat = cc.RepeatForever.create(animate); 
    					this.A10P10.runAction(animate_repeat);
        			}
        		//	11:
        			else if(cont_p2 == 10 && cont_p3 == 11){
        				this.addChild(this.A10P11, 0);
        				cc.spriteFrameCache.addSpriteFrames(res.A10P11_A);
    					var animFrames = [];
						var str = "";
						for (var i = 2; i < 5; i++) {
    						str = "A10P11_P" + (i < 4 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.4, 200);
						var animate   = cc.Animate.create(animation);
						var animate_repeat = cc.RepeatForever.create(animate);  
    					this.A10P11.runAction(animate_repeat);
        			}
//	cont_p2 == 11 CANTA:
        			if(cont_p2 == 11 && cont_p3 == 1){
    					this.addChild(this.A11P01, 0);
    					cc.spriteFrameCache.addSpriteFrames(res.A11P01_A);
    					var animFrames = [];
						var str = "";
						for (var i = 1; i < 3; i++) {
    						str = "A11P01_P" + (i < 2 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.5, 200);
						var animate   = cc.Animate.create(animation); 
						var animate_repeat = cc.RepeatForever.create(animate); 
    					this.A11P01.runAction(animate_repeat);
        			}
        			else if(cont_p2 == 11 && cont_p3 == 2){
    					this.addChild(this.A11P02, 0);
    					cc.spriteFrameCache.addSpriteFrames(res.A11P02_A);
    					var animFrames = [];
						var str = "";
						for (var i = 1; i < 3; i++) {
    						str = "A11P02_P" + (i < 2 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.5, 200);
						var animate   = cc.Animate.create(animation); 
						var animate_repeat = cc.RepeatForever.create(animate); 
    					this.A11P02.runAction(animate_repeat);
        			}
        			else if(cont_p2 == 11 && cont_p3 == 3){
    					this.addChild(this.A11P03, 0);
    					cc.spriteFrameCache.addSpriteFrames(res.A11P03_A);
    					var animFrames = [];
						var str = "";
						for (var i = 1; i < 3; i++) {
    						str = "A11P03_P" + (i < 2 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.5, 200);
						var animate   = cc.Animate.create(animation);
						var animate_repeat = cc.RepeatForever.create(animate);  
    					this.A11P03.runAction(animate_repeat);
        			}
        			else if(cont_p2 == 11 && cont_p3 == 4){
    					this.addChild(this.A11P04, 0);
    					cc.spriteFrameCache.addSpriteFrames(res.A11P04_A);
    					var animFrames = [];
						var str = "";
						for (var i = 1; i < 3; i++) {
    						str = "A11P04_P" + (i < 2 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.5, 200);
						var animate   = cc.Animate.create(animation); 
						var animate_repeat = cc.RepeatForever.create(animate); 
    					this.A11P04.runAction(animate_repeat);
        			}
        			else if(cont_p2 == 11 && cont_p3 == 5){
    					this.addChild(this.A11P05, 0);
    					cc.spriteFrameCache.addSpriteFrames(res.A11P05_A);
    					var animFrames = [];
						var str = "";
						for (var i = 1; i < 3; i++) {
    						str = "A11P05_P" + (i < 2 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.5, 200);
						var animate   = cc.Animate.create(animation);
						var animate_repeat = cc.RepeatForever.create(animate);      
    					this.A11P05.runAction(animate_repeat);
        			}
        			else if(cont_p2 == 11 && cont_p3 == 6){
    					this.addChild(this.A11P06, 0);
    					cc.spriteFrameCache.addSpriteFrames(res.A11P06_A);
    					var animFrames = [];
						var str = "";
						for (var i = 1; i < 3; i++) {
    						str = "A11P06_P" + (i < 2 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.5, 200);
						var animate   = cc.Animate.create(animation);
						var animate_repeat = cc.RepeatForever.create(animate); 
    					this.A11P06.runAction(animate_repeat);
        			}
        			else if(cont_p2 == 11 && cont_p3 == 7){
    					this.addChild(this.A11P07, 0);
    					cc.spriteFrameCache.addSpriteFrames(res.A11P07_A);
    					var animFrames = [];
						var str = "";
						for (var i = 1; i < 3; i++) {
    						str = "A11P07_P" + (i < 2 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.5, 200);
						var animate   = cc.Animate.create(animation);
						var animate_repeat = cc.RepeatForever.create(animate); 
    					this.A11P07.runAction(animate_repeat);
        			}
        			else if(cont_p2 == 11 && cont_p3 == 8){
    					this.addChild(this.A11P08, 0);
    					cc.spriteFrameCache.addSpriteFrames(res.A11P08_A);
    					var animFrames = [];
						var str = "";
						for (var i = 1; i < 3; i++) {
    						str = "A11P08_P" + (i < 2 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.5, 200);
						var animate   = cc.Animate.create(animation); 
						var animate_repeat = cc.RepeatForever.create(animate); 
    					this.A11P08.runAction(animate_repeat);
        			}
        			else if(cont_p2 == 11 && cont_p3 == 9){
        				this.addChild(this.A11P09, 0);
        				cc.spriteFrameCache.addSpriteFrames(res.A11P09_A);
    					var animFrames = [];
						var str = "";
						for (var i = 1; i < 3; i++) {
    						str = "A11P09_P" + (i < 2 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.5, 200);
						var animate   = cc.Animate.create(animation);
						var animate_repeat = cc.RepeatForever.create(animate); 
    					this.A11P09.runAction(animate_repeat);
        			}
        			else if(cont_p2 == 11 && cont_p3 ==10){
        				this.addChild(this.A11P10, 0);
        				cc.spriteFrameCache.addSpriteFrames(res.A11P10_A);
    					var animFrames = [];
						var str = "";
						for (var i = 1; i < 3; i++) {
    						str = "A11P10_P" + (i < 2 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.5, 200);
						var animate   = cc.Animate.create(animation); 
						var animate_repeat = cc.RepeatForever.create(animate); 
    					this.A11P10.runAction(animate_repeat);
        			}
        			else if(cont_p2 == 11 && cont_p3 == 11){
        				this.addChild(this.A11P11, 0);
        				cc.spriteFrameCache.addSpriteFrames(res.A11P11_A);
    					var animFrames = [];
						var str = "";
						for (var i = 1; i < 3; i++) {
    						str = "A11P11_P" + (i < 2 ? (i) : i) + ".png";
    						var spriteFrame = cc.spriteFrameCache.getSpriteFrame(str);
    						var animFrame = new cc.AnimationFrame();
            					animFrame.initWithSpriteFrame(spriteFrame, 1, null);
    							animFrames.push(animFrame);
						}
						var animation = cc.Animation.create(animFrames, 0.5, 200);
						var animate   = cc.Animate.create(animation); 
						var animate_repeat = cc.RepeatForever.create(animate); 
    					this.A11P11.runAction(animate_repeat);
        			}

        			anterior_p1 = cont_p1;
					anterior_p2 = cont_p2;
					anterior_p3 = cont_p3;

				break; 
				}

        }
    }

});

HelloWorldScene2.scene = function(){
        var scene =  new cc.Scene();
        var layer = new HelloWorldScene2();
		scene.addChild(layer);
		return scene;
}

//	ESCENA PRINCIPAL 

var HelloWorldLayer = cc.Layer.extend({
	sprint:null,
	ctor:function() {
		this._super();
		this.init();
	},
	init:function() {
		this._super();
		var size = cc.director.getWinSize(); // Obtener tamaño de pantalla
		
		var Presentacion = cc.Sprite.create(res.BG_START_1);
		Presentacion.setPosition(size.width/2, size.height / 2);
		this.addChild(Presentacion, kZindexBG);
	
	//	Sprite de titulo:
		var Titulo = cc.Sprite.create(res.TITULO);
		Titulo.setPosition(2900, 600);
		this.addChild(Titulo, 0);

		var Titulo_action_1 = cc.JumpTo.create(8, cc.p(550, 300), 100, 6);
		var Titulo_action_2 = cc.JumpTo.create(4, cc.p(size.width/2, size.height / 2), 80, 6);
		var Titulo_sequence = cc.Sequence.create(Titulo_action_1, Titulo_action_2);
		Titulo.runAction(Titulo_sequence);

	//	Sprite de traduccion de titulo:
		var T_titulo = cc.Sprite.create(res.T_TITULO);
		T_titulo.setPosition(2800, 400);
		this.addChild(T_titulo, 0);

		//	Animacion de traduccion de texto:

		var T_titulo_action_1 = cc.MoveBy.create(12, 0, 0);
		var T_titulo_action_2 = cc.MoveBy.create(2, -1800, 0);
		var T_titulo_sequence = cc.Sequence.create(T_titulo_action_1, T_titulo_action_2);
		T_titulo.runAction(T_titulo_sequence);

	//	Texto:
		var Introduccion = cc.Sprite.create(res.INTRODUCCION);
		Introduccion.setPosition(size.width/2, 2000);
		this.addChild(Introduccion, 0);

		//	ANIMACION DE TEXTO:
		var Introduccion_action_1 = cc.MoveBy.create(16, 0, 0);
		var Introduccion_action_2 = cc.MoveBy.create(1, 0, -1460);
		var Introduccion_sequence = cc.Sequence.create(Introduccion_action_1, Introduccion_action_2);
		Introduccion.runAction(Introduccion_sequence);

	//	Button play:
		var Button_play = new ccui.Button();
		Button_play.loadTextures(res.BUTTON_PLAY);
		Button_play.setPosition(cc.p(3000, 100));
		Button_play.addTouchEventListener(this.touchEvent, this);
		this.addChild(Button_play, 0, "Button_play");

		//	ANIMACION DE BOTON:
		var Button_play_action_1 = cc.MoveBy.create(16, 0, 0);
		var Button_play_action_2 = cc.MoveBy.create(1, -1200, 0);
		var Button_play_sequence = cc.Sequence.create(Button_play_action_1, Button_play_action_2);
		Button_play.runAction(Button_play_sequence);

	//	Button about:
		var Button_about = new ccui.Button();
		Button_about.loadTextures(res.BUTTON_ABOUT);
		Button_about.setPosition(cc.p(-180, 100));
		Button_about.addTouchEventListener(this.touchEvent, this);
		this.addChild(Button_about, 0, "Button_about");

		//	ANIMACION DE BOTON:
		var Button_about_action_1 = cc.MoveBy.create(16, 0, 0);
		var Button_about_action_2 = cc.MoveBy.create(1, 350, 0);
		var Button_about_sequence = cc.Sequence.create(Button_about_action_1, Button_about_action_2);
		Button_about.runAction(Button_about_sequence);

		cc.audioEngine.playMusic(res.INTRO_MUSIC, true);
		cc.audioEngine.setMusicVolume(0.2);
	},
	onEnter:function() {
		this._super();
	},
	touchEvent: function(sender, type)
    {
    	var object_name = sender.getName(); //	Obtiene el nombre del evento

        switch (type)
        {
            case ccui.Widget.TOUCH_BEGAN:

            	if(object_name == "Button_play"){
            		cc.director.runScene(HelloWorldScene2.scene());
            	}
            	
            	if(object_name == "Button_about"){
            		cc.director.runScene(HelloWorldScene4.scene());
            	}
            	break;
                           
        }
    }
});

var HelloWorldScene = cc.Scene.extend({
   onEnter:function () {
        this._super();
        var layer = new HelloWorldLayer();
        this.addChild(layer);
    }
});
