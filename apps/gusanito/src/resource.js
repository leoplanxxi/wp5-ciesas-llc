var folder = "";
var folder_sounds = "";
var folder_images = "";

var kZindexBG = 0;
var kZindexFloor = 40;
var kZindexMenu = 0;


if (!cc.sys.isNative)
{
    folder = "res/mediumRes/";
    folder_sounds = "res/sounds/";

    folder_sounds_Accion = "res/sounds/Acciones/";
    folder_sounds_Lugares = "res/sounds/Lugares/";
    folder_sounds_Personajes = "res/sounds/Personajes/";
    folder_sounds_Efectos = "res/sounds/Efectos/";


    folder_images = "res/images/";

    folder_images_escenario = "res/images/escenario/";
    folder_images_personajes = "res/images/personajes/";
    
    folder_images_F_Lugares = "res/images/Frases/Lugares/";
    folder_images_F_Accion = "res/images/Frases/Accion/";
    folder_images_F_Personajes = "res/images/Frases/Personajes/";

    folder_images_T_Lugares = "res/images/Frases/T_lugares/";
    folder_images_T_Acciones = "res/images/Frases/T_acciones/";
    folder_images_T_Personajes = "res/images/Frases/T_personajes/";

}

var res = {
    HelloWorld_png : folder + "HelloWorld.png",
    CloseNormal_png : folder + "CloseNormal.png",
    CloseSelected_png : folder+ "CloseSelected.png",

//IMAGES DE FONDO:
    BG_START : folder_images + "background_m.png",
    BG_START_1 : folder_images + "Background_Presentacion.png",
    BG_ABOUT : folder_images + "BG_AcercaDe.png",
    ANUNCIO: folder_images + "anuncio.png",
    
    INTRODUCCION : folder_images + "Background_Introduccion.png",
    TITULO : folder_images + "titulo.png",
    T_TITULO: folder_images + "Titulo_traduccion.png",


    GUSANITO : folder_images + "gusanito1.png",
    BODY_GUSANITO : folder_images + "capsulav3.png",
    NECK_GUSANITO : folder_images + "cuelloGusano.png",

//BOTONES:

    BUTTON_ABOUT : folder_images + "Boton_AcercaDe.png",
    BUTTON_PLAY : folder_images + "Boton_Jugar.png",
    BUTTON_BEFORE : folder_images + "Button_before.png",

    BUTTON_ARRIBA : folder_images + "Button_arriba.png",
    BUTTON_ABAJO : folder_images + "Button_abajo.png",
    BUTTON_VIRGULA : folder_images + "virgula.png",
    
//  AUDIOS DE INTRODUCCION:
    MAIN_MUSIC: folder_sounds + "Audio_juego.mp3",
    INTRO_MUSIC: folder_sounds + "Audio_intro.mp3",

//  Audio Lugares:
    SL1 : folder_sounds_Lugares + "ka ra dathe (en el rio).mp3",
    SL2 : folder_sounds_Lugares + "ka ra denda (en la tienda).mp3",
    SL3 : folder_sounds_Lugares + "ka ra mbothi (en el agua sucia).mp3",
    SL4 : folder_sounds_Lugares + "ka ra ngu (la casa).mp3",
    SL5 : folder_sounds_Lugares + "ka ra ntoi (en la plaza).mp3",
    SL6 : folder_sounds_Lugares + "ka ra to'ti (en el pozo).mp3",
    SL7 : folder_sounds_Lugares + "ka ra (en el camino).mp3",
    SL8 : folder_sounds_Lugares + "ka ra thi (afuera).mp3",
    SL9 : folder_sounds_Lugares + "ka ra wahi (en la milpa).mp3",
    SL10 : folder_sounds_Lugares + "ka ra xanthe (en el cerro).mp3",
    SL11 : folder_sounds_Lugares + "ka ra za (en el arbol).mp3",

//  Audio Accion:
    SA1 : folder_sounds_Accion + "aha (duerme).mp3",
    SA2 : folder_sounds_Accion + "hekuari (corre).mp3",
    SA3 : folder_sounds_Accion + "ts'oki (muerde).mp3",
    SA4 : folder_sounds_Accion + "nxoki (brinca).mp3",
    SA5 : folder_sounds_Accion + "nzoni (vuela).mp3",
    SA6 : folder_sounds_Accion + "na (habla).mp3",
    SA7 : folder_sounds_Accion + "'yo (camina).mp3",
    SA8 : folder_sounds_Accion + "rats'u (cruza).mp3",
    SA9 : folder_sounds_Accion + "tsi (come).mp3",
    SA10 : folder_sounds_Accion + "tsithe (toma).mp3",
    SA11 : folder_sounds_Accion + "tuhu (canta).mp3",

//  Audio personajes:
    SP1 : folder_sounds_Personajes + "ma juada (mi hermano).mp3",
    SP2 : folder_sounds_Personajes + "ra batsi (el nino).mp3",
    SP3 : folder_sounds_Personajes + "ra jua (el conejo).mp3",
    SP4 : folder_sounds_Personajes + "ra me (la mama).mp3",
    SP5 : folder_sounds_Personajes + "ra nana (la senora).mp3",
    SP6 : folder_sounds_Personajes + "ra dahta (el abuelo).mp3",
    SP7 : folder_sounds_Personajes + "ra ngoro (el guajolote).mp3",
    SP8 : folder_sounds_Personajes + "ra nzaya (el senor).mp3",
    SP9 : folder_sounds_Personajes + "ra oni (el pollo).mp3",
    SP10 : folder_sounds_Personajes + "ra tsanga (la lagartija).mp3",
    SP11 : folder_sounds_Personajes + "ra ts'unts'u (el pajaro).mp3",
      
//  Efectos:
    E_A01P01 : folder_sounds_Efectos + "E_A01P01.mp3",
    E_A01P07 : folder_sounds_Efectos + "E_A01P07.mp3",
    E_A02 : folder_sounds_Efectos + "E_A02.mp3",
    E_A03P01 : folder_sounds_Efectos + "E_A03P01.mp3",
    E_A03P07 : folder_sounds_Efectos + "E_A03P07.mp3",
    E_A04 : folder_sounds_Efectos + "E_A04.mp3",
    E_A05 : folder_sounds_Efectos + "E_A05.mp3",
    E_A06P01 : folder_sounds_Efectos + "E_A06P01.mp3",
    E_A06P03_10 : folder_sounds_Efectos + "E_A06P03_10.mp3",
    E_A07P01 : folder_sounds_Efectos + "E_A07P01.mp3",
    E_A07P01_L01 : folder_sounds_Efectos + "E_A07P01_L01.mp3",
    E_A08 : folder_sounds_Efectos + "E_A08.mp3",
    E_A09P01 : folder_sounds_Efectos + "E_A09P01.mp3",
    E_A09P07 : folder_sounds_Efectos + "E_A09P07.mp3",
    E_A09P10 : folder_sounds_Efectos + "E_A09P10.mp3",
    E_A10P01 : folder_sounds_Efectos + "E_A10P01.mp3",
    E_A10P03 : folder_sounds_Efectos + "E_A10P03.mp3",
    E_A11P01 : folder_sounds_Efectos + "E_A11P01.mp3",
    E_A11P03_10 : folder_sounds_Efectos + "E_A11P03_10.mp3",
    E_A11P04 : folder_sounds_Efectos + "E_A11P04.mp3",
    E_A11P07 : folder_sounds_Efectos + "E_A11P07.mp3",
    E_A11P09 : folder_sounds_Efectos + "E_A11P09.mp3",
    E_A11P11 : folder_sounds_Efectos + "E_A11P11.mp3",


//  IMAGENES ESCENARIOS:
    //  Imagen Lugares:
    IL1 : folder_images_escenario + "el_rio.png",
    IL2 : folder_images_escenario + "la_tienda.jpg",
    IL3 : folder_images_escenario + "el_agua_sucia.png",
    IL4 : folder_images_escenario + "la_casa.png",
    IL5 : folder_images_escenario + "la_plaza.png",
    IL6 : folder_images_escenario + "el_pozo.png",
    IL7 : folder_images_escenario + "el_camino.png",
    IL8 : folder_images_escenario + "el_patio.png",
    IL9 : folder_images_escenario + "la_milpa.png",
    IL10 : folder_images_escenario + "el_cerro.png",
    IL11 : folder_images_escenario + "el_arbol.png",

//////////////////  A01     //////////////////
    A01P01_P1 : folder_images_personajes + "A01P01_P1.png",
    A01P01_P2 : folder_images_personajes + "A01P01_P2.png",
    A01P01_A : folder_images_personajes + "A01P01_A.plist",

    A01P02_P1 : folder_images_personajes + "A01P02_P1.png",
    A01P02_P2 : folder_images_personajes + "A01P02_P2.png",
    A01P02_A : folder_images_personajes + "A01P02_A.plist",

    A01P03_P1 : folder_images_personajes + "A01P03_P1.png",
    A01P03_P2 : folder_images_personajes + "A01P03_P2.png",
    A01P03_A : folder_images_personajes + "A01P03_A.plist",

    A01P04_P1 : folder_images_personajes + "A01P04_P1.png",
    A01P04_P2 : folder_images_personajes + "A01P04_P2.png",
    A01P04_A : folder_images_personajes + "A01P04_A.plist",

    A01P05_P1 : folder_images_personajes + "A01P05_P1.png",
    A01P05_P2 : folder_images_personajes + "A01P05_P2.png",
    A01P05_A : folder_images_personajes + "A01P05_A.plist",

    A01P06_P1 : folder_images_personajes + "A01P06_P1.png",
    A01P06_P2 : folder_images_personajes + "A01P06_P2.png",
    A01P06_A : folder_images_personajes + "A01P06_A.plist",

    A01P07_P1 : folder_images_personajes + "A01P07_P1.png",
    A01P07_P2 : folder_images_personajes + "A01P07_P2.png",
    A01P07_A : folder_images_personajes + "A01P07_A.plist",

    A01P08_P1 : folder_images_personajes + "A01P08_P1.png",
    A01P08_P2 : folder_images_personajes + "A01P08_P2.png",
    A01P08_A : folder_images_personajes + "A01P08_A.plist",

    A01P09_P1 : folder_images_personajes + "A01P09_P1.png",
    A01P09_P2 : folder_images_personajes + "A01P09_P2.png",
    A01P09_A: folder_images_personajes + "A01P09_A.plist",
    
    A01P10_P1 : folder_images_personajes + "A01P10_P1.png",
    A01P10_P2 : folder_images_personajes + "A01P10_P2.png",
    A01P10_P3 : folder_images_personajes + "A01P10_P3.png",
    A01P10_A : folder_images_personajes + "A01P10_A.plist",

    A01P11_P1 : folder_images_personajes + "A01P11_P1.png",
    A01P11_P2 : folder_images_personajes + "A01P11_P2.png",
    A01P11_A : folder_images_personajes + "A01P11_A.plist",

//////////////////	A02		//////////////////

    A02P01_P1 : folder_images_personajes + "A02P01_P1.png",
    A02P01_P2 : folder_images_personajes + "A02P01_P2.png",
    A02P01_P3 : folder_images_personajes + "A02P01_P3.png",
    A02P01_A : folder_images_personajes + "A02P01_A.plist",

    A02P02_P1 : folder_images_personajes + "A02P02_P1.png",
    A02P02_P2 : folder_images_personajes + "A02P02_P2.png",
    A02P02_P3 : folder_images_personajes + "A02P02_P3.png",
    A02P02_A : folder_images_personajes + "A02P02_A.plist",

    //A02P03_P1 : folder_images_personajes + "A02P03_P1.png",
    //A02P03_P2 : folder_images_personajes + "A02P03_P2.png",
    //A02P03_A : folder_images_personajes + "A02P03_A.plist",

    A02P04_P1 : folder_images_personajes + "A02P04_P1.png",
    A02P04_P2 : folder_images_personajes + "A02P04_P2.png",
    A02P04_P3 : folder_images_personajes + "A02P04_P3.png",
    A02P04_A : folder_images_personajes + "A02P04_A.plist",

    A02P05_P1 : folder_images_personajes + "A02P05_P1.png",
    A02P05_P2 : folder_images_personajes + "A02P05_P2.png",
    A02P05_P3 : folder_images_personajes + "A02P05_P3.png",
    A02P05_A : folder_images_personajes + "A02P05_A.plist",

    A02P06_P1 : folder_images_personajes + "A02P06_P1.png",
    A02P06_P2 : folder_images_personajes + "A02P06_P2.png",
    A02P06_P3 : folder_images_personajes + "A02P06_P3.png",
    A02P06_A : folder_images_personajes + "A02P06_A.plist",

    A02P07_P1 : folder_images_personajes + "A02P07_P1.png",
    A02P07_P2 : folder_images_personajes + "A02P07_P2.png",
    A02P07_P3 : folder_images_personajes + "A02P07_P3.png",
    A02P07_P4 : folder_images_personajes + "A02P07_P4.png",
    A02P07_A : folder_images_personajes + "A02P07_A.plist",

    A02P08_P1 : folder_images_personajes + "A02P08_P1.png",
    A02P08_P2 : folder_images_personajes + "A02P08_P2.png",
    A02P08_P3 : folder_images_personajes + "A02P08_P3.png",
    A02P08_A : folder_images_personajes + "A02P08_A.plist",

    A02P09_P1 : folder_images_personajes + "A02P09_P1.png",
    A02P09_P2 : folder_images_personajes + "A02P09_P2.png",
    A02P09_P3 : folder_images_personajes + "A02P09_P3.png",
    A02P09_P4 : folder_images_personajes + "A02P09_P4.png",
    A02P09_A: folder_images_personajes + "A02P09_A.plist",
    
    A02P10_P1 : folder_images_personajes + "A02P10_P1.png",
    A02P10_P2 : folder_images_personajes + "A02P10_P2.png",
    A02P10_A : folder_images_personajes + "A02P10_A.plist",

    A02P11_P1 : folder_images_personajes + "A02P11_P1.png",
    A02P11_P2 : folder_images_personajes + "A02P11_P2.png",
    A02P11_P3 : folder_images_personajes + "A02P11_P3.png",
    A02P11_P4 : folder_images_personajes + "A02P11_P4.png",
    A02P11_A : folder_images_personajes + "A02P11_A.plist",

//  A03    

    A03P01_P1 : folder_images_personajes + "A03P01_P1.png",
    A03P01_P2 : folder_images_personajes + "A03P01_P2.png",
    A03P01_P3 : folder_images_personajes + "A03P01_P3.png",
    A03P01_P4 : folder_images_personajes + "A03P01_P4.png",
    A03P01_P5 : folder_images_personajes + "A03P01_P5.png",
    A03P01_A : folder_images_personajes + "A03P01_A.plist",

    A03P02_P1 : folder_images_personajes + "A03P02_P1.png",
    A03P02_P2 : folder_images_personajes + "A03P02_P2.png",
    A03P02_P3 : folder_images_personajes + "A03P02_P3.png",
    A03P02_P4 : folder_images_personajes + "A03P02_P4.png",
    A03P02_P5 : folder_images_personajes + "A03P02_P5.png",
    A03P02_A : folder_images_personajes + "A03P02_A.plist",

    A03P03_P1 : folder_images_personajes + "A03P03_P1.png",
    A03P03_P2 : folder_images_personajes + "A03P03_P2.png",
    A03P03_P3 : folder_images_personajes + "A03P03_P3.png",
    A03P03_P4 : folder_images_personajes + "A03P03_P4.png",
    A03P03_P5 : folder_images_personajes + "A03P03_P5.png",
    A03P03_P6 : folder_images_personajes + "A03P03_P6.png",
    A03P03_A : folder_images_personajes + "A03P03_A.plist",

    A03P04_P1 : folder_images_personajes + "A03P04_P1.png",
    A03P04_P2 : folder_images_personajes + "A03P04_P2.png",
    A03P04_P3 : folder_images_personajes + "A03P04_P3.png",
    A03P04_P4 : folder_images_personajes + "A03P04_P4.png",
    A03P04_P5 : folder_images_personajes + "A03P04_P5.png",
    A03P04_A : folder_images_personajes + "A03P04_A.plist",

    A03P05_P1 : folder_images_personajes + "A03P05_P1.png",
    A03P05_P2 : folder_images_personajes + "A03P05_P2.png",
    A03P05_P3 : folder_images_personajes + "A03P05_P3.png",
    A03P05_P4 : folder_images_personajes + "A03P05_P4.png",
    A03P05_P5 : folder_images_personajes + "A03P05_P5.png",
    A03P05_A : folder_images_personajes + "A03P05_A.plist",

    A03P06_P1 : folder_images_personajes + "A03P06_P1.png",
    A03P06_P2 : folder_images_personajes + "A03P06_P2.png",
    A03P06_P3 : folder_images_personajes + "A03P06_P3.png",
    A03P06_P4 : folder_images_personajes + "A03P06_P4.png",
    A03P06_P5 : folder_images_personajes + "A03P06_P5.png",
    A03P06_A : folder_images_personajes + "A03P06_A.plist",

    A03P07_P1 : folder_images_personajes + "A03P07_P1.png",
    A03P07_P2 : folder_images_personajes + "A03P07_P2.png",
    A03P07_P3 : folder_images_personajes + "A03P07_P3.png",
    A03P07_P4 : folder_images_personajes + "A03P07_P4.png",
    A03P07_P5 : folder_images_personajes + "A03P07_P5.png",
    A03P07_A : folder_images_personajes + "A03P07_A.plist",

    A03P08_P1 : folder_images_personajes + "A03P08_P1.png",
    A03P08_P2 : folder_images_personajes + "A03P08_P2.png",
    A03P08_P3 : folder_images_personajes + "A03P08_P3.png",
    A03P08_P4 : folder_images_personajes + "A03P08_P4.png",
    A03P08_P5 : folder_images_personajes + "A03P08_P5.png",
    A03P08_A : folder_images_personajes + "A03P08_A.plist",

    A03P09_P1 : folder_images_personajes + "A03P09_P1.png",
    A03P09_P2 : folder_images_personajes + "A03P09_P2.png",
    A03P09_P3 : folder_images_personajes + "A03P09_P3.png",
    A03P09_P4 : folder_images_personajes + "A03P09_P4.png",
    A03P09_P5 : folder_images_personajes + "A03P09_P5.png",
    A03P09_A : folder_images_personajes + "A03P09_A.plist",
    
    A03P10_P1 : folder_images_personajes + "A03P10_P1.png",
    A03P10_P2 : folder_images_personajes + "A03P10_P2.png",
    A03P10_P3 : folder_images_personajes + "A03P10_P3.png",
    A03P10_A : folder_images_personajes + "A03P10_A.plist",

    A03P11_P1 : folder_images_personajes + "A03P11_P1.png",
    A03P11_P2 : folder_images_personajes + "A03P11_P2.png",
    A03P11_P3 : folder_images_personajes + "A03P11_P3.png",
    A03P11_P4 : folder_images_personajes + "A03P11_P4.png",
    A03P11_P5 : folder_images_personajes + "A03P11_P5.png",
    A03P11_A : folder_images_personajes + "A03P11_A.plist",

//////////////////  A04     //////////////////

    A04P01_P1 : folder_images_personajes + "A04P01_P1.png",
    A04P01_P2 : folder_images_personajes + "A04P01_P2.png",
    A04P01_A : folder_images_personajes + "A04P01_A.plist",

    A04P02_P1 : folder_images_personajes + "A04P02_P1.png",
    A04P02_P2 : folder_images_personajes + "A04P02_P2.png",
    A04P02_A : folder_images_personajes + "A04P02_A.plist",

    A08P03_P1 : folder_images_personajes + "A08P03_P1.png",
    A08P03_P2 : folder_images_personajes + "A08P03_P2.png",
    A08P03_P3 : folder_images_personajes + "A08P03_P3.png",
    A08P03_P4 : folder_images_personajes + "A08P03_P4.png",
    A04P03_A : folder_images_personajes + "A04P03_A.plist",

    A04P04_P1 : folder_images_personajes + "A04P04_P1.png",
    A04P04_P2 : folder_images_personajes + "A04P04_P2.png",
    A04P04_A : folder_images_personajes + "A04P04_A.plist",

    A04P05_P1 : folder_images_personajes + "A04P05_P1.png",
    A04P05_P2 : folder_images_personajes + "A04P05_P2.png",
    A04P05_A : folder_images_personajes + "A04P05_A.plist",

    A04P06_P1 : folder_images_personajes + "A04P06_P1.png",
    A04P06_P2 : folder_images_personajes + "A04P06_P2.png",
    A04P06_A : folder_images_personajes + "A04P06_A.plist",

    A04P07_P1 : folder_images_personajes + "A04P07_P1.png",
    A04P07_P2 : folder_images_personajes + "A04P07_P2.png",
    A04P07_A : folder_images_personajes + "A04P07_A.plist",

    A04P08_P1 : folder_images_personajes + "A04P08_P1.png",
    A04P08_P2 : folder_images_personajes + "A04P08_P2.png",
    A04P08_A : folder_images_personajes + "A04P08_A.plist",

    A04P09_P1 : folder_images_personajes + "A04P09_P1.png",
    A04P09_P2 : folder_images_personajes + "A04P09_P2.png",
    A04P09_A: folder_images_personajes + "A04P09_A.plist",
    
    A04P10_P1 : folder_images_personajes + "A04P10_P1.png",
    A04P10_P2 : folder_images_personajes + "A04P10_P2.png",
    A04P10_P3 : folder_images_personajes + "A04P10_P3.png",
    A04P10_A : folder_images_personajes + "A04P10_A.plist",

    A04P11_P1 : folder_images_personajes + "A04P11_P1.png",
    A04P11_P2 : folder_images_personajes + "A04P11_P2.png",
    A04P11_A : folder_images_personajes + "A04P11_A.plist",



//////////////////  A05    //////////////////

    A05P01_P1 : folder_images_personajes + "A05P01_P1.png",
    A05P01_P2 : folder_images_personajes + "A05P01_P2.png",
    A05P01_P3 : folder_images_personajes + "A05P01_P3.png",
    A05P01_A : folder_images_personajes + "A05P01_A.plist",

    A05P02_P1 : folder_images_personajes + "A05P02_P1.png",
    A05P02_P2 : folder_images_personajes + "A05P02_P2.png",
    A05P02_P3 : folder_images_personajes + "A05P02_P3.png",
    A05P02_A : folder_images_personajes + "A05P02_A.plist",

    A05P03_P1 : folder_images_personajes + "A05P03_P1.png",
    A05P03_P2 : folder_images_personajes + "A05P03_P2.png",
    A05P03_P3 : folder_images_personajes + "A05P03_P3.png",
    A05P03_A : folder_images_personajes + "A05P03_A.plist",

    A05P04_P1 : folder_images_personajes + "A05P04_P1.png",
    A05P04_P2 : folder_images_personajes + "A05P04_P2.png",
    A05P04_P3 : folder_images_personajes + "A05P04_P3.png",
    A05P04_A : folder_images_personajes + "A05P04_A.plist",

    A05P05_P1 : folder_images_personajes + "A05P05_P1.png",
    A05P05_P2 : folder_images_personajes + "A05P05_P2.png",
    A05P05_P3 : folder_images_personajes + "A05P05_P3.png",
    A05P05_A : folder_images_personajes + "A05P05_A.plist",

    A05P06_P1 : folder_images_personajes + "A05P06_P1.png",
    A05P06_P2 : folder_images_personajes + "A05P06_P2.png",
    A05P06_P3 : folder_images_personajes + "A05P06_P3.png",
    A05P06_A : folder_images_personajes + "A05P06_A.plist",

    A05P07_P1 : folder_images_personajes + "A05P07_P1.png",
    A05P07_P2 : folder_images_personajes + "A05P07_P2.png",
    A05P07_P3 : folder_images_personajes + "A05P07_P3.png",
    A05P07_P4 : folder_images_personajes + "A05P07_P4.png",
    A05P07_A : folder_images_personajes + "A05P07_A.plist",

    A05P08_P1 : folder_images_personajes + "A05P08_P1.png",
    A05P08_P2 : folder_images_personajes + "A05P08_P2.png",
    A05P08_P3 : folder_images_personajes + "A05P08_P3.png",
    A05P08_A : folder_images_personajes + "A05P08_A.plist",

    A05P09_P1 : folder_images_personajes + "A05P09_P1.png",
    A05P09_P2 : folder_images_personajes + "A05P09_P2.png",
    A05P09_P3 : folder_images_personajes + "A05P09_P3.png",
    A05P09_P4 : folder_images_personajes + "A05P09_P4.png",
    A05P09_A: folder_images_personajes + "A05P09_A.plist",
    
    A05P10_P1 : folder_images_personajes + "A05P10_P1.png",
    A05P10_P2 : folder_images_personajes + "A05P10_P2.png",
    A05P10_P3 : folder_images_personajes + "A05P10_P3.png",
    A05P10_P4 : folder_images_personajes + "A05P10_P4.png",
    A05P10_P5 : folder_images_personajes + "A05P10_P5.png",
    A05P10_P6 : folder_images_personajes + "A05P10_P6.png",
    A05P10_A : folder_images_personajes + "A05P10_A.plist",

    A05P11_P1 : folder_images_personajes + "A05P11_P1.png",
    A05P11_P2 : folder_images_personajes + "A05P11_P2.png",
    A05P11_P3 : folder_images_personajes + "A05P11_P3.png",
    A05P11_P4 : folder_images_personajes + "A05P11_P4.png",
    A05P11_A : folder_images_personajes + "A05P11_A.plist",

//////////////////  A06    //////////////////

    A06P01_P1 : folder_images_personajes + "A06P01_P1.png",
    A06P01_P2 : folder_images_personajes + "A06P01_P2.png",
    A06P01_A : folder_images_personajes + "A06P01_A.plist",

    A06P02_P1 : folder_images_personajes + "A06P02_P1.png",
    A06P02_P2 : folder_images_personajes + "A06P02_P2.png",
    A06P02_A : folder_images_personajes + "A06P02_A.plist",

    A06P03_P1 : folder_images_personajes + "A06P03_P1.png",
    A06P03_P2 : folder_images_personajes + "A06P03_P2.png",
    A06P03_A : folder_images_personajes + "A06P03_A.plist",

    A06P04_P1 : folder_images_personajes + "A06P04_P1.png",
    A06P04_P2 : folder_images_personajes + "A06P04_P2.png",
    A06P04_A : folder_images_personajes + "A06P04_A.plist",

    A06P05_P1 : folder_images_personajes + "A06P05_P1.png",
    A06P05_P2 : folder_images_personajes + "A06P05_P2.png",
    A06P05_A : folder_images_personajes + "A06P05_A.plist",

    A06P06_P1 : folder_images_personajes + "A06P06_P1.png",
    A06P06_P2 : folder_images_personajes + "A06P06_P2.png",
    A06P06_A : folder_images_personajes + "A06P06_A.plist",

    A06P07_P1 : folder_images_personajes + "A06P07_P1.png",
    A06P07_P2 : folder_images_personajes + "A06P07_P2.png",
    A06P07_A : folder_images_personajes + "A06P07_A.plist",

    A06P08_P1 : folder_images_personajes + "A06P08_P1.png",
    A06P08_P2 : folder_images_personajes + "A06P08_P2.png",
    A06P08_A : folder_images_personajes + "A06P08_A.plist",

    A06P09_P1 : folder_images_personajes + "A06P09_P1.png",
    A06P09_P2 : folder_images_personajes + "A06P09_P2.png",
    A06P09_A: folder_images_personajes + "A06P09_A.plist",
    
    A06P10_P1 : folder_images_personajes + "A06P10_P1.png",
    A06P10_P2 : folder_images_personajes + "A06P10_P2.png",
    A06P10_A : folder_images_personajes + "A06P10_A.plist",

    A06P11_P1 : folder_images_personajes + "A06P11_P1.png",
    A06P11_P2 : folder_images_personajes + "A06P11_P2.png",
    A06P11_A : folder_images_personajes + "A06P11_A.plist",

//////////////////  A07    //////////////////

    A07P01_P1 : folder_images_personajes + "A07P01_P1.png",
    A07P01_P2 : folder_images_personajes + "A07P01_P2.png",
    A07P01_A : folder_images_personajes + "A07P01_A.plist",

    A07P02_P1 : folder_images_personajes + "A07P02_P1.png",
    A07P02_P2 : folder_images_personajes + "A07P02_P2.png",
    A07P02_A : folder_images_personajes + "A07P02_A.plist",

    A07P03_P1 : folder_images_personajes + "A07P03_P1.png",
    A07P03_P2 : folder_images_personajes + "A07P03_P2.png",
    A07P03_A : folder_images_personajes + "A07P03_A.plist",

    A07P04_P1 : folder_images_personajes + "A07P04_P1.png",
    A07P04_P2 : folder_images_personajes + "A07P04_P2.png",
    A07P04_A : folder_images_personajes + "A07P04_A.plist",

    A07P05_P1 : folder_images_personajes + "A07P05_P1.png",
    A07P05_P2 : folder_images_personajes + "A07P05_P2.png",
    A07P05_A : folder_images_personajes + "A07P05_A.plist",

    A07P06_P1 : folder_images_personajes + "A07P06_P1.png",
    A07P06_P2 : folder_images_personajes + "A07P06_P2.png",
    A07P06_A : folder_images_personajes + "A07P06_A.plist",

    //A07P07 = A04P07

    A07P08_P1 : folder_images_personajes + "A07P08_P1.png",
    A07P08_P2 : folder_images_personajes + "A07P08_P2.png",
    A07P08_A : folder_images_personajes + "A07P08_A.plist",
    
    //A07P09 = A04P09

    A07P10_P1 : folder_images_personajes + "A07P10_P1.png",
    A07P10_P2 : folder_images_personajes + "A07P10_P2.png",
    A07P10_P3 : folder_images_personajes + "A07P10_P3.png",
    A07P10_A : folder_images_personajes + "A07P10_A.plist",

    //A07P11 = A04P11


//////////////////  A08     //////////////////

    A08P01_P1 : folder_images_personajes + "A08P01_P1.png",
    A08P01_P2 : folder_images_personajes + "A08P01_P2.png",
    A08P01_P3 : folder_images_personajes + "A08P01_P3.png",
    A08P01_P4 : folder_images_personajes + "A08P01_P4.png",
    A08P01_P5 : folder_images_personajes + "A08P01_P5.png",
    A08P01_P6 : folder_images_personajes + "A08P01_P6.png",
    A08P01_A : folder_images_personajes + "A08P01_A.plist",

    A08P02_P1 : folder_images_personajes + "A08P02_P1.png",
    A08P02_P2 : folder_images_personajes + "A08P02_P2.png",
    A08P02_P3 : folder_images_personajes + "A08P02_P3.png",
    A08P02_P4 : folder_images_personajes + "A08P02_P4.png",
    A08P02_P5 : folder_images_personajes + "A08P02_P5.png",
    A08P02_P6 : folder_images_personajes + "A08P02_P6.png",
    A08P02_A : folder_images_personajes + "A08P02_A.plist",

    // A08P03_P1,2,3,4,5 ESTAN EN A04P03
    A08P03_P5 : folder_images_personajes + "A08P03_P5.png",
    A08P03_P6 : folder_images_personajes + "A08P03_P6.png",
    A08P03_P7 : folder_images_personajes + "A08P03_P7.png",
    A08P03_P8 : folder_images_personajes + "A08P03_P8.png",
    A08P03_P9 : folder_images_personajes + "A08P03_P9.png",
    A08P03_P10 : folder_images_personajes + "A08P03_P10.png",
    A08P03_A : folder_images_personajes + "A08P03_A.plist",

    A08P04_P1 : folder_images_personajes + "A08P04_P1.png",
    A08P04_P2 : folder_images_personajes + "A08P04_P2.png",
    A08P04_P3 : folder_images_personajes + "A08P04_P3.png",
    A08P04_P4 : folder_images_personajes + "A08P04_P4.png",
    A08P04_P5 : folder_images_personajes + "A08P04_P5.png",
    A08P04_P6 : folder_images_personajes + "A08P04_P6.png",
    A08P04_A : folder_images_personajes + "A08P04_A.plist",

    A08P05_P1 : folder_images_personajes + "A08P05_P1.png",
    A08P05_P2 : folder_images_personajes + "A08P05_P2.png",
    A08P05_P3 : folder_images_personajes + "A08P05_P3.png",
    A08P05_P4 : folder_images_personajes + "A08P05_P4.png",
    A08P05_P5 : folder_images_personajes + "A08P05_P5.png",
    A08P05_P6 : folder_images_personajes + "A08P05_P6.png",
    A08P05_A : folder_images_personajes + "A08P05_A.plist",

    A08P06_P1 : folder_images_personajes + "A08P06_P1.png",
    A08P06_P2 : folder_images_personajes + "A08P06_P2.png",
    A08P06_P3 : folder_images_personajes + "A08P06_P3.png",
    A08P06_P4 : folder_images_personajes + "A08P06_P4.png",
    A08P06_P5 : folder_images_personajes + "A08P06_P5.png",
    A08P06_P6 : folder_images_personajes + "A08P06_P6.png",
    A08P06_A : folder_images_personajes + "A08P06_A.plist",

    A08P07_P1 : folder_images_personajes + "A08P07_P1.png",
    A08P07_P2 : folder_images_personajes + "A08P07_P2.png",
    A08P07_P3 : folder_images_personajes + "A08P07_P3.png",
    A08P07_P4 : folder_images_personajes + "A08P07_P4.png",
    A08P07_P5 : folder_images_personajes + "A08P07_P5.png",
    A08P07_P6 : folder_images_personajes + "A08P07_P6.png",
    A08P07_A : folder_images_personajes + "A08P07_A.plist",

    A08P08_P1 : folder_images_personajes + "A08P08_P1.png",
    A08P08_P2 : folder_images_personajes + "A08P08_P2.png",
    A08P08_P3 : folder_images_personajes + "A08P08_P3.png",
    A08P08_P4 : folder_images_personajes + "A08P08_P4.png",
    A08P08_P5 : folder_images_personajes + "A08P08_P5.png",
    A08P08_P6 : folder_images_personajes + "A08P08_P6.png",
    A08P08_A : folder_images_personajes + "A08P08_A.plist",

    A08P09_P1 : folder_images_personajes + "A08P09_P1.png",
    A08P09_P2 : folder_images_personajes + "A08P09_P2.png",
    A08P09_P3 : folder_images_personajes + "A08P09_P3.png",
    A08P09_P4 : folder_images_personajes + "A08P09_P4.png",
    A08P09_P5 : folder_images_personajes + "A08P09_P5.png",
    A08P09_P6 : folder_images_personajes + "A08P09_P6.png",
    A08P09_A: folder_images_personajes + "A08P09_A.plist",
    
    A08P10_P1 : folder_images_personajes + "A08P10_P1.png",
    A08P10_P2 : folder_images_personajes + "A08P10_P2.png",
    A08P10_P3 : folder_images_personajes + "A08P10_P3.png",
    A08P10_P4 : folder_images_personajes + "A08P10_P4.png",
    A08P10_P5 : folder_images_personajes + "A08P10_P5.png",
    A08P10_P6 : folder_images_personajes + "A08P10_P6.png",
    A08P10_P7 : folder_images_personajes + "A08P10_P7.png",
    A08P10_A : folder_images_personajes + "A08P10_A.plist",

    A08P11_P1 : folder_images_personajes + "A08P11_P1.png",
    A08P11_P2 : folder_images_personajes + "A08P11_P2.png",
    A08P11_P3 : folder_images_personajes + "A08P11_P3.png",
    A08P11_P4 : folder_images_personajes + "A08P11_P4.png",
    A08P11_P5 : folder_images_personajes + "A08P11_P5.png",
    A08P11_P6 : folder_images_personajes + "A08P11_P6.png",
    A08P11_A : folder_images_personajes + "A08P11_A.plist",

//////////////////  A09    //////////////////

    //  A09P01 = A03P01

    //  A09P02 = A03P02

    A09P03_P1 : folder_images_personajes + "A09P03_P1.png",
    A09P03_P2 : folder_images_personajes + "A09P03_P2.png",
    A09P03_P3 : folder_images_personajes + "A09P03_P3.png",
    A09P03_A : folder_images_personajes + "A09P03_A.plist",

    //  A09P04 = A03P04

    //  A09P05 = A03P05

    //  A09P06 = A03P06

    A09P07_P1 : folder_images_personajes + "A09P07_P1.png",
    A09P07_P2 : folder_images_personajes + "A09P07_P2.png",
    A09P07_A : folder_images_personajes + "A09P07_A.plist",

    //  A09P08 = A03P08

    A09P09_P1 : folder_images_personajes + "A09P09_P1.png",
    A09P09_P2 : folder_images_personajes + "A09P09_P2.png",
    A09P09_A: folder_images_personajes + "A09P09_A.plist",
    
    A09P10_P1 : folder_images_personajes + "A09P10_P1.png",
    A09P10_P2 : folder_images_personajes + "A09P10_P2.png",
    A09P10_A : folder_images_personajes + "A09P10_A.plist",

    A09P11_P1 : folder_images_personajes + "A09P11_P1.png",
    A09P11_P2 : folder_images_personajes + "A09P11_P2.png",
    A09P11_A : folder_images_personajes + "A09P11_A.plist",

//////////////////  A10    //////////////////

    A10P01_P1 : folder_images_personajes + "A10P01_P1.png",
    A10P01_P2 : folder_images_personajes + "A10P01_P2.png",
    A10P01_P3 : folder_images_personajes + "A10P01_P3.png",
    A10P01_P4 : folder_images_personajes + "A10P01_P4.png",
    A10P01_A : folder_images_personajes + "A10P01_A.plist",

    A10P02_P1 : folder_images_personajes + "A10P02_P1.png",
    A10P02_P2 : folder_images_personajes + "A10P02_P2.png",
    A10P02_P3 : folder_images_personajes + "A10P02_P3.png",
    A10P02_P4 : folder_images_personajes + "A10P02_P4.png",
    A10P02_A : folder_images_personajes + "A10P02_A.plist",

    A10P03_P1 : folder_images_personajes + "A10P03_P1.png",
    A10P03_P2 : folder_images_personajes + "A10P03_P2.png",
    A10P03_P3 : folder_images_personajes + "A10P03_P3.png",
    A10P03_P4 : folder_images_personajes + "A10P03_P4.png",
    A10P03_A : folder_images_personajes + "A10P03_A.plist",

    A10P04_P1 : folder_images_personajes + "A10P04_P1.png",
    A10P04_P2 : folder_images_personajes + "A10P04_P2.png",
    A10P04_P3 : folder_images_personajes + "A10P04_P3.png",
    A10P04_P4 : folder_images_personajes + "A10P04_P4.png",
    A10P04_A : folder_images_personajes + "A10P04_A.plist",

    A10P05_P1 : folder_images_personajes + "A10P05_P1.png",
    A10P05_P2 : folder_images_personajes + "A10P05_P2.png",
    A10P05_P3 : folder_images_personajes + "A10P05_P3.png",
    A10P05_P4 : folder_images_personajes + "A10P05_P4.png",
    A10P05_A : folder_images_personajes + "A10P05_A.plist",

    A10P06_P1 : folder_images_personajes + "A10P06_P1.png",
    A10P06_P2 : folder_images_personajes + "A10P06_P2.png",
    A10P06_P3 : folder_images_personajes + "A10P06_P3.png",
    A10P06_A : folder_images_personajes + "A10P06_A.plist",

    A10P07_P1 : folder_images_personajes + "A10P07_P1.png",
    A10P07_P2 : folder_images_personajes + "A10P07_P2.png",
    A10P07_P3 : folder_images_personajes + "A10P07_P3.png",
    A10P07_P4 : folder_images_personajes + "A10P07_P4.png",
    A10P07_A : folder_images_personajes + "A10P07_A.plist",

    A10P08_P1 : folder_images_personajes + "A10P08_P1.png",
    A10P08_P2 : folder_images_personajes + "A10P08_P2.png",
    A10P08_P3 : folder_images_personajes + "A10P08_P3.png",
    A10P08_A : folder_images_personajes + "A10P08_A.plist",

    A10P09_P1 : folder_images_personajes + "A10P09_P1.png",
    A10P09_P2 : folder_images_personajes + "A10P09_P2.png",
    A10P09_P3 : folder_images_personajes + "A10P09_P3.png",
    A10P09_P4 : folder_images_personajes + "A10P09_P4.png",
    A10P09_A: folder_images_personajes + "A10P09_A.plist",
    
    A10P10_P1 : folder_images_personajes + "A10P10_P1.png",
    A10P10_P2 : folder_images_personajes + "A10P10_P2.png",
    A10P10_P3 : folder_images_personajes + "A10P10_P3.png",
    A10P10_A : folder_images_personajes + "A10P10_A.plist",

    A10P11_P1 : folder_images_personajes + "A10P11_P1.png",
    A10P11_P2 : folder_images_personajes + "A10P11_P2.png",
    A10P11_P3 : folder_images_personajes + "A10P11_P3.png",
    A10P11_P4 : folder_images_personajes + "A10P11_P4.png",
    A10P11_A : folder_images_personajes + "A10P11_A.plist",

//////////////////  A11    //////////////////

    A11P01_P1 : folder_images_personajes + "A11P01_P1.png",
    A11P01_P2 : folder_images_personajes + "A11P01_P2.png",
    A11P01_A : folder_images_personajes + "A11P01_A.plist",

    A11P02_P1 : folder_images_personajes + "A11P02_P1.png",
    A11P02_P2 : folder_images_personajes + "A11P02_P2.png",
    A11P02_A : folder_images_personajes + "A11P02_A.plist",

    A11P03_P1 : folder_images_personajes + "A11P03_P1.png",
    A11P03_P2 : folder_images_personajes + "A11P03_P2.png",
    A11P03_A : folder_images_personajes + "A11P03_A.plist",

    A11P04_P1 : folder_images_personajes + "A11P04_P1.png",
    A11P04_P2 : folder_images_personajes + "A11P04_P2.png",
    A11P04_A : folder_images_personajes + "A11P04_A.plist",

    A11P05_P1 : folder_images_personajes + "A11P05_P1.png",
    A11P05_P2 : folder_images_personajes + "A11P05_P2.png",
    A11P05_A : folder_images_personajes + "A11P05_A.plist",

    A11P06_P1 : folder_images_personajes + "A11P06_P1.png",
    A11P06_P2 : folder_images_personajes + "A11P06_P2.png",
    A11P06_A : folder_images_personajes + "A11P06_A.plist",

    A11P07_P1 : folder_images_personajes + "A11P07_P1.png",
    A11P07_P2 : folder_images_personajes + "A11P07_P2.png",
    A11P07_A : folder_images_personajes + "A11P07_A.plist",

    A11P08_P1 : folder_images_personajes + "A11P08_P1.png",
    A11P08_P2 : folder_images_personajes + "A11P08_P2.png",
    A11P08_A : folder_images_personajes + "A11P08_A.plist",

    A11P09_P1 : folder_images_personajes + "A11P09_P1.png",
    A11P09_P2 : folder_images_personajes + "A11P09_P2.png",
    A11P09_A: folder_images_personajes + "A11P09_A.plist",
    
    A11P10_P1 : folder_images_personajes + "A11P10_P1.png",
    A11P10_P2 : folder_images_personajes + "A11P10_P2.png",
    A11P10_A : folder_images_personajes + "A11P10_A.plist",

    A11P11_P1 : folder_images_personajes + "A11P11_P1.png",
    A11P11_P2 : folder_images_personajes + "A11P11_P2.png",
    A11P11_A : folder_images_personajes + "A11P11_A.plist",

    // Imagenes frases_Lugares:
    FL1 : folder_images_F_Lugares + "ka ra dathe_rio.png",
    FL2 : folder_images_F_Lugares + "ka ra denda_tienda.png",
    FL3 : folder_images_F_Lugares + "ka ra mbothi_agua.png",
    FL4 : folder_images_F_Lugares + "ka ra ngu_casa.png",
    FL5 :folder_images_F_Lugares + "ka ra ntoi_plaza.png",
    FL6 : folder_images_F_Lugares + "ka ra ntoti_pozo.png",
    FL7 : folder_images_F_Lugares + "ka ra nu_camino.png",
    FL8 : folder_images_F_Lugares + "ka ra thi_afuera.png",
    FL9 : folder_images_F_Lugares + "ka ra wahi_milpa.png",
    FL10 : folder_images_F_Lugares + "ka ra xanthe_cerro.png",
    FL11 : folder_images_F_Lugares + "ka ra za_arbol.png",

    //  Imagenes frases_Acciones:
    FA1 : folder_images_F_Accion + "aha_duerme.png",
    FA2 : folder_images_F_Accion + "hekuari.png",
    FA3 : folder_images_F_Accion + "ntsoki.png",
    FA4 : folder_images_F_Accion + "nxoki.png",
    FA5 : folder_images_F_Accion + "nzoni.png",
    FA6 : folder_images_F_Accion + "na.png",
    FA7 : folder_images_F_Accion + "no_camina.png",
    FA8 : folder_images_F_Accion + "ratsu.png",
    FA9 : folder_images_F_Accion + "tsi_come.png",
    FA10 : folder_images_F_Accion + "tsithe.png",
    FA11 : folder_images_F_Accion + "tuhu.png",

    // Imagenes frases_Personajes:
    FP1 : folder_images_F_Personajes + "ma juada.png", 
    FP2 : folder_images_F_Personajes + "ra batsi.png",
    FP3 : folder_images_F_Personajes + "ra jua.png",
    FP4 : folder_images_F_Personajes + "ra me.png",
    FP5 : folder_images_F_Personajes + "ra nana.png",
    FP6 : folder_images_F_Personajes + "ra ndata_abuelo.png",
    FP7 : folder_images_F_Personajes + "ra ngoro.png",
    FP8 : folder_images_F_Personajes + "ra nzaya.png",
    FP9 : folder_images_F_Personajes + "ra oni_pollo.png",
    FP10 : folder_images_F_Personajes + "ra tsanga.png",
    FP11 : folder_images_F_Personajes + "ra tsuntsu.png",

//  IMAGENES TRADUCCION:
    
    //  Traduccion de Lugares:
    TL1 : folder_images_T_Lugares + "TL1.png",
    TL2 : folder_images_T_Lugares + "TL2.png",
    TL3 : folder_images_T_Lugares + "TL3.png",
    TL4 : folder_images_T_Lugares + "TL4.png",
    TL5 : folder_images_T_Lugares + "TL5.png",
    TL6 : folder_images_T_Lugares + "TL6.png",
    TL7 : folder_images_T_Lugares + "TL7.png",
    TL8 : folder_images_T_Lugares + "TL8.png",
    TL9 : folder_images_T_Lugares + "TL9.png",
    TL10 : folder_images_T_Lugares + "TL10.png",
    TL11 : folder_images_T_Lugares + "TL11.png",

    //Traduccion de Acciones:

    TA1 : folder_images_T_Acciones + "TA1.png",
    TA2 : folder_images_T_Acciones + "TA2.png",
    TA3 : folder_images_T_Acciones + "TA3.png",
    TA4 : folder_images_T_Acciones + "TA4.png",
    TA5 : folder_images_T_Acciones + "TA5.png",
    TA6 : folder_images_T_Acciones + "TA6.png",
    TA7 : folder_images_T_Acciones + "TA7.png",
    TA8 : folder_images_T_Acciones + "TA8.png",
    TA9 : folder_images_T_Acciones + "TA9.png",
    TA10 : folder_images_T_Acciones + "TA10.png",
    TA11 : folder_images_T_Acciones + "TA11.png",

    //Traduccion de Personajes:

    TP1 : folder_images_T_Personajes + "TP1.png",
    TP2 : folder_images_T_Personajes + "TP2.png",
    TP3 : folder_images_T_Personajes + "TP3.png",
    TP4 : folder_images_T_Personajes + "TP4.png",
    TP5 : folder_images_T_Personajes + "TP5.png",
    TP6 : folder_images_T_Personajes + "TP6.png",
    TP7 : folder_images_T_Personajes + "TP7.png",
    TP8 : folder_images_T_Personajes + "TP8.png",
    TP9 : folder_images_T_Personajes + "TP9.png",
    TP10 : folder_images_T_Personajes + "TP10.png",
    TP11 : folder_images_T_Personajes + "TP11.png"


};

var g_resources = [];
for (var i in res) {
    g_resources.push(res[i]);
}